# Introduction 
.NET 7 implementation of the SiLA2 Standard (https://sila-standard.com/)
  * Platform-independent
  * SiLA2.Server even runs on embedded Linux Host as recommended AspNetCore-Process
  * Feature-To-Proto-Generation by XSLT
    * SiLA2 Core Features included in SiLA2.dll
    * Additional Features should be part of Feature-Implementation Assemblies (just add your features and protos in MSBuild Targets ProtoPreparation & ProtoGeneration like it is done in Assemblies TemperatureController.Features.csproj or ShakerController.Features.csproj)
  * Extensible InProcess Server Web Frontend included (based on Blazor (https://dotnet.microsoft.com/apps/aspnet/web-apps/blazor) which supports "server push" functionality)

   ![image](misc/web_frontend_realtime_temperature_monitoring.gif) 

  * Optional InProcess-Database-Module (based on SQLite) with basic User Management which can be easily extended.
  * Optional InProcess-Document-Database-Module (based on LiteDB) named SiLA2.Database.NoSQL which can be used as AnIML data storage...
  * Optional Inter Process Communication Module (SiLA2.IPC.NetMQ) for Socket Communication...compatible with ZeroMQ (https://zeromq.org/)
  * Optional AnIML Module offering the AnIML Domain Model as C# classes generated from official AnIML schemas (https://github.com/AnIML/schemas)
  * Optional Component to manage Encryption and Certifcates

  If you want to see all the Modules and Components you should start SiLA2.Temperature.Server.App and click >> https://localhost:5001
  Running the Server the first time, you´ll have to add an exception to your browser once due to the self-signed certificate which was created by the Server if there´s none.
   
# Prerequisites
  * Linux / macOS
    * You´ll need the .NET 7 SDK >> https://dotnet.microsoft.com/download/dotnet/7.0
	* It´s not necessary to build applications with a GUI but if you do so an IDE like Visual Studio Code ( >> https://code.visualstudio.com/ ) would be convenient
  * Windows
    * Download free IDE Visual Studio 2022 Community ( >> https://visualstudio.microsoft.com/de/vs/community/ ), use commercial Visual Studio 2022 Version or Visual Studio Code as well
	* .NET 7 SDK is included in Visual Studio 2022...if you want to use Visual Studio Code or other IDEs you´ll have to download it on your own (see link above)

# Getting Started
  * Clone Repo
    * Please be sure fetching sila_base submodule by
      * checking out the Repository with git-submodules recursively 
        * git clone --recurse-submodules https://gitlab.com/SiLA2/sila_csharp.git
      * or check out the Repository and run following commands
        * git submodule init
        * git submodule update
  * Run gRPC-Server
    * SiLA2.Temperature.Server.Basic.App
    or
    * SiLA2.Temperature.Server.App (containing optional WebFrontend- and DatabaseModule)
      * After having started the SilaServer process you can also follow Link https://<server-hostname-or-server-ip>:13742 (in Debug-Mode https://localhost:5001) to open a SilaServer-WebFrontend
      * In the SilaServer-WebFrontend you´ll find NavigationLink "User Management"-View to use SilaServer-Database. There´s also an example of how Server-Push-Feature can be used...just click on NavigationLink "Temperature" and hit button "Change Temperature"...
  * Run SiLA2.Temperature.Client.App connecting automatically to SilaServer

# Build your own Project based on official Nuget-Packages
  * Created ASP.NET Core Application as SiLA2.Server Project
  * Search for & reference SiLA2.* packages found at Nuget.org in Visual Studio or https://www.nuget.org/ ...use at least SiLA2.Core...
  * Create *.sila.xml-Feature-File and include it like it was done in Example Project SiLA2.Referencing.Nuget.Features.csproj
  * Implement the features you´ve defined in your *.sila.xml-Feature-File in your Feature-Assembly
  * Reference your Feature-Assenbly in your SiLA2.Server Project
  * Add MSBuild Targets ProtoPreparation & ProtoGeneration in your FeatureAssembly.csproj (like it is done in any of the ExampleFeature.csproj files)
  * Create SilA2-Clients communicating with the SiLA2.Server...in this case you might want to use Nuget-Package SiLA2.Client... 

# Build and Test
* Just build Solution and run as described in "# Getting Started".
* Alternatively you could use the SiLA Universal Client (https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client)...it´s still experimental and it could be possible that you have to register SiLA2 Servers manually by IP yet...
* If you have problems building the solution you might clean your Nuget-Cache by 'rmdir -r %UserProfile%\.nuget\packages\*' once...

# Contribute
It´s Open Source (License >> MIT)...feel free to use or contribute. For Merge-Requests contact me by E-Mail >>> CPohl@inheco.com or Stefan Koch >>> koch@equicon.de

# For Open Questions
Visit https://gitlab.com/SiLA2/sila_csharp/-/issues

 
