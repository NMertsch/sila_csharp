﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Sila2.Utils;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using System.Net;

namespace SiLA2.AspNetCore
{
    public static class KestrelExtensions
    {
        public static Action<KestrelServerOptions> SetupKestrel(this string[] args)
        {
            return serverOptions =>
            {
                var certificateProvider = serverOptions.ApplicationServices.GetService(typeof(ICertificateProvider)) as ICertificateProvider;
                var certificate = certificateProvider.GetServerCertificate(safeIfNotExists: true);

                serverOptions.ConfigureEndpointDefaults(endpoints => endpoints.Protocols = HttpProtocols.Http1AndHttp2);
                var serverConfig = serverOptions.ApplicationServices.GetService(typeof(IServerConfig)) as IServerConfig;

                args.ParseServerCmdLineArgs<CmdLineServerArgs>(serverConfig);

                serverOptions.Listen(IPAddress.Any, serverConfig.Port, listenOptions => listenOptions.UseHttps(certificate));
            };
        }
    }
}
