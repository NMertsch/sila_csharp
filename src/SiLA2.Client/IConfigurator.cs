﻿using Grpc.Net.Client;
using Microsoft.Extensions.DependencyInjection;
using SiLA2.Server;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Client
{
    public interface IConfigurator
    {
        IServiceCollection Container { get; }
        IDictionary<Guid, ServerData> DiscoveredServers { get; }
        IServiceProvider ServiceProvider { get; }
        Task<GrpcChannel> GetChannel(bool acceptAnyServerCertificate = true);
        Task<GrpcChannel> GetChannel(string host, int port, bool acceptAnyServerCertificate = true);
        Task<IDictionary<Guid, ServerData>> SearchForServers();
        void UpdateServiceProvider();
    }
}