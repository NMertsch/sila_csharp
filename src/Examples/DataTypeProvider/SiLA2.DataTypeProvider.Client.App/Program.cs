﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2.Client;
using SiLA2.Utils.Network;
using Framework = Sila2.Org.Silastandard;

namespace SiLA2.DataTypeProvider.Client.App
{
    internal class Program
    {
        private static IConfigurationRoot _configuration;

        static async Task Main(string[] args)
        {
            try
            {
                var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                _configuration = configBuilder.Build();

                var clientSetup = new Configurator(_configuration, args);

                Console.WriteLine("Starting Server Discovery...");

                var serverMap = await clientSetup.SearchForServers();

                GrpcChannel channel;
                var serverType = "SiLA2DataTypeProviderServer";
                var server = serverMap.Values.FirstOrDefault(x => x.Info.Type == serverType);
                if (server != null)
                {
                    Console.WriteLine($"Connecting to {server}");
                    channel = await clientSetup.GetChannel(server.Config.FQHN, server.Config.Port, acceptAnyServerCertificate: true);
                }
                else
                {
                    var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
                    Console.WriteLine($"No connection automatically discovered. Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
                    channel = await clientSetup.GetChannel(acceptAnyServerCertificate: true);
                }

                Console.WriteLine("Trying to setup Client ...");
                var clientImpl = new Client(channel, clientSetup.ServiceProvider.GetService<ILoggerFactory>());
                Console.WriteLine("Client Setup successful for following Clients :");
                foreach (var client in clientImpl.Clients)
                {
                    Console.WriteLine($"{client.GetType().FullName}");
                }

                Console.WriteLine("\n--- Binary transfer ---\n");

                // property BinaryValueDirectly
                Console.WriteLine("Calling 'Get_BinaryValueDirectly()' ...");
                Console.WriteLine($"Response: '{clientImpl.GetBinaryValueDirectly()}'");

                // property BinaryValueDownload
                Console.WriteLine("\nCalling 'Get_BinaryValueDownload()' ...");
                clientImpl.GetBinaryValueDownload().Wait();

                // command SetBinaryValue
                Console.WriteLine("\nCalling 'SetBinaryValue' with sending the value directly ...");
                Console.WriteLine(clientImpl.SetStringAsDirectBinaryValue("SiLA2_Test_String_Value"));

                Console.WriteLine("\nCalling 'SetBinaryValue' with binary upload ...");
                await clientImpl.SetBinaryValueUpload(System.Text.Encoding.UTF8.GetBytes("A_somewhat_longer_test_string_to_demonstrate_the_binary_upload"), 5);
                Console.WriteLine($"Response: '{System.Text.Encoding.UTF8.GetString(clientImpl.ResponseData)}'");

                // generate random sequence of characters
                const int dataSize = 10 * 1024 * 1024; // 10 MB
                var sourceData = new byte[dataSize];
                var rnd = new Random((int)DateTime.Now.Ticks);
                rnd.NextBytes(sourceData);

                // command SetBinaryValue with large binary data
                const int chunkSize = 65536; //Recommended chunk size for large messages -> 16384 to 65536 Bytes
                Console.WriteLine($"{Environment.NewLine}{DateTime.Now} Calling 'SetBinaryValue' with a random sequence of {dataSize} bytes and chunk size of {chunkSize} ...");
                await clientImpl.SetBinaryValueUpload(sourceData, chunkSize);
                if (sourceData.SequenceEqual(clientImpl.ResponseData))
                {
                    Console.WriteLine($"{DateTime.Now} Received response data equals sent data");
                }
                else
                {
                    Console.WriteLine("ERROR: Received response data differs from sent data!");
                }

                Console.WriteLine("\n--- Any type values ---\n");
                clientImpl.GetAnyTypeValues();

                // setting Any type values
                Console.WriteLine("\n");
                clientImpl.SetAnyTypeStringValue("My_Any_type_string_value");
                clientImpl.SetAnyTypeIntegerValue(123456789);
                clientImpl.SetAnyTypeRealValue(9.87654321);
                clientImpl.SetAnyTypeBooleanValue(true);
                clientImpl.SetAnyTypeBinaryValue("My_Any_type_binary_value_string_content");
                clientImpl.SetAnyTypeDateValue(new Framework.Date { Day = 29, Month = 7, Year = 2021, Timezone = new Framework.Timezone { Hours = 2, Minutes = 0 } });
                clientImpl.SetAnyTypeTimeValue(new Framework.Time { Second = 44, Minute = 33, Hour = 22, Timezone = new Framework.Timezone { Hours = 2, Minutes = 0 } });
                clientImpl.SetAnyTypeTimestampValue(new Framework.Timestamp { Second = 44, Minute = 33, Hour = 22, Day = 29, Month = 7, Year = 2021, Timezone = new Framework.Timezone { Hours = 2, Minutes = 0 } });

                Thread.Sleep(1000);
                Console.WriteLine("Shutting down connection...");

                await channel.ShutdownAsync();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                Console.WriteLine("Client Error: {0}", e);
            }

            Console.ReadKey();
        }
    }
}
