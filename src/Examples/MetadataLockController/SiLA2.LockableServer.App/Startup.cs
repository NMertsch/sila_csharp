﻿using SiLA2.Commands;
using SiLA2.Network;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Utils;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using MetadataLockController.Features.Services;
using System.Reflection;
using SiLA2.Utils.Security;
using SiLA2.Utils.Config;
using SiLA2.AspNetCore;

namespace SiLA2.LockableServer.App
{
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc(options =>
            {
                options.EnableDetailedErrors = true;
                options.Interceptors.Add<Server.Interceptors.LoggingInterceptor>();
                options.Interceptors.Add<Server.Interceptors.MetadataValidationInterceptor>();
                options.Interceptors.Add<Server.Interceptors.ParameterValidationInterceptor>();
            });
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<LockControllerService>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<MetadataManager>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.AddSingleton<GreetingProviderImpl>();
            services.AddSingleton<ICertificateProvider, CertificateProvider>();
            services.AddSingleton<CertificateContext>();
            services.AddSingleton<ICertificateRepository, CertificateRepository>();
            services.AddSingleton<LockControllerService>();
            services.AddSingleton<IServerConfig>(new ServerConfig(_configuration["ServerConfig:Name"],
                                                                Guid.Parse(_configuration["ServerConfig:UUID"]),
                                                                _configuration["ServerConfig:FQHN"],
                                                                int.Parse(_configuration["ServerConfig:Port"]),
                                                                _configuration["ServerConfig:NetworkInterface"],
                                                                _configuration["ServerConfig:DiscoveryServiceName"]));
#if DEBUG
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
            services.ConfigureWritable<ServerConfig>(_configuration.GetSection("ServerConfig"), configFile);
        }

        //Note: GreetingProviderImpl _ is injected for calling its constructor in which the SilaFeature is added to the SiLA2.Server feature collection
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ISiLA2Server siLA2Server, ServerInformation serverInformation, GreetingProviderImpl _, ILogger<Startup> logger)
        {
            siLA2Server.ReadFeature("LockController-v2_0.sila.xml", typeof(LockControllerService));
            siLA2Server.ReadFeature("GreetingProvider-v1_0.sila.xml", typeof(GreetingProviderImpl));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<GreetingProviderImpl>();
                endpoints.MapGrpcService<SiLAService>();
                endpoints.MapGrpcService<LockControllerService>();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });

            logger.LogInformation($"{serverInformation}");
            logger.LogInformation("Starting Server Announcement...");
            siLA2Server.Start();
            logger.LogInformation($"{DateTime.Now} : Started {GetType().Assembly.FullName}");
        }
    }
}
