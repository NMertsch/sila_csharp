﻿using Microsoft.Extensions.Logging;
using SiLA2.Server;
using Grpc.Core;
using Sila2.Org.Silastandard.Examples.Greetingprovider.V1;
using SiLAFramework = Sila2.Org.Silastandard;
using SiLA2.Server.Services;

namespace MetadataLockController.Features.Services
{
    /// <summary>
    /// Implements the functionality of the GreetingsProviderImpl feature.
    /// Uses the LockController service of the SiLA2 library.
    /// </summary>
    public class GreetingProviderImpl : GreetingProvider.GreetingProviderBase
    {
        /// <summary>The start year of the server</summary>
        private readonly int _serverStartYear;
        private readonly LockControllerService _lockController;

        private readonly ILogger<GreetingProviderImpl> _logger;

        public GreetingProviderImpl(ISiLA2Server silaServer, LockControllerService lockController, ILogger<GreetingProviderImpl> logger)
        {
            var silaFeature = silaServer.ReadFeature(Path.Combine("Features", "GreetingProvider-v1_0.sila.xml"));

            _serverStartYear = DateTime.Now.Year;
            _lockController = lockController;

            // specify SayHello command as lockable
            var sayHelloCmd = silaFeature.GetDefinedCommands().Find(match => match.Identifier == "SayHello");
            if (sayHelloCmd != null)
            {
                _lockController.LockableItems = new List<string> { silaFeature.GetFullyQualifiedCommandIdentifier(sayHelloCmd.Identifier) };
            }

            _logger = logger;
        }


        #region Overrides of GreetingProviderBase

        /// <summary>
        /// Checks the metadata for a valid lock identifier and answers the SayHello RPC by returning a greeting message.
        /// </summary>
        /// <param name="request">The call parameters containing the Name parameter.</param>
        /// <param name="context">The call context (not used here).</param>
        /// <returns>The call response containing the Greeting field.</returns>
        public override Task<SayHello_Responses> SayHello(SayHello_Parameters request, ServerCallContext context)
        {
            // check if server is locked and (if so) if the lock identifier is valid
            _lockController.CheckLock(context.RequestHeaders);

            return Task.FromResult(new SayHello_Responses { Greeting = new SiLAFramework.String { Value = "Hello " + request.Name.Value } });
        }

        /// <summary>
        /// Receives the unobservable property RPC and returns the current year as a field of the call response.
        /// </summary>
        /// <param name="request">The call parameters (empty for properties).</param>
        /// <param name="context">The call context (not used here).</param>
        /// <returns>The call response containing the CurrentYear field.</returns>
        public override Task<Get_StartYear_Responses> Get_StartYear(Get_StartYear_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_StartYear_Responses { StartYear = new SiLAFramework.Integer { Value = _serverStartYear } });
        }

        #endregion
    }
}