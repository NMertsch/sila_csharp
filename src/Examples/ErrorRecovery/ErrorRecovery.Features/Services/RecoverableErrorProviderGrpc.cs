// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: RecoverableErrorProvider.proto
// </auto-generated>
#pragma warning disable 0414, 1591, 8981
#region Designer generated code

using grpc = global::Grpc.Core;

namespace Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1 {
  public static partial class RecoverableErrorProvider
  {
    static readonly string __ServiceName = "sila2.de.equicon.examples.recoverableerrorprovider.v1.RecoverableErrorProvider";

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static void __Helper_SerializeMessage(global::Google.Protobuf.IMessage message, grpc::SerializationContext context)
    {
      #if !GRPC_DISABLE_PROTOBUF_BUFFER_SERIALIZATION
      if (message is global::Google.Protobuf.IBufferMessage)
      {
        context.SetPayloadLength(message.CalculateSize());
        global::Google.Protobuf.MessageExtensions.WriteTo(message, context.GetBufferWriter());
        context.Complete();
        return;
      }
      #endif
      context.Complete(global::Google.Protobuf.MessageExtensions.ToByteArray(message));
    }

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static class __Helper_MessageCache<T>
    {
      public static readonly bool IsBufferMessage = global::System.Reflection.IntrospectionExtensions.GetTypeInfo(typeof(global::Google.Protobuf.IBufferMessage)).IsAssignableFrom(typeof(T));
    }

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static T __Helper_DeserializeMessage<T>(grpc::DeserializationContext context, global::Google.Protobuf.MessageParser<T> parser) where T : global::Google.Protobuf.IMessage<T>
    {
      #if !GRPC_DISABLE_PROTOBUF_BUFFER_SERIALIZATION
      if (__Helper_MessageCache<T>.IsBufferMessage)
      {
        return parser.ParseFrom(context.PayloadAsReadOnlySequence());
      }
      #endif
      return parser.ParseFrom(context.PayloadAsNewBuffer());
    }

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Parameters> __Marshaller_sila2_de_equicon_examples_recoverableerrorprovider_v1_RaiseRecoverableError_Parameters = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Parameters.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.CommandConfirmation> __Marshaller_sila2_org_silastandard_CommandConfirmation = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Sila2.Org.Silastandard.CommandConfirmation.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.CommandExecutionUUID> __Marshaller_sila2_org_silastandard_CommandExecutionUUID = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Sila2.Org.Silastandard.CommandExecutionUUID.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.ExecutionInfo> __Marshaller_sila2_org_silastandard_ExecutionInfo = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Sila2.Org.Silastandard.ExecutionInfo.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Responses> __Marshaller_sila2_de_equicon_examples_recoverableerrorprovider_v1_RaiseRecoverableError_Responses = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Responses.Parser));

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Method<global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Parameters, global::Sila2.Org.Silastandard.CommandConfirmation> __Method_RaiseRecoverableError = new grpc::Method<global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Parameters, global::Sila2.Org.Silastandard.CommandConfirmation>(
        grpc::MethodType.Unary,
        __ServiceName,
        "RaiseRecoverableError",
        __Marshaller_sila2_de_equicon_examples_recoverableerrorprovider_v1_RaiseRecoverableError_Parameters,
        __Marshaller_sila2_org_silastandard_CommandConfirmation);

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Method<global::Sila2.Org.Silastandard.CommandExecutionUUID, global::Sila2.Org.Silastandard.ExecutionInfo> __Method_RaiseRecoverableError_Info = new grpc::Method<global::Sila2.Org.Silastandard.CommandExecutionUUID, global::Sila2.Org.Silastandard.ExecutionInfo>(
        grpc::MethodType.ServerStreaming,
        __ServiceName,
        "RaiseRecoverableError_Info",
        __Marshaller_sila2_org_silastandard_CommandExecutionUUID,
        __Marshaller_sila2_org_silastandard_ExecutionInfo);

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Method<global::Sila2.Org.Silastandard.CommandExecutionUUID, global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Responses> __Method_RaiseRecoverableError_Result = new grpc::Method<global::Sila2.Org.Silastandard.CommandExecutionUUID, global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Responses>(
        grpc::MethodType.Unary,
        __ServiceName,
        "RaiseRecoverableError_Result",
        __Marshaller_sila2_org_silastandard_CommandExecutionUUID,
        __Marshaller_sila2_de_equicon_examples_recoverableerrorprovider_v1_RaiseRecoverableError_Responses);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RecoverableErrorProviderReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of RecoverableErrorProvider</summary>
    [grpc::BindServiceMethod(typeof(RecoverableErrorProvider), "BindService")]
    public abstract partial class RecoverableErrorProviderBase
    {
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::System.Threading.Tasks.Task<global::Sila2.Org.Silastandard.CommandConfirmation> RaiseRecoverableError(global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Parameters request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::System.Threading.Tasks.Task RaiseRecoverableError_Info(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::IServerStreamWriter<global::Sila2.Org.Silastandard.ExecutionInfo> responseStream, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::System.Threading.Tasks.Task<global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Responses> RaiseRecoverableError_Result(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Client for RecoverableErrorProvider</summary>
    public partial class RecoverableErrorProviderClient : grpc::ClientBase<RecoverableErrorProviderClient>
    {
      /// <summary>Creates a new client for RecoverableErrorProvider</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public RecoverableErrorProviderClient(grpc::ChannelBase channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for RecoverableErrorProvider that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public RecoverableErrorProviderClient(grpc::CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      protected RecoverableErrorProviderClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      protected RecoverableErrorProviderClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::Sila2.Org.Silastandard.CommandConfirmation RaiseRecoverableError(global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Parameters request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return RaiseRecoverableError(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::Sila2.Org.Silastandard.CommandConfirmation RaiseRecoverableError(global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Parameters request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_RaiseRecoverableError, null, options, request);
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual grpc::AsyncUnaryCall<global::Sila2.Org.Silastandard.CommandConfirmation> RaiseRecoverableErrorAsync(global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Parameters request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return RaiseRecoverableErrorAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual grpc::AsyncUnaryCall<global::Sila2.Org.Silastandard.CommandConfirmation> RaiseRecoverableErrorAsync(global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Parameters request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_RaiseRecoverableError, null, options, request);
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual grpc::AsyncServerStreamingCall<global::Sila2.Org.Silastandard.ExecutionInfo> RaiseRecoverableError_Info(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return RaiseRecoverableError_Info(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual grpc::AsyncServerStreamingCall<global::Sila2.Org.Silastandard.ExecutionInfo> RaiseRecoverableError_Info(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncServerStreamingCall(__Method_RaiseRecoverableError_Info, null, options, request);
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Responses RaiseRecoverableError_Result(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return RaiseRecoverableError_Result(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Responses RaiseRecoverableError_Result(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_RaiseRecoverableError_Result, null, options, request);
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual grpc::AsyncUnaryCall<global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Responses> RaiseRecoverableError_ResultAsync(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return RaiseRecoverableError_ResultAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual grpc::AsyncUnaryCall<global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Responses> RaiseRecoverableError_ResultAsync(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_RaiseRecoverableError_Result, null, options, request);
      }
      /// <summary>Creates a new instance of client from given <c>ClientBaseConfiguration</c>.</summary>
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      protected override RecoverableErrorProviderClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new RecoverableErrorProviderClient(configuration);
      }
    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    public static grpc::ServerServiceDefinition BindService(RecoverableErrorProviderBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_RaiseRecoverableError, serviceImpl.RaiseRecoverableError)
          .AddMethod(__Method_RaiseRecoverableError_Info, serviceImpl.RaiseRecoverableError_Info)
          .AddMethod(__Method_RaiseRecoverableError_Result, serviceImpl.RaiseRecoverableError_Result).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    public static void BindService(grpc::ServiceBinderBase serviceBinder, RecoverableErrorProviderBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_RaiseRecoverableError, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Parameters, global::Sila2.Org.Silastandard.CommandConfirmation>(serviceImpl.RaiseRecoverableError));
      serviceBinder.AddMethod(__Method_RaiseRecoverableError_Info, serviceImpl == null ? null : new grpc::ServerStreamingServerMethod<global::Sila2.Org.Silastandard.CommandExecutionUUID, global::Sila2.Org.Silastandard.ExecutionInfo>(serviceImpl.RaiseRecoverableError_Info));
      serviceBinder.AddMethod(__Method_RaiseRecoverableError_Result, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Sila2.Org.Silastandard.CommandExecutionUUID, global::Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1.RaiseRecoverableError_Responses>(serviceImpl.RaiseRecoverableError_Result));
    }

  }
}
#endregion
