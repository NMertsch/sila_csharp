using SiLA2.AspNetCore;

namespace SiLA2.ErrorRecovery.Server.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel(args.SetupKestrel());
                    webBuilder.UseStartup<Startup>();
                });
    }
}
