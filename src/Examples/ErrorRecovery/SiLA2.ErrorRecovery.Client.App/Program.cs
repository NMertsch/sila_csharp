﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2.Client;
using SiLA2.Utils.Network;

namespace SiLA2.ErrorRecovery.Client.App
{
    internal class Program
    {
        private static IConfigurationRoot _configuration;

        static async Task Main(string[] args)
        {
            try
            {
                var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                _configuration = configBuilder.Build();

                var clientSetup = new Configurator(_configuration, args);

                Console.WriteLine("Starting Server Discovery...");

                var serverMap = await clientSetup.SearchForServers();

                GrpcChannel channel;
                var serverType = "SiLA2ErrorRecoveryServer";
                var server = serverMap.Values.FirstOrDefault(x => x.Info.Type == serverType);
                if (server != null)
                {
                    Console.WriteLine($"Connecting to {server}");
                    channel = await clientSetup.GetChannel(server.Config.FQHN, server.Config.Port, acceptAnyServerCertificate: true);
                }
                else
                {
                    var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
                    Console.WriteLine($"No connection automatically discovered. Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
                    channel = await clientSetup.GetChannel(acceptAnyServerCertificate: true);
                }

                Console.WriteLine("Trying to setup Client ...");
                var clientImpl = new Client(channel, clientSetup.ServiceProvider.GetService<ILoggerFactory>());
                Console.WriteLine("Client Setup successful for following Clients :");
                foreach (var client in clientImpl.Clients)
                {
                    Console.WriteLine($"{client.GetType().FullName}");
                }

                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine("Calling \"Subscribe_RecoverableErrors\" ...");
                clientImpl.StartSubscribingErrors().Wait();

                // raising an error after 3s without automatic execution
                int preErrorDuration = 3;
                int automaticExecutionTimeout = 0;
                clientImpl.HandleErrorManually = true;
                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine($"Calling \"RaiseRecoverableError (after {preErrorDuration}s, automaticExecutionTimeout = {automaticExecutionTimeout}s)\" ...");
                clientImpl.RaiseRecoverableError("Test error to be handled manually", preErrorDuration, automaticExecutionTimeout).Wait();

                Thread.Sleep(1000);

                // raising an error after 3s with automatic execution after 1s
                automaticExecutionTimeout = 1;
                clientImpl.HandleErrorManually = false;
                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine($"Calling \"RaiseRecoverableError (after {preErrorDuration}s, automaticExecutionTimeout = {automaticExecutionTimeout}s)\" ...");
                clientImpl.RaiseRecoverableError($"Test error with automatic handling after {automaticExecutionTimeout}s", preErrorDuration, automaticExecutionTimeout).Wait();

                Thread.Sleep(1000);

                // set error handling timeout to 8s and raise an error without automatic error handling
                int timeout = 8;
                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine($"Calling \"SetErrorHandlingTimeout({timeout}s)\" ...");
                clientImpl.SetErrorHandlingTimeout(timeout);

                Thread.Sleep(1000);

                automaticExecutionTimeout = 0;
                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine($"Calling \"RaiseRecoverableError (after {preErrorDuration}s, automaticExecutionTimeout = {automaticExecutionTimeout}s)\" ...");
                clientImpl.RaiseRecoverableError("Test error without manual or automatic handling", preErrorDuration, automaticExecutionTimeout).Wait();

                Thread.Sleep(1000);

                // canceling error subscription
                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine("Unsubscribing \"RecoverableErrors\" property ...");
                clientImpl.StopSubscribingErrors();

                // reset error handling timeout to 0s and raise an error without sunbscription amd automatic error handling
                timeout = 0;
                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine($"Calling \"SetErrorHandlingTimeout({timeout}s)\" ...");
                clientImpl.SetErrorHandlingTimeout(timeout);

                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine($"Calling \"RaiseRecoverableError (after {preErrorDuration}s, automaticExecutionTimeout = {automaticExecutionTimeout}s)\" without subscribing to the \"RecoverableErrors\" property ...");
                clientImpl.RaiseRecoverableError("Test error without RecoverableErrors subscription", preErrorDuration, automaticExecutionTimeout).Wait();

                Thread.Sleep(1000);
                Console.WriteLine("Shutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                Console.WriteLine("Client Error: {0}", e);
            }

            Console.ReadKey();
        }
    }
}
