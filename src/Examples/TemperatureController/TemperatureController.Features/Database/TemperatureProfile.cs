﻿using SiLA2.Database.SQL.Domain;
using System.Collections.Generic;

namespace TemperatureController.Features.Database
{
    public class TemperatureProfile : BaseEntity
    {
        public int ClockInMilliseconds { get; private set; }
        /// <summary>
        /// Loops < 0 -> Inifinite Loops
        /// </summary>
        public int Loops { get; }
        public List<TemperatureProfileStep> Temperatures { get; private set; }

        public TemperatureProfile(int clockInMilliseconds, int loops, List<TemperatureProfileStep> temperatures)
        {
            ClockInMilliseconds = clockInMilliseconds;
            Loops = loops;
            Temperatures = temperatures;
        }

        public TemperatureProfile() { }
    }
}
