﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1;
using System.Threading.Tasks;
using static Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.TemperatureController;
using System;
using System.Threading;
using SiLA2.Commands;
using SiLA2.Simulation;
using System.IO;

namespace TemperatureController.Features.Services
{
    public class TemperatureControllerService : TemperatureControllerBase, IDisposable
    {
        private readonly IObservableCommandManager<ControlTemperature_Parameters, ControlTemperature_Responses> _controlTemperatureCommandManager;
        private readonly IThermostatSimulator _thermostatSimulator;
        private readonly ILogger<TemperatureControllerService> _logger;
        private readonly SiLA2.Feature _silaFeature;

        public TemperatureControllerService(IObservableCommandManager<ControlTemperature_Parameters, ControlTemperature_Responses> observableCommandManager,
                                            IThermostatSimulator thermostatSimulator,
                                            ILogger<TemperatureControllerService> logger,
                                            SiLA2.Server.ISiLA2Server silaServer)
        {
            _controlTemperatureCommandManager = observableCommandManager;
            _thermostatSimulator = thermostatSimulator;
            _logger = logger;
            _silaFeature = silaServer.ReadFeature(Path.Combine("Features", "TemperatureController-v1_0.sila.xml"));
        }

        #region Command 'ControlTemperature'

        /// <summary>Triggers the TemperatureControl command execution.</summary>
        public override async Task<CommandConfirmation> ControlTemperature(ControlTemperature_Parameters request, ServerCallContext context)
        {
            SiLA2.Server.Utils.Validation.ValidateParameter(request.TargetTemperature.Value, _silaFeature, "ControlTemperature", "TargetTemperature");
            var command = await _controlTemperatureCommandManager.AddCommand(request, ControlTemperatureTask());
            return command.Confirmation;
        }

        /// <summary>Monitors the execution state of the command execution assigned to the given command execution ID.</summary>
        public override async Task ControlTemperature_Info(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _controlTemperatureCommandManager.RegisterForInfo(cmdExecId, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                _logger.LogError($"{e}");
                throw;
            }
        }

        /// <summary>Gets the result of the TemperatureControl command execution assigned to the given command execution ID.</summary>
        public override Task<ControlTemperature_Responses> ControlTemperature_Result(CommandExecutionUUID cmdExecId, ServerCallContext context)
        {
            var command = _controlTemperatureCommandManager.GetCommand(cmdExecId);
            return Task.FromResult(command.Result);
        }

        /// <summary>
        /// Does the temperature control:
        ///  - sets the required target temperature
        ///  - waits for target temperature has been reached or an error occurred meanwhile
        ///  - sets the command execution state assigned to the given command execution ID
        /// </summary>
        private Func<IProgress<ExecutionInfo>, ControlTemperature_Parameters, CancellationToken, ControlTemperature_Responses> ControlTemperatureTask()
        {
            return (progress, parameters, cancellationToken) =>
            {
                _logger.LogInformation("Starting to control the temperature");
                var targetTemperature = parameters.TargetTemperature.Value;
                var executionInfo = new ExecutionInfo
                {
                    ProgressInfo = new Real { Value = 0 },
                    EstimatedRemainingTime = new Duration()
                };
                // initialize
                progress.Report(executionInfo);

                // persist start temperature
                var startTemperature = _thermostatSimulator.CurrentTemperature;

                var taskThread = new Thread(() =>
                {
                    _thermostatSimulator.SetTargetTemperature(targetTemperature, cancellationToken);
                });

                taskThread.Start();
                UpdateExecutionInfo(taskThread, executionInfo, startTemperature, progress);

                executionInfo.ProgressInfo = new Real
                {
                    Value = 1
                };
                executionInfo.EstimatedRemainingTime = new Duration
                {
                    Seconds = 0,
                    Nanos = 0
                };
                progress.Report(executionInfo);

                return new ControlTemperature_Responses();
            };
        }

        #endregion

        #region Property 'CurrentTemperature'

        /// <summary>
        /// Returns the current temperature with a frequency of 1 Hz.
        /// </summary>
        /// <param name="request">The gRPC parameters (empty)</param>
        /// <param name="responseStream">The response stream.</param>
        /// <param name="context">The call context (not used here).</param>
        /// <returns></returns>
        public override async Task Subscribe_CurrentTemperature(Subscribe_CurrentTemperature_Parameters request,
            IServerStreamWriter<Subscribe_CurrentTemperature_Responses> responseStream, ServerCallContext context)
        {
            var methodName = new System.Diagnostics.StackTrace().GetFrame(2).GetMethod().Name;

            do
            {
                await responseStream.WriteAsync(new Subscribe_CurrentTemperature_Responses
                {
                    CurrentTemperature = new Real
                    {
                        Value = _thermostatSimulator.CurrentTemperature
                    }
                });
                await Task.Delay(1000);
            } while (!context.CancellationToken.IsCancellationRequested);

            _logger.LogDebug($"\n\"{methodName}\" subscription cancelled");
        }

        #endregion

        private void UpdateExecutionInfo(Thread taskThread, ExecutionInfo executionInfo, double startTemperature, IProgress<ExecutionInfo> progress)
        {
            while (taskThread.IsAlive)
            {
                progress.Report(executionInfo);
                Thread.Sleep(1000);
                executionInfo.ProgressInfo.Value = Math.Abs(_thermostatSimulator.CurrentTemperature - startTemperature) /
                                                   Math.Abs(_thermostatSimulator.TargetTemperature - startTemperature);

                var absoluteTemperatureDifference =
                    Math.Abs(_thermostatSimulator.TargetTemperature - _thermostatSimulator.CurrentTemperature);
                var remainingTime = absoluteTemperatureDifference / ThermostatSimulator.KELVIN_PER_SECONDS;
                executionInfo.EstimatedRemainingTime = new Duration
                {
                    Seconds = (long)remainingTime,
                    Nanos = (int)((remainingTime - Math.Floor(remainingTime)) * Math.Pow(10, 9))
                };
            }

            executionInfo.CommandStatus = ExecutionInfo.Types.CommandStatus.FinishedSuccessfully;
            progress.Report(executionInfo);
        }

        public void Dispose()
        {
            _controlTemperatureCommandManager.Dispose();
        }
    }
}
