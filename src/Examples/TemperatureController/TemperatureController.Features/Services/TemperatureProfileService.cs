﻿using Microsoft.Extensions.Logging;
using SiLA2.Database.SQL;
using System;
using System.Linq;
using System.Threading.Tasks;
using TemperatureController.Features.Database;

namespace TemperatureController.Features.Services
{
    public class TemperatureProfileService : ITemperatureProfileService
    {
        private readonly IRepository<TemperatureProfile> _temperatureProfileServiceRepository;
        private readonly ILogger<TemperatureProfileService> _logger;

        public TemperatureProfileService(IRepository<TemperatureProfile> temperatureProfileServiceRepository, ILogger<TemperatureProfileService> logger)
        {
            _temperatureProfileServiceRepository = temperatureProfileServiceRepository;
            _logger = logger;
        }

        
        public Task Execute(TemperatureProfile temperatureProfile, Action<double> changeTemperature)
        {
            return Task.Run(() => changeTemperature);
            //throw new NotImplementedException();
        }

        public async Task<TemperatureProfile> GetTemperatureProfile(int id)
        {
            return await _temperatureProfileServiceRepository.GetById(id);
        }

        public IQueryable<TemperatureProfile> GetTemperatureProfiles()
        {
            return _temperatureProfileServiceRepository.Table;
        }

        public async Task InsertTemperatureProfile(TemperatureProfile temperatureProfile)
        {
            await _temperatureProfileServiceRepository.Insert(temperatureProfile);
        }

        public async Task UpdateTemperatureProfile(TemperatureProfile temperatureProfile)
        {
            await _temperatureProfileServiceRepository.Update(temperatureProfile);
        }

        public async Task DeleteTemperatureProfile(TemperatureProfile temperatureProfile)
        {
            await _temperatureProfileServiceRepository.Delete(temperatureProfile);
        }

    }
}
