﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2.Client;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.Network;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SiLA2.Temperature.Service.Client.App
{
    class Program
    {
        private static IConfigurationRoot _configuration;

        static async Task Main(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            _configuration = configBuilder.Build();

            var clientSetup = new Configurator(_configuration, args);

            Console.WriteLine("Starting Server Discovery...");

            var serverMap = await clientSetup.SearchForServers();

            GrpcChannel channel;
            var serverType = "SiLA2TemperatureServer";
            var server = serverMap.Values.FirstOrDefault(x => x.Info.Type == serverType);
            if (server != null)
            {
                Console.WriteLine($"Connecting to {server}");
                channel = await clientSetup.GetChannel(server.Config.FQHN, server.Config.Port, acceptAnyServerCertificate: true);
            }
            else
            {
                var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
                Console.WriteLine($"No connection automatically discovered. Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
                channel = await clientSetup.GetChannel(acceptAnyServerCertificate: true);
            }

            Console.WriteLine("Trying to setup Client ...");
            var clientImpl = new TemperatureControllerClientImpl(channel, clientSetup.ServiceProvider.GetService<ILoggerFactory>());
            Console.WriteLine($"Client Setup successful for following Clients :");
            foreach (var client in clientImpl.Clients)
                Console.WriteLine($"{client.GetType().FullName}");

            // check range validation
            const double targetTemperatureOutOfRangeKelvin = 373.15;
            Console.WriteLine($"\nCalling \"ControlTemperature({targetTemperatureOutOfRangeKelvin})\" ...");
            clientImpl.ControlTemperature(targetTemperatureOutOfRangeKelvin).Wait();
            
            Thread.Sleep(1000);

            // start temperature control and getting the result without waiting for the execution to be finished
            const double targetTemperatureCelsius = 30;
            Console.WriteLine($"\nCalling \"ControlTemperature({targetTemperatureCelsius} °C)\" and ControlTemperature_Result() right afterwards");
            clientImpl.ControlTemperatureNoWait(targetTemperatureCelsius.DegreeCelsius2Kelvin());
            
            Thread.Sleep(1000);

            // start temperature control with valid conditions
            Console.WriteLine($"\nCalling \"ControlTemperature({targetTemperatureCelsius} °C)\" ...");
            clientImpl.ControlTemperature(targetTemperatureCelsius.DegreeCelsius2Kelvin(), false).Wait();
            
            Thread.Sleep(1000);

            // subscribe to temperature property for 5 seconds
            Console.WriteLine("\nCalling \"Subscribe_CurrentTemperature()\" for 5 seconds ...");
            clientImpl.GetCurrentTemperature(new TimeSpan(0, 0, 5)).Wait();
            
            Thread.Sleep(1000);

            // subscribe to temperature property for receiving 3 values
            Console.WriteLine("\nCalling \"Subscribe_CurrentTemperature()\" for receiving 3 values ...");
            clientImpl.GetCurrentTemperature(3).Wait();
            
            Thread.Sleep(1000);

            // start another temperature control with running property outputs
            Console.WriteLine("\nCalling \"ControlTemperature(45 °C)\" ...");
            clientImpl.ControlTemperature(45.0.DegreeCelsius2Kelvin()).Wait();

            Thread.Sleep(1000);
            
            Console.WriteLine("Shutting down connection...");
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");

            Console.ReadKey();
        }
    }
}
