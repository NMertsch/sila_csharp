﻿const temperatureprogresschart = 'temperature-progress-chart';
const progressChart = '#progress-chart';
var temperatureChart;

//### ProgressChart ##########################################################################
function generateChart(werte) {
    if (werte === undefined)
        werte = [];
    createProgressChart(progressChart, getProgressChartOptions(temperatureprogresschart, werte));
}

function refreshChart(werte) {
    updateProgressChart(temperatureprogresschart, werte);
}

function getProgressChartOptions(chartid, items) {
    var options = {
        annotations: {
            yaxis: [{
                y: 150,
                borderColor: '#999',
                label: {
                    show: true,
                    text: '',
                    style: {
                        color: "#fff",
                        background: '#00E396'
                    }
                }
            }]
        },
        xaxis: {
            tickAmount: 6
        },
        chart: {
            id: chartid,
            type: 'area',
            height: 700,
            background: '#ffffcc'
        },
        dataLabels: {
            enabled: false
        },
        series: [{ data: items, name: "Progress" }],

        markers: {
            size: 0,
            style: 'hollow'
        },
        stroke: {
            curve: 'smooth'
        },
        title: {
            text: 'Temperature Progress Chart',
            align: 'left'
        },
        tooltip: {
            y: {
                formatter: function (value, { series, seriesIndex, dataPointIndex, w }) {
                    return value;
                }
            }
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                opacityFrom: 0.7,
                opacityTo: 0.9,
                stops: [0, 100]
            }
        }
    };

    return options;
}

function createProgressChart(element, options) {
    destroyChart();
    temperatureChart = new ApexCharts(document.querySelector(element), options);
    temperatureChart.render();
}

function updateProgressChart(chartid, items) {
    if (items === undefined)
        return;
    ApexCharts.exec(chartid, 'updateSeries', [{ data: items, name: 'Temperature Values' }]);
    //ApexCharts.exec(chartid, 'updateOptions', {
    //    xaxis: {
    //        min: new Date(items[items.length - 1][0]).getTime(),
    //        max: new Date(items[0][0]).getTime()
    //    }
    //}, true, true, true);
    ApexCharts.exec(chartid, 'render');
}

function destroyChart() {
    if (temperatureChart !== undefined) {
        temperatureChart.destroy();
    }
}
    //############################################################################################