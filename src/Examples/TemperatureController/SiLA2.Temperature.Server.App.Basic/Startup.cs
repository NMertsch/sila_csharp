﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SiLA2.AspNetCore;
using SiLA2.Commands;
using SiLA2.Network;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server;
using SiLA2.Server.Interceptors;
using SiLA2.Server.Services;
using SiLA2.Simulation;
using SiLA2.Utils;
using SiLA2.Utils.Config;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using System;
using TemperatureController.Features.Services;

namespace SiLA2.Temperature.Server.Basic.App
{
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc(options =>
            {
                options.EnableDetailedErrors = true;
                options.Interceptors.Add<LoggingInterceptor>();
                options.Interceptors.Add<MetadataValidationInterceptor>();
                options.Interceptors.Add<ParameterValidationInterceptor>();
            });
            services.AddSingleton<MetadataManager>();
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<IThermostatSimulator, ThermostatSimulator>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddSingleton<MetadataManager>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.ConfigureWritable<ServerConfig>(_configuration.GetSection("ServerConfig"));
            services.AddSingleton<TemperatureControllerService>();
            services.AddSingleton<ICertificateProvider, CertificateProvider>();
            services.AddSingleton<CertificateContext>();
            services.AddSingleton<ICertificateRepository, CertificateRepository>();
            services.AddSingleton<IServerConfig>(new ServerConfig(_configuration["ServerConfig:Name"],
                                                                Guid.Parse(_configuration["ServerConfig:UUID"]),
                                                                _configuration["ServerConfig:FQHN"],
                                                                int.Parse(_configuration["ServerConfig:Port"]),
                                                                _configuration["ServerConfig:NetworkInterface"],
                                                                _configuration["ServerConfig:DiscoveryServiceName"]));
        }

        //Note: TemperatureControllerService _ is injected for calling its constructor in which the SilaFeature is added to the SiLA2.Server feature collection
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ISiLA2Server siLA2Server, ServerInformation serverInformation, TemperatureControllerService _, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<SiLAService>();
                endpoints.MapGrpcService<TemperatureControllerService>();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });

            logger.LogInformation($"{serverInformation}");
            logger.LogInformation("Starting Server Announcement...");
            siLA2Server.Start();
            logger.LogInformation($"{DateTime.Now} : Started {GetType().Assembly.FullName}");
        }
    }
}
