using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using SiLA2.AspNetCore;
using SiLA2.Server;

namespace SiLA2.Temperature.Server.Basic.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel(args.SetupKestrel());
                    webBuilder.UseStartup<Startup>();
                });
    }
}
