using Microsoft.Extensions.Logging;
using Sila2.Examples.MockShaker;
using System;
using System.Threading.Tasks;

namespace SiLA2.Simulation
{
    public enum ClampState
    {
        Closed,
        Open
    }

    [Serializable]
    public class ClampNotClosedException : Exception
    {
        public ClampNotClosedException(string message)
            : base(message)
        {
        }
    }

    public class ShakerSimulator : IShakerSimulator
    {
        private bool shaking;
        private ClampState clampState;

        public const int MaxShakingDuration = 5; //[mins]
        private readonly ILogger<ShakerSimulator> _logger;
        public double Progress { get; set; }
        public TimeSpan RemainingTime { get; set; }

        public bool IsShaking
        {
            get => shaking;
        }

        public ClampState ClampState
        {
            get => clampState;
            set
            {
                _logger.LogInformation($"ClampState: {value}");
                clampState = value;
            }
        }

        public ShakerSimulator(ILogger<ShakerSimulator> logger)
        {
            _logger = logger;
        }

        public void Stop()
        {
            shaking = false;
        }

        /// <summary>
        /// Simulates shake operation for a specified time period
        /// </summary>
        /// <param name="duration">max shaking duration of 1min</param>
        /// <returns></returns>
        public async Task Shake(TimeSpan duration)
        {
            if (duration > TimeSpan.FromMinutes(MaxShakingDuration))
            {
                throw new Exception($"Specified duration {duration} exceeds max duration ");
            }
            // check if clamp is closed
            if (ClampState != ClampState.Closed)
            {
                throw new ClampNotClosedException("Unable to shake with opened clamp");
            }

            _logger.LogInformation("Shake it..");
            shaking = true;
            var startTime = DateTime.Now;
            var endTime = startTime + duration;
            var currentTime = startTime;
            while (currentTime < endTime && shaking)
            {
                _logger.LogInformation("Shake shake it!");
                Progress = (currentTime - startTime).Seconds / duration.Seconds;
                RemainingTime = endTime - currentTime;
                await Task.Delay(300);
                _logger.LogInformation("Shake it..");
                await Task.Delay(300);
                currentTime = DateTime.Now;
            }

            _logger.LogInformation("Shaking it like a polaroid picture!");
        }
    }
}