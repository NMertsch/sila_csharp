﻿using SiLA2.Simulation;
using System;
using System.Threading.Tasks;

namespace Sila2.Examples.MockShaker
{
    public interface IShakerSimulator
    {
        ClampState ClampState { get; set; }
        bool IsShaking { get; }
        double Progress { get; set; }
        TimeSpan RemainingTime { get; set; }

        Task Shake(TimeSpan duration);
        void Stop();
    }
}