﻿namespace ShakerController.Features.Services
{
    using Grpc.Core;
    using Microsoft.Extensions.Logging;
    using Sila2.De.Equicon.Handling.Platehandlingcontrol.V1;
    using Sila2.Examples.MockShaker;
    using SiLA2.Server;
    using SiLA2.Server.Utils;
    using SiLA2.Simulation;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using SiLAFramework = Sila2.Org.Silastandard;

    public class PlateHandlingService : PlateHandlingControl.PlateHandlingControlBase
    {
        private readonly IShakerSimulator _shakerSimulator;
        private readonly ILogger<PlateHandlingService> _logger;

        public PlateHandlingService(IShakerSimulator shakerSimulator, ISiLA2Server silaServer, ILogger<PlateHandlingService> logger)
        {
            silaServer.ReadFeature(Path.Combine("Features", "PlateHandlingControl-v1_0.sila.xml"));
            _shakerSimulator = shakerSimulator;
            _logger = logger;
        }

        #region Overrides of PlateHandlingControlBase
        public override Task<OpenClamp_Responses> OpenClamp(OpenClamp_Parameters request, ServerCallContext context)
        {
            // check if currently shaking
            if (_shakerSimulator.IsShaking)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError("Clamp can not be opened because device is currently shaking. Wait for the device to finish shaking."));
            }

            // simulate clamp opening
            Thread.Sleep(1000);

            _shakerSimulator.ClampState = ClampState.Open;

            return Task.FromResult(new OpenClamp_Responses());
        }

        public override Task<CloseClamp_Responses> CloseClamp(CloseClamp_Parameters request, ServerCallContext context)
        {
            // check if currently shaking
            if (_shakerSimulator.IsShaking)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError("Clamp can not be closed because device is currently shaking. Wait for the device to finish shaking"));
            }

            // simulate clamp closing
            Thread.Sleep(1000);

            _shakerSimulator.ClampState = ClampState.Closed;
            return Task.FromResult(new CloseClamp_Responses());
        }

        public override Task<Get_CurrentClampState_Responses> Get_CurrentClampState(Get_CurrentClampState_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_CurrentClampState_Responses { CurrentClampState = new SiLAFramework.Boolean { Value = _shakerSimulator.ClampState == ClampState.Open } });
        }

        #endregion
    }
}