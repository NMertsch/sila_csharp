using SiLA2.Server;

namespace ShakerController.Features.Services
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml;
    using Grpc.Core;
    using SiLA2.Commands;
    using Microsoft.Extensions.Logging;
    using SiLA2.Server.Utils;
    using SiLA2.Simulation;
    using Sila2.Examples.MockShaker;
    using Sila2.De.Equicon.Mixing.Shakingcontrol.V1;
    using Sila2.Org.Silastandard;
    using System.IO;

    public class ShakingService : ShakingControl.ShakingControlBase
    {
        private readonly IShakerSimulator _shakerSimulator;
        private readonly ILogger<ShakingService> _logger;
        private readonly SiLA2.Feature _silaFeature;
        private readonly IObservableCommandManager<Shake_Parameters, Shake_Responses> _shakeCommandManager;

        public ShakingService(ISiLA2Server silaServer, IObservableCommandManager<Shake_Parameters, Shake_Responses> observableCommandManager, IShakerSimulator shakerSimulator, ILogger<ShakingService> logger)
        {
            _silaFeature = silaServer.ReadFeature(Path.Combine("Features", "ShakingControl-v1_0.sila.xml"));

            _shakerSimulator = shakerSimulator;
            _logger = logger;
            _shakeCommandManager = observableCommandManager;
        }

        public override Task<StopShaking_Responses> StopShaking(StopShaking_Parameters request, ServerCallContext context)
        {
            _shakerSimulator.Stop();
            return Task.FromResult(new StopShaking_Responses());
        }

        public override async Task<CommandConfirmation> Shake(Shake_Parameters request, ServerCallContext context)
        {
            var command = await _shakeCommandManager.AddCommand(request, ShakeTask());
            return command.Confirmation;
        }

        public override async Task Shake_Info(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _shakeCommandManager.RegisterForInfo(cmdExecId, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                _logger.LogError($"{e}");
                throw;
            }
        }

        public override Task<Shake_Responses> Shake_Result(CommandExecutionUUID cmdExecId, ServerCallContext context)
        {
            var command = _shakeCommandManager.GetCommand(cmdExecId);
            return Task.FromResult(command.Result);
        }

        private Func<IProgress<ExecutionInfo>, Shake_Parameters, CancellationToken, Shake_Responses> ShakeTask()
        {
            return (progress, parameters, cancellationToken) =>
            {
                // check duration parameter
                var duration = TimeSpan.Zero;
                try
                {
                    duration = XmlConvert.ToTimeSpan(parameters.Duration.Value);
                }
                catch
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                        _silaFeature.GetFullyQualifiedCommandParameterIdentifier("Shake", "Duration"),
                        "Invalid duration notation. Specify the duration in ISO 8601 notation"));
                }

                try
                {
                    ReportProgress(progress, cancellationToken).Wait();
                    _shakerSimulator.Shake(duration).Wait(cancellationToken);
                }
                catch (Exception e)
                {
                    _logger.LogWarning($"{e}");
                    if (e.InnerException is ClampNotClosedException)
                    {
                        ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError("Clamp Not Closed: " + e.Message));
                    }

                    throw;
                }

                return new Shake_Responses();
            };
        }

        private async Task ReportProgress(IProgress<ExecutionInfo> progress, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Starting progress reporting...");
            var executionInfo = new ExecutionInfo();
            while (cancellationToken.IsCancellationRequested)
            {
                executionInfo.ProgressInfo = new Real { Value = _shakerSimulator.Progress };
                var remainingTime = _shakerSimulator.RemainingTime;
                executionInfo.EstimatedRemainingTime = new Duration
                { Seconds = (long)remainingTime.TotalSeconds, Nanos = remainingTime.Milliseconds * 100000 };
                progress.Report(executionInfo);
                await Task.Delay(2000, cancellationToken);
            }

            _logger.LogDebug("Ending progress reporting");
        }
    }
}