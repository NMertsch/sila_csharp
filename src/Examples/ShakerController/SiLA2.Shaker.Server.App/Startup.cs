using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sila2.Examples.MockShaker;
using SiLA2.Commands;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Simulation;
using ShakerController.Features.Services;
using SiLA2.Frontend.Razor.Services;
using SiLA2.Database.SQL;
using Microsoft.EntityFrameworkCore;
using SiLA2.Utils.Network;
using SiLA2.Utils.gRPC;
using SiLA2.Frontend.Razor.Services.UserManagement;
using SiLA2.Frontend.Razor.Services.UserManagement.Domain;
using System;
using System.IO;
using System.Reflection;
using SiLA2.Utils.Security;
using SiLA2.Frontend.Razor;
using SiLA2.Server.Interceptors;
using SiLA2.Utils.Config;
using SiLA2.AspNetCore;

namespace SiLA2.Shaker.Server.App
{
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc(options =>
            {
                options.EnableDetailedErrors = true;
                options.Interceptors.Add<LoggingInterceptor>();
                options.Interceptors.Add<MetadataValidationInterceptor>();
                options.Interceptors.Add<ParameterValidationInterceptor>();
            });
            services.AddSingleton<MetadataManager>();
            services.AddGrpcReflection();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddDbContext<IDbUserContext, UserDbContext>(x => x.UseSqlite(_configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<IRepository<User>, UserRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddScoped<IGrpcClientProvider, GrpcClientProvider>();
            services.AddScoped<IFeatureMapper, FeatureMapper>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.AddSingleton<IShakerSimulator, ShakerSimulator>();
            services.AddSingleton<PlateHandlingService>();
            services.AddSingleton<ShakingService>();
            services.AddSingleton<ICertificateProvider, CertificateProvider>();
            services.AddSingleton<CertificateContext>();
            services.AddSingleton<ICertificateRepository, CertificateRepository>();
            services.AddScoped<FileJsInterop>();
            services.AddSingleton<LockControllerService>();
            services.AddSingleton<ErrorRecoveryServiceImpl>();
            services.AddSingleton<IServerConfig>(new ServerConfig(_configuration["ServerConfig:Name"],
                                                                Guid.Parse(_configuration["ServerConfig:UUID"]),
                                                                _configuration["ServerConfig:FQHN"],
                                                                int.Parse(_configuration["ServerConfig:Port"]),
                                                                _configuration["ServerConfig:NetworkInterface"],
                                                                _configuration["ServerConfig:DiscoveryServiceName"]));
#if DEBUG
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
            services.ConfigureWritable<ServerConfig>(_configuration.GetSection("ServerConfig"), configFile);
        }

        //Note: PlateHandlingService _1, ShakingService _2 are injected for calling their constructors in which the SilaFeature are added to the SiLA2.Server feature collection
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ISiLA2Server siLA2Server, ServerInformation serverInformation, PlateHandlingService _1, ShakingService _2, ILogger<Startup> logger)
        {
            SetupSiLA2CoreFeatures(siLA2Server);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<SiLAService>();
                endpoints.MapGrpcService<LockControllerService>();
                endpoints.MapGrpcService<SiLABinaryDownloadService>();
                endpoints.MapGrpcService<SiLABinaryUploadService>();
                endpoints.MapGrpcService<PlateHandlingService>();
                endpoints.MapGrpcService<ShakingService>();

                endpoints.MapGrpcReflectionService();

                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
                endpoints.MapControllerRoute("Fallback",
                          "{controller}/{action}/{id?}",
                          new { controller = "App", action = "Index" });
            });

            logger.LogInformation($"{serverInformation}");
            logger.LogInformation("Starting Server Announcement...");
            siLA2Server.Start();
            logger.LogInformation($"{DateTime.Now} : Started {GetType().Assembly.FullName}");
        }

        //Be sure having registered the Service in the IoC-Container and copied the Feature files into application directory !
        private void SetupSiLA2CoreFeatures(ISiLA2Server siLA2Server)
        {
            siLA2Server.ReadFeature("LockController-v2_0.sila.xml", typeof(LockControllerService));
            siLA2Server.ReadFeature("ErrorRecoveryService-v1_0.sila.xml", typeof(ErrorRecoveryServiceImpl));
        }
    }
}
