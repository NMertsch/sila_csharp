using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using SiLA2.AspNetCore;

namespace SiLA2.Shaker.Server.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStaticWebAssets();
                    webBuilder.ConfigureKestrel(args.SetupKestrel());
                    webBuilder.UseStartup<Startup>();
                });
    }
}
