﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2.Client;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using static Sila2.Org.Silastandard.Core.Connectionconfigurationservice.V1.ConnectionConfigurationService;
using String = Sila2.Org.Silastandard.String;
using Sila2.Org.Silastandard.Core.Connectionconfigurationservice.V1;
using Sila2.Org.Silastandard;
using Makaretu.Dns;

namespace SiLA2.ServerInitiatedConnection.ClientApp
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            var clientSetup = new Configurator(configuration, args);
            ILogger<Program> logger = (ILogger<Program>)clientSetup.ServiceProvider.GetService(typeof(ILogger<Program>));


            var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
            Console.WriteLine($"Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
            GrpcChannel channel = await ((IGrpcChannelProvider)clientSetup.ServiceProvider.GetService(typeof(IGrpcChannelProvider))).GetChannel(clientConfig.IpOrCdirOrFullyQualifiedHostName, clientConfig.Port, accceptAnyServerCertificate: true);

            var client = new ConnectionConfigurationServiceClient(channel);
            var result = client.Get_ConfiguredSiLAClients(new Get_ConfiguredSiLAClients_Parameters());

            Console.WriteLine("Connecting SiLA2 Clients configured for Server Initiated Connection...");
            
            List<Task> tasks = new();
            foreach (var item in result.ConfiguredSiLAClients)
            {
                try
                {
                    var task = Task.Run(() => {
                        var request = new ConnectSiLAClient_Parameters
                        {
                            ClientName = new String { Value = $"{item.SiLAClientHost}:{item.SiLAClientPort}" },
                            SiLAClientHost = item.SiLAClientHost,
                            SiLAClientPort = item.SiLAClientPort,
                            Persist = new Sila2.Org.Silastandard.Boolean { Value = true }
                        };
                        Console.WriteLine($"Connecting Client: {item}");
                        client.ConnectSiLAClient(request);
                    });
                    tasks.Add(task);
                }
                catch
                {
                    throw;
                }
            }

            await Task.WhenAll(tasks);

            Console.WriteLine("Client work finished...press any key to close console...");
            Console.ReadKey();
        }
    }
}