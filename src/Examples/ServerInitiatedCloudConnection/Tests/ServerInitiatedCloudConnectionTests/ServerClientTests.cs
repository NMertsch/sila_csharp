

using SiLA2.Server.Tests;
using ServerApp = SiLA2.ServerInitiatedCloudConnection.ServerApp;
using ClientApp = SiLA2.ServerInitiatedCloudConnection.ClientApp;
using static Sila2.Org.Silastandard.Core.Connectionconfigurationservice.V1.ConnectionConfigurationService;
using Sila2.Org.Silastandard.Core.Connectionconfigurationservice.V1;
using Sila2.Org.Silastandard;
using static Sila2.Org.Silastandard.CloudClientEndpoint;
using Google.Protobuf;
using Grpc.Core;
using Sila2.Org.Silastandard.Test.Observablecommandtest.V1;
using SiLA2.Utils.gRPC;
using SiLA2.ServerInitiatedCloudConnection.ServerApp;
using Microsoft.Extensions.DependencyInjection;
using SiLA2.Server.Services;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.DataCollection;
using System.Reflection.PortableExecutable;
using System.Threading;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Hosting.Server;
using static Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using static Sila2.Org.Silastandard.ClientMessageSender;
using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;

namespace ServerInitiatedCloudConnectionTests;

[TestFixture, Ignore("Work in progress..")]
public class Tests
{
    ConnectionConfigurationServiceClient _connConfigServiceClient;
    CloudClientEndpointClient _cloudClientEndpointClient;
    GrpcChannelProvider _channelProvider;
    ClientMessageSenderClient _messageSenderClient;

    //private TestServerFixture<Startup> _server;
    //private TestServerFixture<ClientApp.Startup> _client;
    GrpcChannel _channelServer;
    GrpcChannel _channelClient;

    [OneTimeSetUp]
    public async Task SetupOnce()
    {
        var args = new string[] { };
        //_server = new TestServerFixture<ServerApp.Startup>(args, 5008);
        //_client = new TestServerFixture<ClientApp.Startup>(args, 5009);
        _channelServer = await new GrpcChannelProvider(null).GetChannel("localhost", 5008, true);
        _channelClient = await new GrpcChannelProvider(null).GetChannel("localhost", 5009, true);
        _connConfigServiceClient = new ConnectionConfigurationServiceClient(_channelServer);
        _cloudClientEndpointClient = new CloudClientEndpointClient(_channelClient);
        _messageSenderClient = new ClientMessageSenderClient(_channelClient);
    }

    [Test]
    public void CloudClient_could_be_found()
    {
        var response = _connConfigServiceClient.Get_ConfiguredSiLAClients(new Get_ConfiguredSiLAClients_Parameters());
        Assert.NotNull(response);
        Assert.NotNull(response.ConfiguredSiLAClients);
        Assert.That(response.ConfiguredSiLAClients, Has.Count.EqualTo(1));
    }

    [Test]
    public void ServerInitiatedConnectionStatus_could_be_changed()
    {
        var response = _connConfigServiceClient.Get_ServerInitiatedConnectionModeStatus(new Get_ServerInitiatedConnectionModeStatus_Parameters());
        Assert.False(response.ServerInitiatedConnectionModeStatus.Value);
        var enableResponse = _connConfigServiceClient.EnableServerInitiatedConnectionMode(new EnableServerInitiatedConnectionMode_Parameters());

        response = _connConfigServiceClient.Get_ServerInitiatedConnectionModeStatus(new Get_ServerInitiatedConnectionModeStatus_Parameters());
        Assert.True(response.ServerInitiatedConnectionModeStatus.Value);
    }

    [Test, Ignore("Decide if we need this...")]
    public void Should_return_connected_clients()
    {
        var responseConected = _connConfigServiceClient.Get_ConnectedSiLAClients(new Get_ConnectedSiLAClients_Parameters());
        Assert.NotNull(responseConected);
        Assert.That(responseConected.ConnectedSiLAClients, Has.Count.EqualTo(0));

        var responseConfigured = _connConfigServiceClient.Get_ConfiguredSiLAClients(new Get_ConfiguredSiLAClients_Parameters());
        var client = responseConfigured.ConfiguredSiLAClients[0];
        var requestConnect = new ConnectSiLAClient_Parameters
        {
            ClientName = new Sila2.Org.Silastandard.String { Value = $"{client.SiLAClientHost}:{client.SiLAClientPort}" },
            SiLAClientHost = client.SiLAClientHost,
            SiLAClientPort = client.SiLAClientPort,
            Persist = new Sila2.Org.Silastandard.Boolean { Value = true }
        };

        var connectClientResponse = _connConfigServiceClient.ConnectSiLAClient(requestConnect);
        responseConected = _connConfigServiceClient.Get_ConnectedSiLAClients(new Get_ConnectedSiLAClients_Parameters());
        Assert.NotNull(responseConected);
        Assert.That(responseConected.ConnectedSiLAClients, Has.Count.EqualTo(1));

        var disconnectRequest = new DisconnectSiLAClient_Parameters()
        {
            ClientName = client.ClientName
        };

        var disconnectClientResponse = _connConfigServiceClient.DisconnectSiLAClient(disconnectRequest);
        responseConected = _connConfigServiceClient.Get_ConnectedSiLAClients(new Get_ConnectedSiLAClients_Parameters());
        Assert.That(responseConected.ConnectedSiLAClients, Has.Count.EqualTo(0));
    }

    [Test]
    public async Task Server_should_connect_to_client()
    {
        var cts = new CancellationTokenSource();
        cts.CancelAfter(TimeSpan.FromSeconds(1));
        var options = new CallOptions(null, null, cts.Token);
        var asyncDuplexStreamingCall = _cloudClientEndpointClient.ConnectSiLAServer(options);
        //var msg = new UnobservablePropertyValue { Value = ByteString.CopyFromUtf8("Message from Server Stream") };
        //await asyncDuplexStreamingCall.RequestStream.WriteAsync(new SiLAServerMessage { UnobservablePropertyValue = msg });
        //await Task.Delay(1000);
        Assert.IsNotNull(asyncDuplexStreamingCall.ResponseStream);
        //await asyncDuplexStreamingCall.ResponseStream.MoveNext();
        //Assert.IsNotNull(asyncDuplexStreamingCall.ResponseStream.Current);
    }

    //TODO: Work with/Implement SiLA2.Server.Services.SiLAClientMessageService & SiLA2.Server.Services.SiLAServerMessageService
    [Test]
    public async Task ServerInitiated_duplex_stream()
    {
        // Arrange
        var clientConnectRequest = new ConnectSiLAClient_Parameters
        {
            ClientName = new Sila2.Org.Silastandard.String { Value = $"CloudClient" },
            SiLAClientHost = new Sila2.Org.Silastandard.String { Value = "localhost" },
            SiLAClientPort = new Sila2.Org.Silastandard.Integer { Value = 5009 },
            Persist = new Sila2.Org.Silastandard.Boolean { Value = true }
        };
        var clientResponse = _connConfigServiceClient.ConnectSiLAClient(clientConnectRequest);

        var requestId = Guid.NewGuid().ToString();
        //var clientMessageService = _client.ServiceProvider.GetService<ISiLAClientMessageService>();
        //Assert.IsNotNull(clientMessageService);
        //var unobservablePropertyRead = new UnobservablePropertyRead { FullyQualifiedPropertyId = "org.silastandard/core/SiLAService/v1/Property/ServerName" };

        //var serverMessageService = _server.ServiceProvider.GetService<ISiLAServerMessageService>();
        //Assert.IsNotNull(serverMessageService);
        //Assert.That(serverMessageService.ServerMessageResponses.Count(), Is.EqualTo(0));

        //await clientMessageService.AddClientMessageRequest(new SiLAClientMessage { RequestUUID = requestId, UnobservablePropertyRead = unobservablePropertyRead });
        //await Task.Delay(3000);
        //Assert.That(serverMessageService.ServerMessageResponses.Count(), Is.EqualTo(1));

        // System under Test

        // Act

        // Assert
        Assert.Pass();
    }

    [Test]
    public async Task ServerInitiated_duplex_stream3()
    {
        var requestConnect = new ConnectSiLAClient_Parameters
        {
            ClientName = new Sila2.Org.Silastandard.String { Value = $"CloudClient" },
            SiLAClientHost = new Sila2.Org.Silastandard.String { Value = "localhost" },
            SiLAClientPort = new Sila2.Org.Silastandard.Integer { Value = 5009 },
            Persist = new Sila2.Org.Silastandard.Boolean { Value = true }
        };

        await Task.Delay(6000);
        var cts = new CancellationTokenSource();
        cts.CancelAfter(5000);
        var connectTask = Task.Run(() =>
        {
            var connectResponse = _connConfigServiceClient.ConnectSiLAClient(requestConnect/*, null, null, cts.Token*/);
        });
        //Assert.NotNull(connectResponse);

        var message = new SiLAClientMessage()
        {
            UnobservablePropertyRead = new UnobservablePropertyRead
            {
                FullyQualifiedPropertyId = "org.silastandard/core/SiLAService/v1/Property/ServerName"
            }
        };

        _messageSenderClient.SendSiLAClientMessage(message);
        await Task.Delay(500);

        //var cloudEndpointClient = new CloudClientEndpointClient(_channelClient);
        //var duplexStream = cloudEndpointClient.ConnectSiLAServer();

        //await foreach (SiLAClientMessage? response in duplexStream.ResponseStream.ReadAllAsync())
        //{
        //    if (response.UnobservablePropertyRead != null)
        //    {
        //        var propertyName = response.UnobservablePropertyRead.FullyQualifiedPropertyId;
        //        var silaService = new SiLAServiceClient(_channelServer);
        //        var serverName = silaService.Get_ServerName(new Get_ServerName_Parameters());
        //        var serverMessage = new SiLAServerMessage()
        //        {
        //            UnobservablePropertyValue = new UnobservablePropertyValue { Value = ByteString.CopyFromUtf8(serverName.ServerName.Value) }
        //        };
        //        //var serverMessage = new SiLAServerMessage()
        //        //{
        //        //    UnobservablePropertyValue = new UnobservablePropertyValue { Value = ByteString.CopyFromUtf8("I am the server and this is my name") }
        //        //};
        //        await duplexStream.RequestStream.WriteAsync(serverMessage);
        //    }
        //}
    }


    [Test, Ignore("Reactivate later...")]
    public async Task ServerInitiated_read_unobservable_property()
    {
        var responseConfigured = _connConfigServiceClient.Get_ConfiguredSiLAClients(new Get_ConfiguredSiLAClients_Parameters());
        var configuredCloudClient = responseConfigured.ConfiguredSiLAClients[0];
        var requestConnect = new ConnectSiLAClient_Parameters
        {
            ClientName = new Sila2.Org.Silastandard.String { Value = $"{configuredCloudClient.SiLAClientHost}:{configuredCloudClient.SiLAClientPort}" },
            SiLAClientHost = configuredCloudClient.SiLAClientHost,
            SiLAClientPort = configuredCloudClient.SiLAClientPort,
            Persist = new Sila2.Org.Silastandard.Boolean { Value = true }
        };

        // initiate connectection by server
        _connConfigServiceClient.ConnectSiLAClient(requestConnect);
        var responseConected = _connConfigServiceClient.Get_ConnectedSiLAClients(new Get_ConnectedSiLAClients_Parameters());
        var connectedClient = responseConected.ConnectedSiLAClients[0];

        var channel = await _channelProvider.GetChannel(connectedClient.SiLAClientHost.Value, unchecked((int)connectedClient.SiLAClientPort.Value), true);
        var cloudClient = new CloudClientEndpoint.CloudClientEndpointClient(channel);

        var duplexStream = cloudClient.ConnectSiLAServer();
        Console.WriteLine("Starting background task to receive messages");
        var readTask = Task.Run(async () =>
        {
            await foreach (SiLAClientMessage? response in duplexStream.ResponseStream.ReadAllAsync())
            {
                Console.WriteLine(response.UnobservablePropertyRead.FullyQualifiedPropertyId);
                // Echo messages sent to the service

                Assert.That(response.UnobservablePropertyRead.FullyQualifiedPropertyId, Is.EqualTo("sila2.org.silastandard.CloudClientEndpoint"));
            }
        });

        Console.WriteLine("Starting to send messages");
        var cts = new CancellationTokenSource();
        cts.CancelAfter(3000);
        do
        {
            var serverMessage = new SiLAServerMessage()
            {
                UnobservablePropertyValue = new UnobservablePropertyValue()
                {
                    Value = ByteString.CopyFromUtf8("org.silastandard/core/SiLAService/v1/Property/ServerName")
                }
            };
            await duplexStream.RequestStream.WriteAsync(serverMessage);
        } while (!cts.IsCancellationRequested);


    }



    //[Test, Ignore("Reactivate later...")]
    //public async Task ServerInitiated_read_observable_property()
    //{
    //    var responseConfigured = _connConfigServiceClient.Get_ConfiguredSiLAClients(new Get_ConfiguredSiLAClients_Parameters());
    //    var configuredCloudClient = responseConfigured.ConfiguredSiLAClients[0];
    //    var requestConnect = new ConnectSiLAClient_Parameters
    //    {
    //        ClientName = new Sila2.Org.Silastandard.String { Value = $"{configuredCloudClient.SiLAClientHost}:{configuredCloudClient.SiLAClientPort}" },
    //        SiLAClientHost = configuredCloudClient.SiLAClientHost,
    //        SiLAClientPort = configuredCloudClient.SiLAClientPort,
    //        Persist = new Sila2.Org.Silastandard.Boolean { Value = true }
    //    };


    //    // initiate connectection by server
    //    _connConfigServiceClient.ConnectSiLAClient(requestConnect);
    //    var responseConected = _connConfigServiceClient.Get_ConnectedSiLAClients(new Get_ConnectedSiLAClients_Parameters());
    //    var connectedClient = responseConected.ConnectedSiLAClients[0];

    //    var channel = await _channelProvider.GetChannel(connectedClient.SiLAClientHost.Value, unchecked((int)connectedClient.SiLAClientPort.Value), true);
    //    var cloudClient = new CloudClientEndpoint.CloudClientEndpointClient(channel);

    //    // create duplex 
    //    var asyncDuplexStreamingCall = cloudClient.ConnectSiLAServer();
    //    var cts = new CancellationTokenSource();
    //    cts.CancelAfter(3000);
    //    var messageFromClient = string.Empty;
    //    do
    //    {
    //        var msg = new ObservablePropertyValue { Value = ByteString.CopyFromUtf8("Message from Server Stream") };
    //        await asyncDuplexStreamingCall.RequestStream.WriteAsync(new SiLAServerMessage { ObservablePropertyValue = msg });

    //        if (asyncDuplexStreamingCall.ResponseStream.Current != null)
    //        {
    //            try
    //            {
    //                //messageFromClient = asyncDuplexStreamingCall.ResponseStream.Current.UnobservablePropertyRead.FullyQualifiedPropertyId;
    //                messageFromClient = asyncDuplexStreamingCall.ResponseStream.Current.ObservablePropertySubscription.FullyQualifiedPropertyId;
    //            }
    //            finally { }
    //        }
    //    }
    //    while (!cts.IsCancellationRequested && await asyncDuplexStreamingCall.ResponseStream.MoveNext(cts.Token));
    //}




    //[Test]
    //public void ServerInitiated_call_method()
    //{

    //}

    //[Test]
    //public void ServerInitiated_call_observable_method()
    //{

    //}
}