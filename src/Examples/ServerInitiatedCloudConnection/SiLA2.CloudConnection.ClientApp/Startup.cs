using SiLA2.Commands;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Network.Discovery;
using SiLA2.Network;
using SiLA2.Server.Interceptors;
using SiLA2.Server.Services;
using SiLA2.Server;
using SiLA2.Utils.Config;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using System.Reflection;
using SiLA2.Utils;
using ClientApp.Features;
using SiLA2.AspNetCore;

namespace SiLA2.ServerInitiatedCloudConnection.ClientApp;

public class Startup
{
    private IConfiguration _configuration { get; }

    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        BootstrapIocContainer(services);
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ISiLA2Server siLA2Server, ServerInformation serverInformation, CloudEndPoint _, ILogger<Startup> logger)
    {
        //siLA2Server.ReadFeature("???.sila.xml", typeof(??));

        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapGrpcService<SiLAService>();
            endpoints.MapGrpcService<CloudEndPoint>();
            endpoints.MapGrpcService<ClientMessageSenderImpl>();
        });

        logger.LogInformation($"{DateTime.Now} : Started SiLA2.ServerInitiatedConnection.ClientApp");
        logger.LogInformation($"{serverInformation}");
    }

    private void BootstrapIocContainer(IServiceCollection services)
    {
        services.AddGrpc(options =>
        {
            options.EnableDetailedErrors = true;
            options.Interceptors.Add<LoggingInterceptor>();
            options.Interceptors.Add<MetadataValidationInterceptor>();
            options.Interceptors.Add<ParameterValidationInterceptor>();
        });
        services.AddSingleton<MetadataManager>();
        services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
        services.AddTransient<INetworkService, NetworkService>();
        services.AddSingleton<IBinaryUploadRepository, BinaryUploadRepository>();
        services.AddSingleton<IBinaryDownloadRepository, BinaryDownloadRepository>();
        services.AddSingleton<ServiceDiscoveryInfo>();
        services.AddSingleton<ServerInformation>();
        services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
        services.AddSingleton<ISiLA2Server, SiLA2Server>();
        services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
        services.AddScoped<IServerDataProvider, ServerDataProvider>();
        services.AddSingleton<ICertificateProvider, CertificateProvider>();
        services.AddSingleton<CertificateContext>();
        services.AddSingleton<ICertificateRepository, CertificateRepository>();
        services.AddSingleton<CloudEndPoint>();
        services.AddSingleton<ISiLAClientMessageService, SiLAClientMessageService>();
        services.AddSingleton<ClientMessageSenderImpl>();
        services.AddSingleton<IServerConfig>(new ServerConfig(_configuration["ServerConfig:Name"],
                                                            Guid.Parse(_configuration["ServerConfig:UUID"]),
                                                            _configuration["ServerConfig:FQHN"],
                                                            int.Parse(_configuration["ServerConfig:Port"]),
                                                            _configuration["ServerConfig:NetworkInterface"],
                                                            _configuration["ServerConfig:DiscoveryServiceName"]));
#if DEBUG
        var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
        var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
        services.ConfigureWritable<ServerConfig>(_configuration.GetSection("ServerConfig"), configFile);
    }
}