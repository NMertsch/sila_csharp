﻿using Grpc.Net.Client;
using Makaretu.Dns;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Core.Connectionconfigurationservice.V1;
using SiLA2.Utils.gRPC;
using static Sila2.Org.Silastandard.ClientMessageSender;
using static Sila2.Org.Silastandard.Core.Connectionconfigurationservice.V1.ConnectionConfigurationService;

namespace SendMessage.ConsoleApp
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            await Task.Delay(6000);

            Console.WriteLine("Hello, World!");
            GrpcChannel channelClient = await new GrpcChannelProvider(null).GetChannel("localhost", 5009, true);
            ClientMessageSenderClient messageSenderClient = new ClientMessageSenderClient(channelClient);

            var requestConnect = new ConnectSiLAClient_Parameters
            {
                ClientName = new Sila2.Org.Silastandard.String { Value = $"CloudClient" },
                SiLAClientHost = new Sila2.Org.Silastandard.String { Value = "localhost" },
                SiLAClientPort = new Sila2.Org.Silastandard.Integer { Value = 5009 },
                Persist = new Sila2.Org.Silastandard.Boolean { Value = true }
            };
            
            var channelServer = await new GrpcChannelProvider(null).GetChannel("localhost", 5008, true);
            var connConfigServiceClient = new ConnectionConfigurationServiceClient(channelServer);

            var cts = new CancellationTokenSource();
            //cts.CancelAfter(TimeSpan.FromSeconds(15));
            var connectTask = Task.Run(() =>
            {
                var connectResponse = connConfigServiceClient.ConnectSiLAClient(requestConnect, null, null, cts.Token);
            });

            var message = new SiLAClientMessage()
            {
                UnobservablePropertyRead = new UnobservablePropertyRead
                {
                    FullyQualifiedPropertyId = "org.silastandard/core/SiLAService/v1/Property/ServerName"
                }
            };

            while (true)
            {
                messageSenderClient.SendSiLAClientMessage(message);
                await Task.Delay(500);
                var line = Console.ReadLine();
                if (line == "s")
                { 
                    cts.Cancel();
                    break;
                }
            };
        }
    }
}