using SiLA2.AspNetCore;
using SiLA2.Server;

namespace SiLA2.ServerInitiatedCloudConnection.ServerApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStaticWebAssets();
                    webBuilder.ConfigureKestrel(args.SetupKestrel());
                    webBuilder.UseStartup<Startup>();
                });
    }
}