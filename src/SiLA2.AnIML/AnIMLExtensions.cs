﻿using AnIMLCore;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SiLA2.AnIML
{
    public static class AnIMLExtensions
    {
        public static SeriesType GetAnIMLSeries(this AnIMLType animl, int experimentStep = 0, int result = 0, int focusedSeries = 0)
        {
            var series = animl.ExperimentStepSet.ExperimentStep[experimentStep].Result[result].SeriesSet.Series[focusedSeries];
            series.Items = new object[] { };
            return series;
        }

        public static SeriesSetType CreateAnIMLSeries(this AnIMLType animl, int numberOfSeries, int experimentStep = 0, int result = 0)
        {
            var list = new List<SeriesType>();
            for (int i = 0; i < numberOfSeries; i++)
            {
                list.Add(new SeriesType());
            }
            var seriesSet = animl.ExperimentStepSet.ExperimentStep[experimentStep].Result[result].SeriesSet;
            seriesSet.Series = list.ToArray();
            return seriesSet;
        }

        public static string GetAnIMLXml(this AnIMLType animl)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(AnIMLType));
            string xml = string.Empty;
            using (var ms = new MemoryStream())
            {
                serializer.Serialize(ms, animl);
                ms.Position = 0;
                xml = Encoding.UTF8.GetString(ms.ToArray());
            }

            // Used to be needed before 09.11.2022
            //string byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            //if (xml.StartsWith(byteOrderMarkUtf8))
            //{
            //    xml = xml.Remove(0, byteOrderMarkUtf8.Length);
            //}

            XDocument.Parse(xml).ValidateAnIMLCoreDocument();

            return xml;
        }

        public static void ValidateAnIMLCoreDocument(this XDocument animl)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var animlCoreSchema = GetXmlSchema(assembly, "animl-core.xsd");
            var xmlDSigSchema = GetXmlSchema(assembly, "xmldsig-core-schema.xsd");

            var schemaSet = new XmlSchemaSet();
            schemaSet.Add(animlCoreSchema);
            schemaSet.Add(xmlDSigSchema);

            animl.Validate(schemaSet, ValidationEventHandler);
        }

        private static XmlSchema GetXmlSchema(Assembly assembly, string resourceFileName)
        {
            var resourceNameAnIMLCore = assembly.GetManifestResourceNames().Single(x => x.EndsWith(resourceFileName));
            
            using (Stream stream = assembly.GetManifestResourceStream(resourceNameAnIMLCore))
            {
                return XmlSchema.Read(stream, ValidationEventHandler);
            }
        }

        private static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            XmlSeverityType type = XmlSeverityType.Warning;
            if (Enum.TryParse("Error", out type))
            {
                if (type == XmlSeverityType.Error) throw new Exception(e.Message);
            }
        }
    }
}
