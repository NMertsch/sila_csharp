﻿using AnIMLCore;

namespace SiLA2.AnIML.Services.Builder
{
    public interface ISeriesTypeBuilder
    {
        AnIMLType Build();
    }
}
