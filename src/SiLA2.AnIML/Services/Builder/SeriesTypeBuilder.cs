﻿using AnIMLCore;

namespace SiLA2.AnIML.Services.Builder
{
    public class SeriesTypeBuilder : ISeriesTypeBuilder
    {
        public AnIMLType Build()
        {
            AnIMLType anIMLType = new AnIMLType { SampleSet = new SampleSetType(), ExperimentStepSet = new ExperimentStepSetType() };
            anIMLType.ExperimentStepSet.ExperimentStep = new ExperimentStepType[] {new ExperimentStepType()};
            anIMLType.ExperimentStepSet.ExperimentStep[0].Result = new ResultType[] { new ResultType { SeriesSet = new SeriesSetType { Series = new SeriesType[] { new SeriesType() } } } };

            return anIMLType;
        }
    }
}
