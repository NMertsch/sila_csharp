﻿using AnIMLCore;
using SiLA2.Database.NoSQL;

namespace SiLA2.AnIML.Services
{
    public interface IAnIMLRepository : IBaseRepository<AnIMLType>
    {
        // TODO: Serialization Error due to arrays used in generated AnIML classes...
        //string GetAnIMLAsXml(AnIMLType anIML);
    }
}