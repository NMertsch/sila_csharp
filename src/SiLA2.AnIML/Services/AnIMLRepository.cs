﻿using AnIMLCore;
using LiteDB;
using SiLA2.Database.NoSQL;
using System.Text;
using System.Xml.Serialization;

namespace SiLA2.AnIML.Services
{
    public class AnIMLRepository : BaseRepository<AnIMLType>, IAnIMLRepository
    {
        public AnIMLRepository(ILiteDatabase db) : base(db)
        {
        }

        // TODO: Serialization Error due to arrays used in generated AnIML classes...
        //public string GetAnIMLAsXml(AnIMLType anIML)
        //{
        //    string result = null;
        //    var serializer = new XmlSerializer(typeof(AnIMLType),);
        //    using (var stream = new MemoryStream())
        //    {
        //        serializer.Serialize(stream, anIML);
        //        result = Encoding.UTF8.GetString(stream.ToArray());
        //    }
        //    return result;
        //}
    }
}