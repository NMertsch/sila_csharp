using Grpc.Core;
using Sila2.Org.Silastandard.Test.Parameterconstraintstest.V1;
using System.Text;

namespace SiLA2.IntegrationTests.Server.Tests
{
    public class SiLA2TypeTests
    {
        private GrpcChannel _channel;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            var clientSetup = new Configurator(configuration, new string[] { });

            var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
            Console.WriteLine($"Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
            _channel = await ((IGrpcChannelProvider)clientSetup.ServiceProvider.GetService(typeof(IGrpcChannelProvider))).GetChannel(clientConfig.IpOrCdirOrFullyQualifiedHostName, clientConfig.Port, accceptAnyServerCertificate: true);
        }

        [Test]
        public void Should_Perform_AnyDataTypeTests()
        {
            // Arrange
            const int EXPECTED_RESULT_OF_GET_ANYTYPE_INTEGER_VALUE = 5124;

            // System under Test
            var anyTypeTestClient = new SiLAFramework.Test.Anytypetest.V1.AnyTypeTest.AnyTypeTestClient(_channel);

            // Act
            var anyTypeResult = anyTypeTestClient.Get_AnyTypeIntegerValue(new SiLAFramework.Test.Anytypetest.V1.Get_AnyTypeIntegerValue_Parameters());

            // Assert
            var dataType = AnyType.ExtractAnyType(anyTypeResult.AnyTypeIntegerValue.Type);
            Assert.IsTrue(dataType.Item is BasicType && (BasicType)dataType.Item == BasicType.Integer, $"[Get_AnyTypeIntegerValue] Wrong any type data type: {dataType}");

            var integerValue = (int)SiLAFramework.Integer.Parser.ParseFrom(anyTypeResult.AnyTypeIntegerValue.Payload).Value;
            Assert.That(integerValue, Is.EqualTo(EXPECTED_RESULT_OF_GET_ANYTYPE_INTEGER_VALUE), $"[Get_AnyTypeIntegerValue] Wrong integer value: {integerValue}");
        }

        [Test]
        public void Should_Perform_ErrorHandling_Tests()
        {
            // Arrange
            const string EXPECTED_DEFINED_EXECUTION_ERROR_TEXT = "SiLA DefinedExecution Error occurred. Details: { \"definedExecutionError\": { \"errorIdentifier\": \"org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError\", \"message\": \"SiLA2_test_error_message\" } }";

            // System under Test
            var errorHandlingTestClient = new SiLAFramework.Test.Errorhandlingtest.V1.ErrorHandlingTest.ErrorHandlingTestClient(_channel);

            // Act & Assert
            var exception = Assert.Throws<RpcException>(() => errorHandlingTestClient.RaiseDefinedExecutionError(new SiLAFramework.Test.Errorhandlingtest.V1.RaiseDefinedExecutionError_Parameters()));
            Assert.That(SiLA2.Server.Utils.ErrorHandling.HandleException(exception), Is.EqualTo(EXPECTED_DEFINED_EXECUTION_ERROR_TEXT));
        }

        [Test, Ignore("Ignored due to error in CheckStringConstraintSchema")]
        public void Should_Check_String_Constraints()
        {
            // Arrange
            var validLengthString = new CheckStringConstraintLength_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "0123456789" } };
            var validMaximalLengthString = new CheckStringConstraintMaximalLength_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "short enough" } };
            var validMinimalLengthString = new CheckStringConstraintMinimalLength_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "big enough" } };
            var validMinMaxLengthString = new CheckStringConstraintMinMaxLength_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "fitting constraints" } };
            var validSetString = new CheckStringConstraintSet_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "Second option" } };
            var validPatternString = new CheckStringConstraintPattern_Parameters { ConstrainedParameter = new SiLAFramework.String { Value = "08/05/2018" } };
            var validSiLA2FeatureString = new CheckStringConstraintSchema_Parameters
            {
                ConstrainedParameter = new SiLAFramework.String
                {
                    Value = File.ReadAllText(Path.Combine("TestData", "SiLAService-v1_0.sila.xml"), Encoding.UTF8)
                }
            };

            // System under Test
            var parameterConstraintsTestClient = new ParameterConstraintsTest.ParameterConstraintsTestClient(_channel);

            // Act & Assert
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckStringConstraintLength(validLengthString));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckStringConstraintMaximalLength(validMaximalLengthString));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckStringConstraintMinimalLength(validMinimalLengthString));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckStringConstraintMinMaxLength(validMinMaxLengthString));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckStringConstraintSet(validSetString));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckStringConstraintPattern(validPatternString));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckStringConstraintSchema(validSiLA2FeatureString));
        }

        [Test]
        public void Should_Check_Integer_Constraints()
        {
            // System under Test
            var parameterConstraintsTestClient = new ParameterConstraintsTest.ParameterConstraintsTestClient(_channel);

            // Act & Assert
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckIntegerConstraintSet(new CheckIntegerConstraintSet_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 3 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckScientificallyNotatedIntegerLimit(new CheckScientificallyNotatedIntegerLimit_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = (long)-1e2 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckIntegerConstraintMaximalExclusive(new CheckIntegerConstraintMaximalExclusive_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 10 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckIntegerConstraintMaximalInclusive(new CheckIntegerConstraintMaximalInclusive_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 10 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckIntegerConstraintMinimalExclusive(new CheckIntegerConstraintMinimalExclusive_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 0 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckIntegerConstraintMinimalInclusive(new CheckIntegerConstraintMinimalInclusive_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 0 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckIntegerConstraintMinMax(new CheckIntegerConstraintMinMax_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 0 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckIntegerConstraintMinMax(new CheckIntegerConstraintMinMax_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 10 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckIntegerConstraintUnit(new CheckIntegerConstraintUnit_Parameters { ConstrainedParameter = new SiLAFramework.Integer { Value = 123 } }));
        }

        [Test]
        public void Should_Check_Real_Constraints()
        {
            // System under Test
            var parameterConstraintsTestClient = new ParameterConstraintsTest.ParameterConstraintsTestClient(_channel);

            // Act & Assert
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckRealConstraintSet(new CheckRealConstraintSet_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 2.22 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckScientificallyNotatedRealLimit(new CheckScientificallyNotatedRealLimit_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 1e-2 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckRealConstraintMaximalExclusive(new CheckRealConstraintMaximalExclusive_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = -5e3 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckRealConstraintMaximalInclusive(new CheckRealConstraintMaximalInclusive_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 10 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckRealConstraintMinimalExclusive(new CheckRealConstraintMinimalExclusive_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 1e-5 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckRealConstraintMinimalInclusive(new CheckRealConstraintMinimalInclusive_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 0 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckRealConstraintMinMax(new CheckRealConstraintMinMax_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 1.23 } }));
            Assert.DoesNotThrow(() => parameterConstraintsTestClient.CheckRealConstraintUnit(new CheckRealConstraintUnit_Parameters { ConstrainedParameter = new SiLAFramework.Real { Value = 123 } }));
        }
    }
}