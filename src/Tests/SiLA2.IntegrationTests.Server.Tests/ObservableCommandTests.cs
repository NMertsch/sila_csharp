﻿using Grpc.Core;
using Sila2.Org.Silastandard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    public class ObservableCommandTests
    {
        private GrpcChannel _channel;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            var clientSetup = new Configurator(configuration, new string[] { });

            var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
            Console.WriteLine($"Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
            _channel = await ((IGrpcChannelProvider)clientSetup.ServiceProvider.GetService(typeof(IGrpcChannelProvider))).GetChannel(clientConfig.IpOrCdirOrFullyQualifiedHostName, clientConfig.Port, accceptAnyServerCertificate: true);
        }

        [Test, Ignore("Test fails on CI")]
        public async Task EchoValueAfterDelay_Works()
        {
            // Arrange
            const int SENT_VALUE = 123;
            const double EXECUTION_DELAY_IN_SECONDS = 3.0;

            // System under Test
            var observableCommandTestClient = new SiLAFramework.Test.Observablecommandtest.V1.ObservableCommandTest.ObservableCommandTestClient(_channel);

            // Act & Assert
            var startTime = DateTime.Now;

            // start command
            var cmdId = observableCommandTestClient.EchoValueAfterDelay(new SiLAFramework.Test.Observablecommandtest.V1.EchoValueAfterDelay_Parameters
            {
                Value = new Integer { Value = SENT_VALUE },
                Delay = new Real { Value = EXECUTION_DELAY_IN_SECONDS }
            }).CommandExecutionUUID;

            List<ExecutionInfo> receivedInfos = new List<ExecutionInfo>();

            // wait for command execution to finish
            try
            {
                using (var call = observableCommandTestClient.EchoValueAfterDelay_Info(cmdId))
                {
                    var responseStream = call.ResponseStream;
                    while (await responseStream.MoveNext())
                    {
                        receivedInfos.Add(responseStream.Current);

                        // As long as the specified delay has not been passed, the command status must be waiting
                        Assert.IsTrue((DateTime.Now - startTime).TotalSeconds > EXECUTION_DELAY_IN_SECONDS || receivedInfos.Last().CommandStatus == ExecutionInfo.Types.CommandStatus.Waiting);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Assert.GreaterOrEqual((DateTime.Now - startTime).TotalSeconds, EXECUTION_DELAY_IN_SECONDS);

            // at least 3 command execution info messages have to be sent
            Assert.GreaterOrEqual(receivedInfos.Count, 3);

            // first command execution status has to be Waiting, second last has to be Running and last one has to be FinishedSuccessfully
            Assert.True(receivedInfos.First().CommandStatus == ExecutionInfo.Types.CommandStatus.Waiting);
            Assert.NotNull(receivedInfos.Reverse<ExecutionInfo>().Skip(1).Take(1).DefaultIfEmpty());
            Assert.True(receivedInfos.Reverse<ExecutionInfo>().Skip(1).Take(1).First().CommandStatus == ExecutionInfo.Types.CommandStatus.Running);
            Assert.True(receivedInfos.Last().CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully);

            // get response and check against sent value
            var response = observableCommandTestClient.EchoValueAfterDelay_Result(cmdId);
            Assert.That(response.ReceivedValue.Value, Is.EqualTo(SENT_VALUE));
        }

        [Test]
        public void Should_Receive_IntermediateResponses()
        {
            // Arrange

            // System under Test

            // Act

            // Assert
            Assert.Pass();
        }
    }
}
