﻿using Grpc.Core;
using static Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;
using Sila2.Org.Silastandard.Core.Silaservice.V1;

namespace SiLA2.IntegrationTests.ClientApp.Native.Grpc
{
    class Program
    {
        const string HOST = "127.0.0.1";

        static void Main(string[] args)
        {
#if DEBUG
            const int SECURE_PORT = 50052; // Please adjust Port if you want to connect to another SiLA2-Server than SiLA2.Temperature.Server.App in Test Scenario
#else
            const int SECURE_PORT = 13742;
#endif

            try
            {
                GetServerResponse(SECURE_PORT, new SslCredentials(File.ReadAllText("ca.crt")), $"{Environment.NewLine}Sending secure (https) Server-Name-Request SiLA2 Server by Native gRPC-Client "); // File.ReadAllText("server.crt")) works as well because server.crt is equal to ca.cert
            }
            catch (RpcException e)
            {
                Console.WriteLine(e);
            }
            Console.ReadKey();
        }

        private static void GetServerResponse(int port, ChannelCredentials sslCredentials, string requestText)
        {
            var channel = new Channel($"{HOST}:{port}", sslCredentials);
            Console.WriteLine($"{Environment.NewLine}{requestText} ({channel.Target})");
            var silaService = new SiLAServiceClient(channel);
            var response = silaService.Get_ServerName(new Get_ServerName_Parameters());
            Console.WriteLine($"{Environment.NewLine}Response from SiLA2 Server : {response.ServerName}");
        }
    }
}
