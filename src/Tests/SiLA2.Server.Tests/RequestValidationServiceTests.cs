﻿using Google.Protobuf;
using NUnit.Framework;
using Sila = Sila2.Org.Silastandard;
using SiLA2.Server.Services;
using System;
using System.IO;
using Sila2.Org.Silastandard.Examples.Datatypeprovider.V1;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    public class RequestValidationServiceTests
    {
        readonly string TEST_FEATURE_PATH = Path.Combine("TestData", "DataTypeProvider.sila.xml");

        private Feature _testfeature;

        [OneTimeSetUp]
        public void Init()
        {
            _testfeature = TestHelper.GetFeatureFromFile(TEST_FEATURE_PATH);
        }

        [Test]
        public void Should_Validate_Command_SetStringValue_Parameters()
        {
            // Arrange
            const string SET_STRING_VALUE_ID = "SetStringValue";

            // System under Test
            var setStringValue_Parameters = new SetStringValue_Parameters { StringValue = new Sila.String { Value = "Test" } };

            // Act & Assert
            Assert.DoesNotThrow(() => setStringValue_Parameters.ValidateCommandParameters(_testfeature, SET_STRING_VALUE_ID));
        }

        [Test]
        public void Should_Fail_Parameter_Validation_With_Wrong_Parameters_Message()
        {
            // Arrange
            const string SET_STRING_VALUE_ID = "SetStringValue";

            // System under Test
            var setIntegerValue_Parameters = new SetIntegerValue_Parameters { IntegerValue = new Sila.Integer { Value = 42 } };

            // Act & Assert
            Assert.Catch(() => setIntegerValue_Parameters.ValidateCommandParameters(_testfeature, SET_STRING_VALUE_ID));
        }

        [Test]
        public void Should_Validate_Command_SetIntegerValue_Parameters()
        {
            // Arrange
            const string SET_INT_VALUE_ID = "SetIntegerValue";

            // System under Test
            var setIntegerValue_Parameters = new SetIntegerValue_Parameters { IntegerValue = new Sila.Integer { Value = 42 } };

            // Act & Assert
            Assert.DoesNotThrow(() => setIntegerValue_Parameters.ValidateCommandParameters(_testfeature, SET_INT_VALUE_ID));
        }

        [Test]
        public void Should_Validate_Command_SeveralValues_Parameters()
        {
            // Arrange
            var now = DateTime.Now;
            Sila.Timezone timezone = new Sila.Timezone { Hours = now.Hour, Minutes = (uint)now.Minute };

            // System under Test
            var setSeveralValues_Parameters = new SetSeveralValues_Parameters
            {
                StringValue = new Sila.String { Value = "Test" },
                IntegerValue = new Sila.Integer { Value = 42 },
                RealValue = new Sila.Real { Value = 3.1415 },
                BooleanValue = new Sila.Boolean { Value = true },
                DateValue = new Sila.Date { Year = 2019, Month = 9, Day = 25, Timezone = timezone },
                TimeValue = new Sila.Time { Hour = 12, Minute = 34, Second = 56, Timezone = timezone },
                TimeStampValue = new Sila.Timestamp
                {
                    Year = (uint)now.Year,
                    Month = (uint)now.Month,
                    Day = (uint)now.Day,
                    Hour = (uint)now.Hour,
                    Minute = (uint)now.Minute,
                    Second = (uint)now.Second,
                    Timezone = timezone
                }
            };

            const string SET_SEVERAL_VALUES_ID = "SetSeveralValues";

            // Act & Assert
            Assert.DoesNotThrow(() => setSeveralValues_Parameters.ValidateCommandParameters(_testfeature, SET_SEVERAL_VALUES_ID));
        }

        [Test]
        public void Should_Validate_Command_SetStringList_Parameters()
        {
            // Arrange
            const string SET_STRING_LIST_ID = "SetStringList";

            // System under Test
            var stringListParameters = new SetStringList_Parameters
            {
                StringList =
                {
                    new Sila.String{ Value = "SiLA 2" },
                    new Sila.String{ Value = "is" },
                    new Sila.String{ Value = "great" }
                }
            };

            // Act & Assert
            Assert.DoesNotThrow(() => stringListParameters.ValidateCommandParameters(_testfeature, SET_STRING_LIST_ID));
        }

        [Test]
        public void Should_Validate_Command_Structure_Parameters()
        {
            // Arrange
            const string SET_STRUCTURE_VALUE_ID = "SetStructureValue";

            var testStructure = new DataType_MyStructure.Types.MyStructure_Struct
            {
                StringTypeValue = new Sila.String { Value = "StructureStringTestVariable" },
                IntegerTypeValue = new Sila.Integer { Value = 5142 },
                RealTypeValue = new Sila.Real { Value = 3.1415926 },
                BooleanTypeValue = new Sila.Boolean { Value = true },
                BinaryTypeValue = new Sila.Binary { Value = ByteString.CopyFromUtf8("SiLA2_Binary_String_Value") },
                DateTypeValue = new Sila.Date { Year = 2019, Month = 9, Day = 25 },
                TimeTypeValue = new Sila.Time { Hour = 12, Minute = 34, Second = 56 },
                TimestampTypeValue = new Sila.Timestamp { Year = 2019, Month = 09, Day = 25, Hour = 12, Minute = 34, Second = 56 },
                AnyTypeValue = new Sila.Any { Payload = ByteString.CopyFromUtf8("SiLA2_Any_Type_String_Value"), Type = "string" },
            };

            // System under Test
            var setStructureValue_Parameters = new SetStructureValue_Parameters { StructureValue = new DataType_MyStructure { MyStructure = testStructure } };

            // Act & Assert
            Assert.DoesNotThrow(() => setStructureValue_Parameters.ValidateCommandParameters(_testfeature, SET_STRUCTURE_VALUE_ID));
        }


        [Test]
        public void Should_Validate_Command_DeepStructure_Parameters()
        {
            // Arrange
            const string SET_DEEPSTRUCTURE_VALUE_ID = "SetDeepStructureValue";
            var testStructureDeep = new DataType_DeepStructure.Types.DeepStructure_Struct
            {
                OuterStringTypeValue = new Sila.String { Value = "Outer_Test_String" },
                OuterIntegerTypeValue = new Sila.Integer { Value = 1111 },
                MiddleStructure = new DataType_DeepStructure.Types.DeepStructure_Struct.Types.MiddleStructure_Struct
                {
                    MiddleStringTypeValue = new Sila.String { Value = "Middle_Test_String" },
                    MiddleIntegerTypeValue = new Sila.Integer { Value = 2222 },
                    InnerStructure = new DataType_DeepStructure.Types.DeepStructure_Struct.Types.MiddleStructure_Struct.Types.InnerStructure_Struct
                    {
                        InnerStringTypeValue = new Sila.String { Value = "Inner_Test_String" },
                        InnerIntegerTypeValue = new Sila.Integer { Value = 3333 }
                    }
                }
            };


            // System under Test
            var setDeepStructureValue_Parameters = new SetDeepStructureValue_Parameters { DeepStructureValue = new DataType_DeepStructure { DeepStructure = testStructureDeep } };

            // Act & Assert
            Assert.DoesNotThrow(() => setDeepStructureValue_Parameters.ValidateCommandParameters(_testfeature, SET_DEEPSTRUCTURE_VALUE_ID));
        }

        [Test, Ignore("Work in progress...")]
        public void Should_Validate_Command_Binary_Parameters()
        {
            // Arrange
            var directBinaryValueParameter = new SetBinaryValue_Parameters { BinaryValue = new Sila.Binary { Value = ByteString.CopyFromUtf8("TestString") } };
            var binaryTransferParameter = new SetBinaryValue_Parameters { BinaryValue = new Sila.Binary { BinaryTransferUUID = Guid.NewGuid().ToString() } };
            const string SET_BINARY_VALUE_ID = "SetBinaryValue";

            // Act 
            try
            {
                binaryTransferParameter.ValidateCommandParameters(_testfeature, SET_BINARY_VALUE_ID);
                directBinaryValueParameter.ValidateCommandParameters(_testfeature, SET_BINARY_VALUE_ID);

                // Assert
                Assert.Catch(() => binaryTransferParameter.ValidateCommandParameters(_testfeature, SET_BINARY_VALUE_ID));
            }
            catch (Exception ex)
            {

            }
        }
    }
}
