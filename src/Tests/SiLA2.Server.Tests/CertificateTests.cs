﻿using NUnit.Framework;
using SiLA2.Utils.Security;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    public class CertificateTests
    {
        [Test]
        public void Should_Create_CA_From_Discovery()
        {
            // Arrange
            var ca = File.ReadAllText("mdns_transfer_test_ca.crt");
            var data = PemEncoding.Find(ca);
            var caRe = Convert.FromBase64String(ca[data.Base64Data]);
            var caCertServer = new X509Certificate2(caRe);
            var caMdns = "ca0=-----BEGIN CERTIFICATE-----\r\nca1=MIICxjCCAa6gAwIBAgIJAP3EOJgQkA/0MA0GCSqGSIb3DQEBCwUAMA8xDTALBgNV\r\nca2=BAMTBFJvb3QwIBcNMjIwODA4MDkzNDA0WhgPMjEyMjA4MDgwOTM1MDRaMA8xDTAL\r\nca3=BgNVBAMTBFJvb3QwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDQYpbX\r\nca4=OJVncuUmQqPWbkcs9V2VS1yW3Konj3NlGByqd5KfnPWD9EYKqo2WMF7oThRta44L\r\nca5=jsxD6wcdKnXwi57riUdBDObUKIBn2gmP43WvaLEa7FnnNIT3DNRIbRRRbmiETRyd\r\nca6=Y8rHdoQPpQbEOMwYEtrMQVcvjgJmOanCVpJWHCAg1iUCmFb/d5Wwee1vXeawQrT2\r\nca7=TokKf80CMbnYpYMFzJVZGzz2qgcctRCHNTHzOtCK+7ffbJH0Tx87ZbwsD1xyrjez\r\nca8=1hnEVSo/Y8HCJFnA1+U8EMTCxYO/u5duFtpNL4Ar6/faLdPxwPj+GjGbxgzZkryN\r\nca9=4rB21vwcVNmvhJwlAgMBAAGjIzAhMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/\r\nca10=BAQDAgIEMA0GCSqGSIb3DQEBCwUAA4IBAQC0a25NKutw3XMMLPRd4LIew7KAUdXk\r\nca11=08nenNptDX54iph6Yw/Z17QBuXs1NMBGu5zGUYA/TYAMoPcVQTvr1WYNFSO3OFJn\r\nca12=llifSwPEwBqtBF9+1RfFDwtGSxjsVCy/wBK/Y1DaBZCrWsOI4pj3aNXg7+R0a47t\r\nca13=v5hqLydYtPDSbCemyMgbrx50cbbgZIcJcWJbIO9eB1msoVE8OOuJBWW/K077eCov\r\nca14=enA+9HQ5CWmbSJVNH6dd8f87xtdbz+TpBKjY2sa4+9CDAkAijkXoBjCiM/XMMCl0\r\nca15=kcypQSKXqoS136lAHkDNKRWPrTmjZIsLNh2G9Y1E4ENmUh/ViX2M8RUm\r\nca16=-----END CERTIFICATE-----\r\n";
            
            // System under Test
            var repo = new CertificateRepository(new CertificateContext(), null, null);

            // Act
            var caCertClient = repo.GetCaFromFormattedCa(caMdns);

            // Assert
            Assert.True(caCertServer.RawData.SequenceEqual(caCertClient.RawData));
        }
    }
}
