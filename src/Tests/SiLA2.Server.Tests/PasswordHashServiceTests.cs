﻿using NUnit.Framework;
using SiLA2.Utils;
using System;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    public class PasswordHashServiceTests
    {
        [Test]
        public void ShouldVerfifyExistingPassword()
        {
            //Arrange
            const string PWD = "t3st!";
            var hashInput = new PasswordHash(PWD);
            var hashBytes = hashInput.ToArray();
            var hashBytesBase64 = Convert.ToBase64String(hashBytes);
            var hashBytesResult = Convert.FromBase64String(hashBytesBase64);

            //SUT
            var resultPasswordHash = new PasswordHash(hashBytesResult);

            //Act
            var result = resultPasswordHash.Verify(PWD);

            //Assert
            Assert.That(result, Is.True);
        }
    }
}