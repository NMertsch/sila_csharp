﻿using NUnit.Framework;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;
using SiLA2.IntegrationTests.ServerApp;
using Sila2.Org.Silastandard;
using Grpc.Core;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    public class SiLAServiceTests
    {
        TestServerFixture<Startup> _testServerFixture;

        // Arrange
        private const string MODIFIED_SERVER_NAME = "ModifiedServerName";
        private const string EXPECTED_SERVER_NAME = "SiLA2 Integration Test Server";
        private const string EXPECTED_SERVER_TYPE = "SiLA2IntegrationTestServer";
        private const string EXPECTED_SERVER_UUID = "A82121B1-22DE-4B81-A450-86FA2E5344EE";
        private const string EXPECTED_SERVER_DESCRIPTION = "SiLA2 Integration Test Server. Cross-platform implementation for .NET with web server Kestrel as Service Host.";
        private const string EXPECTED_SERVER_VENDOR_URL = "https://sila2.gitlab.io/sila_base/";
        private const string EXPECTED_SERVER_VERSION = "7.2.1";
        private const string EXPECTED_SILA_SERVICE_FEATURE_DEFINITION_PART = "This Feature MUST be implemented by each SiLA Server.    It specifies Commands and Properties to discover the Features a SiLA Server implements as well as details    about the SiLA Server, like name, type, description, vendor and UUID.    Any interaction described in this feature MUST not affect the behaviour of any other Feature.";
        private const string EXPECTED_LOCKCONTROLLER_SERVICE_FEATURE_DEFINITION_PART = "This Feature allows a SiLA Client to lock a SiLA Server for exclusive use, preventing other SiLA Clients";
        private const string EXPECTED_ERROR_RECOVERY_SERVICE_FEATURE_DEFINITION_PART = "This feature enables SiLA error handling during the execution of observable commands.";
        private const string EXPECTED_ANYTYPE_TEST_SERVICE_FEATURE_DEFINITION_PART = "Provides commands and properties to set or respectively get SiLA Any Type values via command parameters or property responses respectively.";
        private const string EXPECTED_BASIC_DATAT_YPES_TEST_SERVICE_FEATURE_DEFINITION_PART = "Provides commands and properties to set or respectively get all SiLA Basic Data Types via command parameters or property responses respectively.";
        private const string EXPECTED_ERRORHANDLING_TEST_SERVICE_FEATURE_DEFINITION_PART = "Raises the \"Test Error\" with the error message 'SiLA2_test_error_message'";
        private const string EXPECTED_LIST_DATA_TYPE_SERVICE_FEATURE_DEFINITION_PART = "Provides commands and properties to set or respectively get SiLA List Data Type values via command parameters or property responses respectively.";
        private const string EXPECTED_LOCKABLE_COMMAND_PROVIDER_FEATURE_DEFINITION_PART = "Contains a command that requires locking. The lock is to be obtained through the \"Lock Controller\" core feature.";
        private const string EXPECTED_STRUCTURE_DATA_TYPE_SERVICE_FEATURE_DEFINITION_PART = "Provides commands and properties to set or respectively get SiLA Structure Data Type values via command parameters or property responses respectively.";
        private const string EXPECTED_UNOBSERVABLE_COMMAND_SERVICE_FEATURE_DEFINITION_PART = "A command that takes no parameters and returns no responses";
        private const string EXPECTED_UNOBSERVABLE_PROPERTY_SERVICE_FEATURE_DEFINITION_PART = "This feature tests a static and a dynamic unobservable property.";
        private const string EXPECTED_PARAMETER_CONSTRAINTS_TEST_DEFINITION_PART = "This is a test feature to generically test constrained parameters.";
        private const string EXPECTED_METADATA_CONSUMER_TEST_DEFINITION_PART = "This feature consumes SiLA Client Metadata from the \"Metadata Provider\" feature.";
        private const string EXPECTED_METADATA_PROVIDER_DEFINITION_PART = "This feature provides SiLA Client Metadata to the \"Metadata Consumer Test\" feature.";
        private const string EXPECTED_OBSERVABLE_COMMAND_TEST_DEFINITION_PART = "This is a test feature to test observable commands.";
        private const string EXPECTED_OBSERVABLE_PROPERTY_TEST_DEFINITION_PART = "This is a test feature to test observable properties.";

        private SiLAServiceClient _siLAServiceClient;
        private readonly List<string> _expectedImplementedFeatures = new List<string> 
                                                            { { "org.silastandard/core/SiLAService/v1" },
                                                              { "org.silastandard/core/LockController/v2" },
                                                              { "org.silastandard/core/ErrorRecoveryService/v1" },
                                                              { "org.silastandard/test/AnyTypeTest/v1" },
                                                              { "org.silastandard/test/BasicDataTypesTest/v1" },
                                                              { "org.silastandard/test/BinaryTransferTest/v1" },
                                                              { "org.silastandard/test/ErrorHandlingTest/v1" },
                                                              { "org.silastandard/test/ListDataTypeTest/v1" },
                                                              { "org.silastandard/test/LockableCommandProvider/v1" },
                                                              { "org.silastandard/test/MetadataConsumerTest/v1" },
                                                              { "org.silastandard/test/MetadataProvider/v1" },
                                                              { "org.silastandard/test/StructureDataTypeTest/v1" },
                                                              { "org.silastandard/test/UnobservableCommandTest/v1" },
                                                              { "org.silastandard/test/UnobservablePropertyTest/v1" },
                                                              { "org.silastandard/test/ParameterConstraintsTest/v1"},
                                                              { "org.silastandard/test/ObservableCommandTest/v1"},
                                                              { "org.silastandard/test/ObservablePropertyTest/v1"}
                                                            };

        [OneTimeSetUp]
        public void SetupOnce()
        {
            // System under Test
            var args = new string[] { };
            _testServerFixture = new TestServerFixture<Startup>(args);
            _siLAServiceClient = new SiLAServiceClient(_testServerFixture.GrpcChannel);
        }

        [OneTimeTearDown]
        public void TeardownOnce()
        {
            _testServerFixture.Dispose();   
        }

        [Test]
        public async Task Should_Get_ServerNameAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerNameAsync(new Get_ServerName_Parameters());

            // Assert
            Assert.That(result.ServerName.Value, Is.EqualTo(EXPECTED_SERVER_NAME));
        }

        [Test]
        public void Should_Get_ServerName() 
        { 
            // Act
            var result = _siLAServiceClient.Get_ServerName(new Get_ServerName_Parameters());

            // Assert
            Assert.That(result.ServerName.Value, Is.EqualTo(EXPECTED_SERVER_NAME));
        }

        [Test]
        public async Task Should_Get_ServerUUIDAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerUUIDAsync(new Get_ServerUUID_Parameters());

            // Assert
            Assert.That(result.ServerUUID.Value.ToUpper(), Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [Test]
        public void Should_Get_ServerUUID()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerUUID(new Get_ServerUUID_Parameters());

            // Assert
            Assert.That(result.ServerUUID.Value.ToUpper(), Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [TestCase("org.silastandard/core/SiLAService/v1", EXPECTED_SILA_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/core/LockController/v2", EXPECTED_LOCKCONTROLLER_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/core/ErrorRecoveryService/v1", EXPECTED_ERROR_RECOVERY_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/AnyTypeTest/v1", EXPECTED_ANYTYPE_TEST_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/BasicDataTypesTest/v1", EXPECTED_BASIC_DATAT_YPES_TEST_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/ErrorHandlingTest/v1", EXPECTED_ERRORHANDLING_TEST_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/ListDataTypeTest/v1", EXPECTED_LIST_DATA_TYPE_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/LockableCommandProvider/v1", EXPECTED_LOCKABLE_COMMAND_PROVIDER_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/MetadataConsumerTest/v1", EXPECTED_METADATA_CONSUMER_TEST_DEFINITION_PART)]
        [TestCase("org.silastandard/test/MetadataProvider/v1", EXPECTED_METADATA_PROVIDER_DEFINITION_PART)]
        [TestCase("org.silastandard/test/StructureDataTypeTest/v1", EXPECTED_STRUCTURE_DATA_TYPE_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/UnobservableCommandTest/v1", EXPECTED_UNOBSERVABLE_COMMAND_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/UnobservablePropertyTest/v1", EXPECTED_UNOBSERVABLE_PROPERTY_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/ParameterConstraintsTest/v1", EXPECTED_PARAMETER_CONSTRAINTS_TEST_DEFINITION_PART)]
        [TestCase("org.silastandard/test/ObservableCommandTest/v1", EXPECTED_OBSERVABLE_COMMAND_TEST_DEFINITION_PART)]
        [TestCase("org.silastandard/test/ObservablePropertyTest/v1", EXPECTED_OBSERVABLE_PROPERTY_TEST_DEFINITION_PART)]
        public async Task Should_Get_FeatureDefinitionAsync(string featureDefinitionId, string resultStringPart)
        {
            // Arrange
            var featureDefinitionParameter = new GetFeatureDefinition_Parameters();
            var featureDefId = new String { Value = featureDefinitionId };
            featureDefinitionParameter.FeatureIdentifier = featureDefId;

            // Act
            var result = await _siLAServiceClient.GetFeatureDefinitionAsync(featureDefinitionParameter);

            // Assert
            Assert.That(result.FeatureDefinition.Value.IndexOf(resultStringPart)> -1);
        }

        [TestCase("org.silastandard/core/SiLAService/v1", EXPECTED_SILA_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/core/LockController/v2", EXPECTED_LOCKCONTROLLER_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/core/ErrorRecoveryService/v1", EXPECTED_ERROR_RECOVERY_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/AnyTypeTest/v1", EXPECTED_ANYTYPE_TEST_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/BasicDataTypesTest/v1", EXPECTED_BASIC_DATAT_YPES_TEST_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/ErrorHandlingTest/v1", EXPECTED_ERRORHANDLING_TEST_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/ListDataTypeTest/v1", EXPECTED_LIST_DATA_TYPE_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/LockableCommandProvider/v1", EXPECTED_LOCKABLE_COMMAND_PROVIDER_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/MetadataConsumerTest/v1", EXPECTED_METADATA_CONSUMER_TEST_DEFINITION_PART)]
        [TestCase("org.silastandard/test/MetadataProvider/v1", EXPECTED_METADATA_PROVIDER_DEFINITION_PART)]
        [TestCase("org.silastandard/test/StructureDataTypeTest/v1", EXPECTED_STRUCTURE_DATA_TYPE_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/UnobservableCommandTest/v1", EXPECTED_UNOBSERVABLE_COMMAND_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/UnobservablePropertyTest/v1", EXPECTED_UNOBSERVABLE_PROPERTY_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/test/ParameterConstraintsTest/v1", EXPECTED_PARAMETER_CONSTRAINTS_TEST_DEFINITION_PART)]
        [TestCase("org.silastandard/test/ObservableCommandTest/v1", EXPECTED_OBSERVABLE_COMMAND_TEST_DEFINITION_PART)]
        [TestCase("org.silastandard/test/ObservablePropertyTest/v1", EXPECTED_OBSERVABLE_PROPERTY_TEST_DEFINITION_PART)]
        public void Should_Get_FeatureDefinition(string featureDefinitionId, string resultStringPart)
        {
            // Arrange
            var featureDefinitionParameter = new GetFeatureDefinition_Parameters();
            var featureDefId = new String { Value = featureDefinitionId };
            featureDefinitionParameter.FeatureIdentifier = featureDefId;

            // Act
            var result = _siLAServiceClient.GetFeatureDefinition(featureDefinitionParameter);

            // Assert
            Assert.That(result.FeatureDefinition.Value.IndexOf(resultStringPart) > -1);
        }

        [TestCase("SiLAService", "SiLA Validation Error occurred. Details: { \"validationError\": { \"parameter\": \"org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/Parameter/FeatureIdentifier\", \"message\": \"The Fully Qualified FeatureIdentifier does not match its Regular Expression '\\\\b[a-z][a-z0-9\\\\.]{0,254}/[a-z][a-z0-9\\\\.]{0,254}/[A-Z][a-zA-Z0-9]*/v\\\\d+\\\\b' !\" } }")]
        [TestCase("", "SiLA Validation Error occurred. Details: { \"validationError\": { \"parameter\": \"org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/Parameter/FeatureIdentifier\", \"message\": \"The FeatureIdentifier must not be NULL or empty !\" } }")]
        public async Task Should_Fail_On_Getting_FeatureDefinition_By_Wrong_Identifier(string featureIdentifier, string expectedExceptionMessage)
        {
            // Act & Assert
            var exception = Assert.Throws<RpcException>(() => _siLAServiceClient.GetFeatureDefinition(new GetFeatureDefinition_Parameters { FeatureIdentifier = new String { Value = featureIdentifier } }) );
            Assert.That(Utils.ErrorHandling.HandleException(exception), Is.EqualTo(expectedExceptionMessage));
        }

        [TestCase("SiLAService", "SiLA Validation Error occurred. Details: { \"validationError\": { \"parameter\": \"org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/Parameter/FeatureIdentifier\", \"message\": \"The Fully Qualified FeatureIdentifier does not match its Regular Expression '\\\\b[a-z][a-z0-9\\\\.]{0,254}/[a-z][a-z0-9\\\\.]{0,254}/[A-Z][a-zA-Z0-9]*/v\\\\d+\\\\b' !\" } }")]
        [TestCase("", "SiLA Validation Error occurred. Details: { \"validationError\": { \"parameter\": \"org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/Parameter/FeatureIdentifier\", \"message\": \"The FeatureIdentifier must not be NULL or empty !\" } }")]
        public async Task Should_Fail_On_Getting_FeatureDefinition_By_Wrong_Identifier_Async(string featureIdentifier, string expectedExceptionMessage)
        {
            // Act & Assert
            var exception = Assert.ThrowsAsync<RpcException>(async() => await _siLAServiceClient.GetFeatureDefinitionAsync(new GetFeatureDefinition_Parameters { FeatureIdentifier = new String { Value = featureIdentifier } }));
            Assert.That(Utils.ErrorHandling.HandleException(exception), Is.EqualTo(expectedExceptionMessage));
        }

        [Test]
        public async Task Should_Get_ServerVersionAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerVersionAsync(new Get_ServerVersion_Parameters());

            // Assert
            Assert.That(result.ServerVersion.Value, Is.EqualTo(EXPECTED_SERVER_VERSION));
        }

        [Test]
        public void Should_Get_ServerVersion()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerVersion(new Get_ServerVersion_Parameters());

            // Assert
            Assert.That(result.ServerVersion.Value, Is.EqualTo(EXPECTED_SERVER_VERSION));
        }

        [Test]
        public async Task Should_Get_ServerVendorURLAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerVendorURLAsync(new Get_ServerVendorURL_Parameters());

            // Assert
            Assert.That(result.ServerVendorURL.Value, Is.EqualTo(EXPECTED_SERVER_VENDOR_URL));
        }

        [Test]
        public void Should_Get_ServerVendorURL()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerVendorURL(new Get_ServerVendorURL_Parameters());

            // Assert
            Assert.That(result.ServerVendorURL.Value, Is.EqualTo(EXPECTED_SERVER_VENDOR_URL));
        }

        [Test]
        public async Task Should_Get_ServerDescriptionAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerDescriptionAsync(new Get_ServerDescription_Parameters());

            // Assert
            Assert.That(result.ServerDescription.Value, Is.EqualTo(EXPECTED_SERVER_DESCRIPTION));
        }

        [Test]
        public void Should_Get_ServerDescription()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerDescription(new Get_ServerDescription_Parameters());

            // Assert
            Assert.That(result.ServerDescription.Value, Is.EqualTo(EXPECTED_SERVER_DESCRIPTION));
        }

        [Test]
        public async Task Should_Get_ServerTypeAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerTypeAsync(new Get_ServerType_Parameters());

            // Assert
            Assert.That(result.ServerType.Value, Is.EqualTo(EXPECTED_SERVER_TYPE));
        }

        [Test]
        public void Should_Get_ServerType()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerType(new Get_ServerType_Parameters());

            // Assert
            Assert.That(result.ServerType.Value, Is.EqualTo(EXPECTED_SERVER_TYPE));
        }

        [Test]
        public async Task Should_Get_ImplementedFeaturesAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ImplementedFeaturesAsync(new Get_ImplementedFeatures_Parameters());

            // Assert
            Assert.That(result.ImplementedFeatures.Count, Is.EqualTo(_expectedImplementedFeatures.Count));
            foreach(var feature in result.ImplementedFeatures)
            {
                Assert.That(_expectedImplementedFeatures.Contains(feature.Value));
            }
        }

        [Test]
        public void Should_Get_ImplementedFeatures()
        {
            // Act
            var result = _siLAServiceClient.Get_ImplementedFeatures(new Get_ImplementedFeatures_Parameters());

            // Assert
            Assert.That(result.ImplementedFeatures.Count, Is.EqualTo(_expectedImplementedFeatures.Count));
            foreach (var feature in result.ImplementedFeatures)
            {
                Assert.That(_expectedImplementedFeatures.Contains(feature.Value));
            }
        }

        [Test]
        public void Should_Set_ServerName()
        {
            // Arrange
            var oldServerName = _siLAServiceClient.Get_ServerName(new Get_ServerName_Parameters());

            try
            {
                // Act
                _siLAServiceClient.SetServerName(new SetServerName_Parameters { ServerName = new String { Value = MODIFIED_SERVER_NAME } });

                // Assert
                var result = _siLAServiceClient.Get_ServerName(new Get_ServerName_Parameters());

                Assert.That(result.ServerName.Value, Is.EqualTo(MODIFIED_SERVER_NAME));
            }
            finally
            {
                _siLAServiceClient.SetServerName(new SetServerName_Parameters { ServerName = new String { Value = oldServerName.ServerName.Value } });
            }
        }

        [Test]
        public async Task Should_Set_ServerNameAsync()
        {
            // Arrange
            var oldServerName = await _siLAServiceClient.Get_ServerNameAsync(new Get_ServerName_Parameters());

            try
            {
                // Act
                _siLAServiceClient.SetServerName(new SetServerName_Parameters { ServerName = new String { Value = MODIFIED_SERVER_NAME } });

                // Assert
                var result = _siLAServiceClient.Get_ServerName(new Get_ServerName_Parameters());

                Assert.That(result.ServerName.Value, Is.EqualTo(MODIFIED_SERVER_NAME));
            }
            finally
            {
                _siLAServiceClient.SetServerName(new SetServerName_Parameters { ServerName = new String { Value = oldServerName.ServerName.Value } });
            }
        }
    }
}
