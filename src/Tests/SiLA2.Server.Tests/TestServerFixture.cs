﻿using Grpc.Net.Client;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using Sila2.Utils;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SiLA2.Server.Tests
{
    public sealed class TestServerFixture<T> : IDisposable where T : class
    {
        private readonly WebApplicationFactory<T> _factory;

        public IServiceProvider ServiceProvider => _factory.Services;

        public TestServerFixture(string[] args, int? port = null)
        {
            Args = args;
            _factory = new WebApplicationFactory<T>();  //.ConfigureKestrel(args.SetupKestrel());
            _factory.WithWebHostBuilder(builder => builder.ConfigureKestrel(serverOptions =>
            {
                var certificateProvider = serverOptions.ApplicationServices.GetService(typeof(ICertificateProvider)) as ICertificateProvider;
                var certificate = certificateProvider.GetServerCertificate(safeIfNotExists: true);

                serverOptions.ConfigureEndpointDefaults(endpoints => endpoints.Protocols = HttpProtocols.Http1AndHttp2);
                var serverConfig = serverOptions.ApplicationServices.GetService(typeof(IServerConfig)) as IServerConfig;

                args.ParseServerCmdLineArgs<CmdLineServerArgs>(serverConfig);

                serverOptions.Listen(IPAddress.Any, serverConfig.Port, listenOptions => listenOptions.UseHttps(certificate));
            }));
            var client = _factory.CreateDefaultClient(new ResponseVersionHandler());
            var address = port.HasValue ? new Uri($"https://localhost:{port.Value}") : client.BaseAddress;
            GrpcChannel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
            {
                HttpClient = client
            });
        }

        public GrpcChannel GrpcChannel { get; }
        
        public string[] Args { get; set; }

        public void Dispose()
        {
            _factory.Dispose();
        }

        private class ResponseVersionHandler : DelegatingHandler
        {
            protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                var response = await base.SendAsync(request, cancellationToken);
                response.Version = request.Version;
                return response;
            }
        }
    }
}
