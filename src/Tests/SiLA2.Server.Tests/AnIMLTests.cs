﻿using AnIMLCore;
using AnIMLTechniqueNsAlias = AnIMLTechnique;
using NUnit.Framework;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using SiLA2.AnIML;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    internal class AnIMLTests
    {
        const string TEST_FOLDER = "TestData";

        [TestCase("microplate-sila.animl", 5, 1, 4.749, 5.652)]
        public void Should_Deserialize_AnIML_From_File(string fileName, int experimentStepCount, int focusedSeries, double firstSeriesValue, double lastSeriesValue)
        {
            // Arrange
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;

            var filePath = $"{Path.Combine(TEST_FOLDER, fileName)}";
            AnIMLType aniML = null;

            // System under Test
            XmlSerializer serializer = new XmlSerializer(typeof(AnIMLType));

            // Act
            using (var fs = File.OpenRead(filePath))
            {
                using (XmlReader reader = XmlReader.Create(fs, settings))
                {
                    aniML = (AnIMLType)serializer.Deserialize(reader);
                }
            }

            // Assert
            Assert.IsNotNull(aniML);
            Assert.That(aniML.ExperimentStepSet.ExperimentStep.Length, Is.EqualTo(experimentStepCount));
            var valueSet = (IndividualValueSetType)aniML.ExperimentStepSet.ExperimentStep[0].Result[0].SeriesSet.Series[focusedSeries].Items[0];
            Assert.That(valueSet.Items[0], Is.EqualTo(firstSeriesValue));
            Assert.That(valueSet.Items[valueSet.Items.Length - 1], Is.EqualTo(lastSeriesValue));
        }

        [TestCase("microplate-read.atdd")]
        [TestCase("chromatography.atdd")]
        public void Should_Deserialize_AnIML_Technique_From_File(string fileName)
        {
            // Arrange
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.IgnoreWhitespace = true;

            var filePath = $"{Path.Combine(TEST_FOLDER, fileName)}";
            AnIMLTechniqueNsAlias.TechniqueType animlTechnique = null;

            // System under Test
            XmlSerializer serializer = new XmlSerializer(typeof(AnIMLTechniqueNsAlias.TechniqueType));

            // Act
            using (var fs = File.OpenRead(filePath))
            {
                using (XmlReader reader = XmlReader.Create(fs, settings))
                {
                    animlTechnique = (AnIMLTechniqueNsAlias.TechniqueType)serializer.Deserialize(reader);
                }
            }

            // Assert
            Assert.IsNotNull(animlTechnique);
        }

        [Test]
        public void Should_Validate_AnIML_Xml()
        {
            // Arrange
            string ANIML_TEST_DOCUMENT = Path.Combine(TEST_FOLDER, "microplate-sila.animl");

            // System under Test
            XDocument xDoc = null;

            using (var stream = File.OpenRead(ANIML_TEST_DOCUMENT))
            {
                xDoc = XDocument.Load(stream);
            }

            // Act & Assert
            Assert.DoesNotThrow(() => xDoc.ValidateAnIMLCoreDocument());
        }

        [TestCase("microplate-sila.animl")]
        public void Should_Serialize_AnIML_To_Xml(string fileName)
        {
            // Arrange
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;

            var filePath = $"{Path.Combine(TEST_FOLDER, fileName)}";
            AnIMLType aniML = null;

            XmlSerializer serializer = new XmlSerializer(typeof(AnIMLType));

            using (var fs = File.OpenRead(filePath))
            {
                using (XmlReader reader = XmlReader.Create(fs, settings))
                {
                    aniML = (AnIMLType)serializer.Deserialize(reader);
                }
            }

            // System under Test & Act
            var result = aniML.GetAnIMLXml();

            // Assert
            Assert.IsNotEmpty(result);
        }
    }
}
