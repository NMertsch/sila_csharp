﻿using System.Globalization;
using System.Text;
using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard.Test.Listdatatypetest.V1;
using SiLA2.Domain;
using SiLA2.Server;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class ListDataTypeTestServiceImpl : ListDataTypeTest.ListDataTypeTestBase
    {
        #region Private members

        private readonly ILogger<ListDataTypeTestServiceImpl> _logger;
        private readonly Feature _silaFeature;

        #endregion

        #region Constructors and destructors

        public ListDataTypeTestServiceImpl(ISiLA2Server siLA2Server, ILogger<ListDataTypeTestServiceImpl> logger)
        {
            _logger = logger;
            _silaFeature = siLA2Server.ReadFeature(Path.Combine("Features", "ListDataTypeTest-v1_0.sila.xml"));
        }

        #endregion

        #region Overrides of ListDataTypeTestBase

        public override Task<Get_EmptyStringList_Responses> Get_EmptyStringList(Get_EmptyStringList_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_EmptyStringList_Responses { EmptyStringList = { } });
        }

        public override Task<EchoStringList_Responses> EchoStringList(EchoStringList_Parameters request, ServerCallContext context)
        {
            var response = new EchoStringList_Responses();
            response.ReceivedValues.AddRange(request.StringList);

            return Task.FromResult(response);
        }

        public override Task<Get_StringList_Responses> Get_StringList(Get_StringList_Parameters request, ServerCallContext context)
        {
            var response = new Get_StringList_Responses
            {
                StringList =
                {
                    new SiLAFramework.String{ Value = "SiLA 2" },
                    new SiLAFramework.String{ Value = "is" },
                    new SiLAFramework.String{ Value = "great" }
                }
            };

            return Task.FromResult(response);
        }

        public override Task<EchoIntegerList_Responses> EchoIntegerList(EchoIntegerList_Parameters request, ServerCallContext context)
        {
            var response = new EchoIntegerList_Responses();
            response.ReceivedValues.AddRange(request.IntegerList);

            return Task.FromResult(response);
        }

        public override Task<Get_IntegerList_Responses> Get_IntegerList(Get_IntegerList_Parameters request, ServerCallContext context)
        {
            var response = new Get_IntegerList_Responses
            {
                IntegerList =
                {
                    new SiLAFramework.Integer{ Value = 1 },
                    new SiLAFramework.Integer{ Value = 2 },
                    new SiLAFramework.Integer{ Value = 3 }
                }
            };

            return Task.FromResult(response);
        }

        public override Task<EchoStructureList_Responses> EchoStructureList(EchoStructureList_Parameters request, ServerCallContext context)
        {
            // assemble log entry
            var sb = new StringBuilder("Received structures: \n");
            foreach (var structureValue in request.StructureList)
            {
                sb.AppendLine("{");

                sb.AppendLine($"  String type value: '{structureValue.TestStructure.StringTypeValue.Value}'");
                sb.AppendLine($"  Integer type value: {structureValue.TestStructure.IntegerTypeValue.Value}");
                sb.AppendLine($"  Real type value: {structureValue.TestStructure.RealTypeValue.Value}");
                sb.AppendLine($"  Boolean type value: {structureValue.TestStructure.BooleanTypeValue.Value}");
                sb.AppendLine($"  Binary type value: {structureValue.TestStructure.BinaryTypeValue.Value.ToStringUtf8()}");
                var dateValue = new DateTime(
                    (int)structureValue.TestStructure.DateTypeValue.Year,
                    (int)structureValue.TestStructure.DateTypeValue.Month,
                    (int)structureValue.TestStructure.DateTypeValue.Day);
                sb.AppendLine($"  Date type value: {dateValue.Date}");
                var timeValue = new TimeSpan(
                    (int)structureValue.TestStructure.TimeTypeValue.Hour,
                    (int)structureValue.TestStructure.TimeTypeValue.Minute,
                    (int)structureValue.TestStructure.TimeTypeValue.Second);
                sb.AppendLine($"  Time type value: {timeValue}");
                var timestampValue = new DateTime(
                    (int)structureValue.TestStructure.TimestampTypeValue.Year,
                    (int)structureValue.TestStructure.TimestampTypeValue.Month,
                    (int)structureValue.TestStructure.TimestampTypeValue.Day,
                    (int)structureValue.TestStructure.TimestampTypeValue.Hour,
                    (int)structureValue.TestStructure.TimestampTypeValue.Minute,
                    (int)structureValue.TestStructure.TimestampTypeValue.Second);
                sb.AppendLine($" Timestamp type value: {timestampValue}");

                var dataType = AnyType.ExtractAnyType(structureValue.TestStructure.AnyTypeValue.Type);
                if (dataType.Item is BasicType)
                {
                    string payload = string.Empty;
                    switch ((BasicType)dataType.Item)
                    {
                        case BasicType.String:
                            payload = SiLAFramework.String.Parser.ParseFrom(structureValue.TestStructure.AnyTypeValue.Payload).Value;
                            break;
                        case BasicType.Integer:
                            payload = SiLAFramework.Integer.Parser.ParseFrom(structureValue.TestStructure.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Real:
                            payload = SiLAFramework.Real.Parser.ParseFrom(structureValue.TestStructure.AnyTypeValue.Payload).Value.ToString(CultureInfo.InvariantCulture);
                            break;
                        case BasicType.Boolean:
                            payload = SiLAFramework.Boolean.Parser.ParseFrom(structureValue.TestStructure.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Binary:
                            var binary = SiLAFramework.Binary.Parser.ParseFrom(structureValue.TestStructure.AnyTypeValue.Payload);
                            if (binary.UnionCase == SiLAFramework.Binary.UnionOneofCase.Value)
                            {
                                payload = SiLAFramework.Boolean.Parser.ParseFrom(structureValue.TestStructure.AnyTypeValue.Payload).Value.ToString();
                                break;
                            }

                            _logger.LogError($"Response contains the wrong union type '{binary.UnionCase}' (expected: '{SiLAFramework.Binary.UnionOneofCase.Value}')");
                            break;
                        case BasicType.Date:
                            payload = SiLAFramework.Date.Parser.ParseFrom(structureValue.TestStructure.AnyTypeValue.Payload).ToString();
                            break;
                        case BasicType.Time:
                            payload = SiLAFramework.Time.Parser.ParseFrom(structureValue.TestStructure.AnyTypeValue.Payload).ToString();
                            break;
                        case BasicType.Timestamp:
                            payload = SiLAFramework.Timestamp.Parser.ParseFrom(structureValue.TestStructure.AnyTypeValue.Payload).ToString();
                            break;
                        default:
                            throw new Exception($"'{dataType.Item}' is not a valid AnyType type");
                    }

                    sb.AppendLine($"  Any type value: {(BasicType)dataType.Item} type {payload}");

                    sb.AppendLine("}");
                }
            }

            _logger.LogInformation(sb.ToString());

            var response = new EchoStructureList_Responses();
            response.ReceivedValues.AddRange(request.StructureList);

            return Task.FromResult(response);
        }

        public override Task<Get_StructureList_Responses> Get_StructureList(Get_StructureList_Parameters request, ServerCallContext context)
        {
            var response = new Get_StructureList_Responses();

            const string stringTypeValue = "SiLA2_Test_String_Value_";
            const int integerTypeValue = 5124;
            const double realTypeValue = 3.1415926;
            const string binaryStringTypeValue = "Binary_String_Value_";
            var dateTimeValue = new DateTime(2022, 8, 5, 12, 34, 56);
            const string anyTypeStringValue = "Any_Type_String_Value_";

            for (uint i = 0; i < 3; i++)
            {
                response.StructureList.Add(new DataType_TestStructure
                {
                    TestStructure = new DataType_TestStructure.Types.TestStructure_Struct
                    {
                        StringTypeValue = new SiLAFramework.String { Value = stringTypeValue + (i + 1) },
                        IntegerTypeValue = new SiLAFramework.Integer { Value = integerTypeValue + i },
                        RealTypeValue = new SiLAFramework.Real { Value = realTypeValue + i },
                        BooleanTypeValue = new SiLAFramework.Boolean { Value = i % 2 == 0 },
                        BinaryTypeValue = new SiLAFramework.Binary { Value = ByteString.CopyFromUtf8(binaryStringTypeValue + (i + 1)) },
                        DateTypeValue = new SiLAFramework.Date
                        {
                            Year = (uint)dateTimeValue.Year + i,
                            Month = (uint)dateTimeValue.Month + i,
                            Day = (uint)dateTimeValue.Day + i
                        },
                        TimeTypeValue = new SiLAFramework.Time
                        {
                            Hour = (uint)dateTimeValue.Hour + i,
                            Minute = (uint)dateTimeValue.Minute + i,
                            Second = (uint)dateTimeValue.Second + i
                        },
                        TimestampTypeValue = new SiLAFramework.Timestamp
                        {
                            Year = (uint)dateTimeValue.Year + i,
                            Month = (uint)dateTimeValue.Month + i,
                            Day = (uint)dateTimeValue.Day + i,
                            Hour = (uint)dateTimeValue.Hour + i,
                            Minute = (uint)dateTimeValue.Minute + i,
                            Second = (uint)dateTimeValue.Second + i
                        },
                        AnyTypeValue = AnyType.CreateAnyTypeObject(anyTypeStringValue + (i + 1))
                    }
                });
            }

            return Task.FromResult(response);
        }

        #endregion
    }
}
