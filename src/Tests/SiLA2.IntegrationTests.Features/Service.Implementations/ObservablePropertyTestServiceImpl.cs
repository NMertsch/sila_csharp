﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard.Test.Observablepropertytest.V1;
using SiLA2.Commands;
using SiLA2.Server;
using SiLA2.Server.Services;

namespace SiLA2.IntegrationTests.Features.Service.Implementations
{
    public class ObservablePropertyTestServiceImpl : ObservablePropertyTest.ObservablePropertyTestBase
    {
        private readonly IObservableCommandManager<SetValue_Parameters, SetValue_Responses> _observableCommandManagerSetValue;
        private readonly IObservableCommandManager<Subscribe_Alternating_Parameters, Subscribe_Alternating_Responses> _observableCommandManagerSubscribeAlternating;
        private readonly ILogger<ObservablePropertyTestServiceImpl> _logger;
        private Feature _feature;

        public ObservablePropertyTestServiceImpl(ISiLA2Server siLA2Server, 
                                                 IObservableCommandManager<SetValue_Parameters, SetValue_Responses> observableCommandManagerSetValue,
                                                 IObservableCommandManager<Subscribe_Alternating_Parameters, Subscribe_Alternating_Responses> observableCommandManagerSubscribeAlternating,
                                                 ILogger<ObservablePropertyTestServiceImpl> logger)
        {
            _observableCommandManagerSetValue = observableCommandManagerSetValue;
            _observableCommandManagerSubscribeAlternating = observableCommandManagerSubscribeAlternating;
            _logger = logger;
            _feature = siLA2Server.ReadFeature(Path.Combine("Features", "ObservablePropertyTest-v1_0.sila.xml"));
        }

        public override async Task<SetValue_Responses> SetValue(SetValue_Parameters request, ServerCallContext context)
        {
            request.ValidateCommandParameters(_feature, "SetValue");
            var response = new SetValue_Responses();
            return response;
        }

        public override async Task Subscribe_Alternating(Subscribe_Alternating_Parameters request, IServerStreamWriter<Subscribe_Alternating_Responses> responseStream, ServerCallContext context)
        {
            int counter = 0; 
            do
            {
                await responseStream.WriteAsync(new Subscribe_Alternating_Responses
                {
                    Alternating = new Sila2.Org.Silastandard.Boolean { Value = counter++ % 2 == 0 }
                });
                await Task.Delay(1000);
            } while (!context.CancellationToken.IsCancellationRequested);
        }

        public override async Task Subscribe_Editable(Subscribe_Editable_Parameters request, IServerStreamWriter<Subscribe_Editable_Responses> responseStream, ServerCallContext context)
        {
            int counter = 1;
            do
            {
                if (counter == 4)
                    break;

                await responseStream.WriteAsync(new Subscribe_Editable_Responses
                {
                    Editable = new Sila2.Org.Silastandard.Integer { Value = counter++ }
                });
                await Task.Delay(1000);

            } while (!context.CancellationToken.IsCancellationRequested);
        }

        public override async Task Subscribe_FixedValue(Subscribe_FixedValue_Parameters request, IServerStreamWriter<Subscribe_FixedValue_Responses> responseStream, ServerCallContext context)
        {
            await responseStream.WriteAsync(new Subscribe_FixedValue_Responses
            {
                FixedValue = new Sila2.Org.Silastandard.Integer { Value = 42 }
            }, context.CancellationToken);
        }
    }
}
