﻿using System.Reflection;
using CommandLine.Text;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard.Test.Metadataprovider.V1;
using SiLA2.Server;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class MetadataProviderServiceImpl : MetadataProvider.MetadataProviderBase
    {
        private readonly Feature _metadataProviderFeature;
        private readonly Feature _metadataConsumerTestFeature;

        private readonly ILogger<MetadataProviderServiceImpl> _logger;

        public MetadataProviderServiceImpl(ISiLA2Server silaServer, ILogger<MetadataProviderServiceImpl> logger)
        {
            _logger = logger;
            _metadataProviderFeature = silaServer.ReadFeature(Path.Combine("Features", "MetadataProvider-v1_0.sila.xml"));
            _metadataConsumerTestFeature = silaServer.GetFeature("org.silastandard/test/MetadataConsumerTest/v1");

            silaServer.MetadataManager.CollectMetadataAffections(_metadataProviderFeature, this);
        }

        public override Task<Get_FCPAffectedByMetadata_StringMetadata_Responses> Get_FCPAffectedByMetadata_StringMetadata(Get_FCPAffectedByMetadata_StringMetadata_Parameters request, ServerCallContext context)
        {
            var response = new Get_FCPAffectedByMetadata_StringMetadata_Responses();
            response.AffectedCalls.Add(new Sila2.Org.Silastandard.String { Value = _metadataConsumerTestFeature.FullyQualifiedIdentifier });
            return Task.FromResult(response);
        }

        public override Task<Get_FCPAffectedByMetadata_TwoIntegersMetadata_Responses> Get_FCPAffectedByMetadata_TwoIntegersMetadata(Get_FCPAffectedByMetadata_TwoIntegersMetadata_Parameters request, ServerCallContext context)
        {
            var response = new Get_FCPAffectedByMetadata_TwoIntegersMetadata_Responses();
            response.AffectedCalls.Add(new Sila2.Org.Silastandard.String { Value = _metadataConsumerTestFeature.GetFullyQualifiedCommandIdentifier("UnpackMetadata") });
            return Task.FromResult(response);
        }
    }
}
