﻿using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard.Test.Anytypetest.V1;
using SiLA2.Domain;
using SiLA2.Server;
using SiLA2.Server.Utils;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class AnyTypeTestServiceImpl : AnyTypeTest.AnyTypeTestBase
    {
        #region Private members

        private readonly Feature _silaFeature;

        private readonly ILogger<AnyTypeTestServiceImpl> _logger;

        #endregion

        #region Constructors and destructors

        public AnyTypeTestServiceImpl(ISiLA2Server silaServer, ILogger<AnyTypeTestServiceImpl> logger)
        {
            _logger = logger;
            _silaFeature = silaServer.ReadFeature(Path.Combine("Features", "AnyTypeTest-v1_0.sila.xml"));
        }

        #endregion

        #region Overrides of AnyTypeTestBase

        public override Task<SetAnyTypeValue_Responses> SetAnyTypeValue(SetAnyTypeValue_Parameters request, ServerCallContext context)
        {
            try
            {
                // deserialize the type information
                var dataType = AnyType.ExtractAnyType(request.AnyTypeValue.Type);

                if (dataType.Item is BasicType)
                {
                    var payload = string.Empty;
                    switch ((BasicType)dataType.Item)
                    {
                        case BasicType.String:
                            payload = SiLAFramework.String.Parser.ParseFrom(request.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Integer:
                            payload = SiLAFramework.Integer.Parser.ParseFrom(request.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Real:
                            payload = SiLAFramework.Real.Parser.ParseFrom(request.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Boolean:
                            payload = SiLAFramework.Boolean.Parser.ParseFrom(request.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Binary:
                            var binary = SiLAFramework.Binary.Parser.ParseFrom(request.AnyTypeValue.Payload);
                            if (binary.UnionCase == SiLAFramework.Binary.UnionOneofCase.Value)
                            {
                                payload = SiLAFramework.Boolean.Parser.ParseFrom(request.AnyTypeValue.Payload).Value.ToString();
                                break;
                            }

                            _logger.LogError($"Response contains the wrong union type '{binary.UnionCase}' (expected: '{SiLAFramework.Binary.UnionOneofCase.Value}')");
                            break;
                        case BasicType.Date:
                            payload = SiLAFramework.Date.Parser.ParseFrom(request.AnyTypeValue.Payload).ToString();
                            break;
                        case BasicType.Time:
                            payload = SiLAFramework.Time.Parser.ParseFrom(request.AnyTypeValue.Payload).ToString();
                            break;
                        case BasicType.Timestamp:
                            payload = SiLAFramework.Timestamp.Parser.ParseFrom(request.AnyTypeValue.Payload).ToString();
                            break;
                        default:
                            throw new Exception($"'{dataType.Item.ToString()}' is not a valid AnyType type");
                    }

                    return Task.FromResult(new SetAnyTypeValue_Responses
                    {
                        ReceivedAnyType = new SiLAFramework.String { Value = ((BasicType)dataType.Item).ToString() },
                        ReceivedValue = request.AnyTypeValue
                    });
                }
            }
            catch (System.Xml.Schema.XmlSchemaValidationException ex)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    _silaFeature.GetFullyQualifiedCommandParameterIdentifier("SetAnyTypeValue", "AnyTypeValue"),
                    $"Given type description '{request.AnyTypeValue.Type}' is not matching the schema AnyTypeDataType.xsd: {ex.Message}"));
            }
            catch (Exception ex)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError("Exception in command SetAnyTypeValue: " + ex.Message));
            }

            return base.SetAnyTypeValue(request, context);
        }

        public override Task<Get_AnyTypeStringValue_Responses> Get_AnyTypeStringValue(Get_AnyTypeStringValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeStringValue_Responses
            {
                AnyTypeStringValue = AnyType.CreateAnyTypeObject("SiLA_Any_type_of_String_type")
            });
        }

        public override Task<Get_AnyTypeIntegerValue_Responses> Get_AnyTypeIntegerValue(Get_AnyTypeIntegerValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeIntegerValue_Responses
            {
                AnyTypeIntegerValue = AnyType.CreateAnyTypeObject(5124)
            });
        }

        public override Task<Get_AnyTypeRealValue_Responses> Get_AnyTypeRealValue(Get_AnyTypeRealValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeRealValue_Responses
            {
                AnyTypeRealValue = AnyType.CreateAnyTypeObject(3.1415926)
            });
        }

        public override Task<Get_AnyTypeBooleanValue_Responses> Get_AnyTypeBooleanValue(Get_AnyTypeBooleanValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeBooleanValue_Responses
            {
                AnyTypeBooleanValue = AnyType.CreateAnyTypeObject(true)
            });
        }

        public override Task<Get_AnyTypeBinaryValue_Responses> Get_AnyTypeBinaryValue(Get_AnyTypeBinaryValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeBinaryValue_Responses
            {
                AnyTypeBinaryValue = AnyType.CreateAnyTypeObject(new SiLAFramework.Binary
                {
                    Value = ByteString.CopyFromUtf8("SiLA_Any_type_of_Binary_type")
                })
            });
        }

        public override Task<Get_AnyTypeDateValue_Responses> Get_AnyTypeDateValue(Get_AnyTypeDateValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeDateValue_Responses
            {
                AnyTypeDateValue = AnyType.CreateAnyTypeObject(new SiLAFramework.Date
                {
                    Year = 2022,
                    Month = 8,
                    Day = 5,
                    Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 }
                })
            });
        }

        public override Task<Get_AnyTypeTimeValue_Responses> Get_AnyTypeTimeValue(Get_AnyTypeTimeValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeTimeValue_Responses
            {
                AnyTypeTimeValue = AnyType.CreateAnyTypeObject(new SiLAFramework.Time
                {
                    Hour = 12,
                    Minute = 34,
                    Second = 56,
                    Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 }
                })
            });
        }

        public override Task<Get_AnyTypeTimestampValue_Responses> Get_AnyTypeTimestampValue(Get_AnyTypeTimestampValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeTimestampValue_Responses
            {
                AnyTypeTimestampValue = AnyType.CreateAnyTypeObject(new SiLAFramework.Timestamp
                {
                    Year = 2022,
                    Month = 8,
                    Day = 5,
                    Hour = 12,
                    Minute = 34,
                    Second = 56,
                    Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 }
                })
            });
        }

        public override Task<Get_AnyTypeListValue_Responses> Get_AnyTypeListValue(Get_AnyTypeListValue_Parameters request, ServerCallContext context)
        {
            var list = new Google.Protobuf.Collections.RepeatedField<SiLAFramework.String>
            {
                new SiLAFramework.String {Value = "SiLA 2"},
                new SiLAFramework.String {Value = "Any"},
                new SiLAFramework.String {Value = "Type"},
                new SiLAFramework.String {Value = "String"},
                new SiLAFramework.String {Value = "List"}
            };

            FieldCodec<SiLAFramework.String> codec = FieldCodec.ForMessage(10, SiLAFramework.String.Parser);
            byte[] bytes = new byte[list.CalculateSize(codec)];
            CodedOutputStream cos = new CodedOutputStream(bytes);
            list.WriteTo(cos, codec);

            // serialize type
            string typeStringXML;
            var value = new DataTypeType { Item = new ListType { DataType = new DataTypeType { Item = BasicType.String } } };
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(value.GetType());
            using (System.IO.StringWriter textWriter = new System.IO.StringWriter())
            {
                xmlSerializer.Serialize(textWriter, value);
                typeStringXML = textWriter.ToString();
            }

            return Task.FromResult(new Get_AnyTypeListValue_Responses
            {
                AnyTypeListValue = new SiLAFramework.Any
                {
                    Type = typeStringXML,
                    Payload = ByteString.CopyFrom(bytes)
                }
            });
        }

        public class Structure
        {
            public SiLAFramework.String StringTypeValue { get; set; }
            public SiLAFramework.Integer IntegerTypeValue { get; set; }
            public SiLAFramework.Date DateTypeValue { get; set; }
        }

        //TODO: implementation not finished
        public override Task<Get_AnyTypeStructureValue_Responses> Get_AnyTypeStructureValue(Get_AnyTypeStructureValue_Parameters request, ServerCallContext context)
        {
            // serialize type
            string typeStringXML;
            var structureType = new DataTypeType
            {
                Item = new StructureType
                {
                    Element = new[]
                    {
                        new SiLAElement {Identifier = "StructElementString", DisplayName = "Struct Element String", Description = "A String type structure element", DataType = new DataTypeType {Item = BasicType.String}},
                        new SiLAElement {Identifier = "StructElementInteger", DisplayName = "Struct Element Integer", Description = "An Integer type structure element", DataType = new DataTypeType {Item = BasicType.Integer}},
                        new SiLAElement {Identifier = "StructElementDate", DisplayName = "Struct Element Date", Description = "A Date type structure element", DataType = new DataTypeType {Item = BasicType.Date}}
                    }
                }
            };
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(structureType.GetType());
            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, structureType);
                typeStringXML = textWriter.ToString();
            }

            var structureValue = new Structure
            {
                StringTypeValue = new SiLAFramework.String { Value = "A String value" },
                IntegerTypeValue = new SiLAFramework.Integer { Value = 83737665 },
                DateTypeValue = new SiLAFramework.Date { Year = 2022, Month = 8, Day = 5, Timezone = new SiLAFramework.Timezone { Hours = 2 } }
            };

            var structure = new Google.Protobuf.WellKnownTypes.Struct();
            structure.Fields.Add("StructElementString", new Google.Protobuf.WellKnownTypes.Value());

            byte[] bytes = new byte[structure.CalculateSize()];
            var cos = new CodedOutputStream(bytes);
            structure.WriteTo(cos);

            return Task.FromResult(new Get_AnyTypeStructureValue_Responses
            {
                AnyTypeStructureValue = new SiLAFramework.Any
                {
                    Type = typeStringXML,
                    Payload = ByteString.CopyFrom(bytes)
                }
            });
        }

        #endregion
    }
}
