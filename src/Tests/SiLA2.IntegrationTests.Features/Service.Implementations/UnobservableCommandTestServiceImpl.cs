﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard.Test.Unobservablecommandtest.V1;
using SiLA2.Server;
using SiLA2.Server.Utils;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class UnobservableCommandTestServiceImpl : UnobservableCommandTest.UnobservableCommandTestBase
    {
        private readonly ILogger<UnobservableCommandTestServiceImpl> _logger;
        private readonly Feature _silaFeature;

        public UnobservableCommandTestServiceImpl(ISiLA2Server siLA2Server, ILogger<UnobservableCommandTestServiceImpl> logger)
        {
            _logger = logger;
            _silaFeature = siLA2Server.ReadFeature(Path.Combine("Features", "UnobservableCommandTest-v1_0.sila.xml"));
        }
        
        public override Task<CommandWithoutParametersAndResponses_Responses> CommandWithoutParametersAndResponses(CommandWithoutParametersAndResponses_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new CommandWithoutParametersAndResponses_Responses());
        }

        public override Task<ConvertIntegerToString_Responses> ConvertIntegerToString(ConvertIntegerToString_Parameters request, ServerCallContext context)
        {
            if(request.Integer == null)
            {
                ErrorHandling.RaiseSiLAError(new SiLAFramework.SiLAError { ValidationError = new SiLAFramework.ValidationError { Parameter = "org.silastandard/test/UnobservableCommandTest/v1/Command/ConvertIntegerToString/Parameter/Integer", Message = "Parameter must not be null !" } });
            }

            return Task.FromResult(new ConvertIntegerToString_Responses
            {
                StringRepresentation = new SiLAFramework.String { Value = request.Integer.Value.ToString() }
            });
        }

        public override Task<JoinIntegerAndString_Responses> JoinIntegerAndString(JoinIntegerAndString_Parameters request, ServerCallContext context)
        {
            if (request.Integer == null)
            {
                ErrorHandling.RaiseSiLAError(new SiLAFramework.SiLAError { ValidationError = new SiLAFramework.ValidationError { Parameter = "org.silastandard/test/UnobservableCommandTest/v1/Command/JoinIntegerAndString/Parameter/Integer", Message = "Parameter must not be null !" } });
            }

            if (request.String == null || request.String.Value == null)
            {
                ErrorHandling.RaiseSiLAError(new SiLAFramework.SiLAError { ValidationError = new SiLAFramework.ValidationError { Parameter = "org.silastandard/test/UnobservableCommandTest/v1/Command/JoinIntegerAndString/Parameter/String", Message = "Parameter must not be null !" } });
            }

            return Task.FromResult(new JoinIntegerAndString_Responses
            {
                JoinedParameters = new SiLAFramework.String { Value = $"{request.Integer.Value}{request.String.Value}" }
            });
        }

        public override Task<SplitStringAfterFirstCharacter_Responses> SplitStringAfterFirstCharacter(SplitStringAfterFirstCharacter_Parameters request, ServerCallContext context)
        {
            if (request.String == null || request.String.Value == null)
            {
                ErrorHandling.RaiseSiLAError(new SiLAFramework.SiLAError { ValidationError = new SiLAFramework.ValidationError { Parameter = "org.silastandard/test/UnobservableCommandTest/v1/Command/SplitStringAfterFirstCharacter/Parameter/String", Message = "Parameter must not be null !" } });
            }

            return Task.FromResult(new SplitStringAfterFirstCharacter_Responses
            {
                FirstCharacter = new SiLAFramework.String
                {
                    Value = string.IsNullOrEmpty(request.String.Value) ? string.Empty : request.String.Value.Substring(0, 1)
                },
                Remainder = new SiLAFramework.String
                {
                    Value = request.String.Value.Length < 2 ? string.Empty : request.String.Value.Substring(1)
                }
            });
        }
    }
}
