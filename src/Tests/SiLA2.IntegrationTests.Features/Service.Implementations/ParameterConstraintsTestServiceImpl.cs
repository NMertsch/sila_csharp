﻿using Grpc.Core;
using Sila2.Org.Silastandard.Test.Parameterconstraintstest.V1;
using SiLA2.Server.Utils;
using System.Diagnostics;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class ParameterConstraintsTestServiceImpl : ParameterConstraintsTest.ParameterConstraintsTestBase
    {
        private readonly Feature _silaFeature;

        public ParameterConstraintsTestServiceImpl(Server.ISiLA2Server silaServer)
        {
            _silaFeature = silaServer.ReadFeature(Path.Combine("Features", "ParameterConstraintsTest-v1_0.sila.xml"));
        }

        #region Overrides of ParameterConstraintsTestBase

        #region String type constraints

        public override Task<CheckStringConstraintLength_Responses> CheckStringConstraintLength(CheckStringConstraintLength_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckStringConstraintLength_Responses());
        }

        public override Task<CheckStringConstraintMinimalLength_Responses> CheckStringConstraintMinimalLength(CheckStringConstraintMinimalLength_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckStringConstraintMinimalLength_Responses());
        }

        public override Task<CheckStringConstraintMaximalLength_Responses> CheckStringConstraintMaximalLength(CheckStringConstraintMaximalLength_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckStringConstraintMaximalLength_Responses());
        }

        public override Task<CheckStringConstraintMinMaxLength_Responses> CheckStringConstraintMinMaxLength(CheckStringConstraintMinMaxLength_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckStringConstraintMinMaxLength_Responses());
        }

        public override Task<CheckStringConstraintSet_Responses> CheckStringConstraintSet(CheckStringConstraintSet_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckStringConstraintSet_Responses());
        }

        public override Task<CheckStringConstraintPattern_Responses> CheckStringConstraintPattern(CheckStringConstraintPattern_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckStringConstraintPattern_Responses());
        }

        public override Task<CheckStringConstraintContentType_Responses> CheckStringConstraintContentType(CheckStringConstraintContentType_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckStringConstraintContentType_Responses());
        }

        public override Task<CheckStringConstraintFullyQualifiedIdentifier_Responses> CheckStringConstraintFullyQualifiedIdentifier(CheckStringConstraintFullyQualifiedIdentifier_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.FeatureIdentifier.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "FeatureIdentifier");
            Validation.ValidateParameter(request.CommandIdentifier.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "CommandIdentifier");
            Validation.ValidateParameter(request.CommandParameterIdentifier.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "CommandParameterIdentifier");
            Validation.ValidateParameter(request.CommandResponseIdentifier.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "CommandResponseIdentifier");
            Validation.ValidateParameter(request.IntermediateCommandResponseIdentifier.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "IntermediateCommandResponseIdentifier");
            Validation.ValidateParameter(request.ExecutionErrorIdentifier.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ExecutionErrorIdentifier");
            Validation.ValidateParameter(request.PropertyIdentifier.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "PropertyIdentifier");
            Validation.ValidateParameter(request.CustomDataTypeIdentifier.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "CustomDataTypeIdentifier");
            Validation.ValidateParameter(request.MetadataIdentifier.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "MetadataIdentifier");
            return Task.FromResult(new CheckStringConstraintFullyQualifiedIdentifier_Responses());
        }

        public override Task<CheckStringConstraintSchema_Responses> CheckStringConstraintSchema(CheckStringConstraintSchema_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckStringConstraintSchema_Responses());
        }

        #endregion

        #region Integer type constraints

        public override Task<CheckIntegerConstraintSet_Responses> CheckIntegerConstraintSet(CheckIntegerConstraintSet_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckIntegerConstraintSet_Responses());
        }

        public override Task<CheckScientificallyNotatedIntegerLimit_Responses> CheckScientificallyNotatedIntegerLimit(CheckScientificallyNotatedIntegerLimit_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckScientificallyNotatedIntegerLimit_Responses());
        }

        public override Task<CheckIntegerConstraintMaximalExclusive_Responses> CheckIntegerConstraintMaximalExclusive(CheckIntegerConstraintMaximalExclusive_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckIntegerConstraintMaximalExclusive_Responses());
        }

        public override Task<CheckIntegerConstraintMaximalInclusive_Responses> CheckIntegerConstraintMaximalInclusive(CheckIntegerConstraintMaximalInclusive_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckIntegerConstraintMaximalInclusive_Responses());
        }

        public override Task<CheckIntegerConstraintMinimalExclusive_Responses> CheckIntegerConstraintMinimalExclusive(CheckIntegerConstraintMinimalExclusive_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckIntegerConstraintMinimalExclusive_Responses());
        }

        public override Task<CheckIntegerConstraintMinimalInclusive_Responses> CheckIntegerConstraintMinimalInclusive(CheckIntegerConstraintMinimalInclusive_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckIntegerConstraintMinimalInclusive_Responses());
        }

        public override Task<CheckIntegerConstraintMinMax_Responses> CheckIntegerConstraintMinMax(CheckIntegerConstraintMinMax_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckIntegerConstraintMinMax_Responses());
        }

        public override Task<CheckIntegerConstraintUnit_Responses> CheckIntegerConstraintUnit(CheckIntegerConstraintUnit_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckIntegerConstraintUnit_Responses());
        }

        #endregion

        #region Real type constraints

        public override Task<CheckRealConstraintSet_Responses> CheckRealConstraintSet(CheckRealConstraintSet_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckRealConstraintSet_Responses());
        }

        public override Task<CheckScientificallyNotatedRealLimit_Responses> CheckScientificallyNotatedRealLimit(CheckScientificallyNotatedRealLimit_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckScientificallyNotatedRealLimit_Responses());
        }

        public override Task<CheckRealConstraintMaximalExclusive_Responses> CheckRealConstraintMaximalExclusive(CheckRealConstraintMaximalExclusive_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckRealConstraintMaximalExclusive_Responses());
        }

        public override Task<CheckRealConstraintMaximalInclusive_Responses> CheckRealConstraintMaximalInclusive(CheckRealConstraintMaximalInclusive_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckRealConstraintMaximalInclusive_Responses());
        }

        public override Task<CheckRealConstraintMinimalExclusive_Responses> CheckRealConstraintMinimalExclusive(CheckRealConstraintMinimalExclusive_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckRealConstraintMinimalExclusive_Responses());
        }

        public override Task<CheckRealConstraintMinimalInclusive_Responses> CheckRealConstraintMinimalInclusive(CheckRealConstraintMinimalInclusive_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckRealConstraintMinimalInclusive_Responses());
        }

        public override Task<CheckRealConstraintMinMax_Responses> CheckRealConstraintMinMax(CheckRealConstraintMinMax_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckRealConstraintMinMax_Responses());
        }

        public override Task<CheckRealConstraintUnit_Responses> CheckRealConstraintUnit(CheckRealConstraintUnit_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckRealConstraintUnit_Responses());
        }

        #endregion

        #region Date type constraints

        public override Task<CheckDateConstraintSet_Responses> CheckDateConstraintSet(CheckDateConstraintSet_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckDateConstraintSet_Responses());
        }

        public override Task<CheckDateConstraintMaximalExclusive_Responses> CheckDateConstraintMaximalExclusive(CheckDateConstraintMaximalExclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckDateConstraintMaximalExclusive_Responses());
        }

        public override Task<CheckDateConstraintMaximalInclusive_Responses> CheckDateConstraintMaximalInclusive(CheckDateConstraintMaximalInclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckDateConstraintMaximalInclusive_Responses());
        }

        public override Task<CheckDateConstraintMinimalExclusive_Responses> CheckDateConstraintMinimalExclusive(CheckDateConstraintMinimalExclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckDateConstraintMinimalExclusive_Responses());
        }

        public override Task<CheckDateConstraintMinimalInclusive_Responses> CheckDateConstraintMinimalInclusive(CheckDateConstraintMinimalInclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckDateConstraintMinimalInclusive_Responses());
        }

        public override Task<CheckDateConstraintMinMax_Responses> CheckDateConstraintMinMax(CheckDateConstraintMinMax_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckDateConstraintMinMax_Responses());
        }

        #endregion

        #region Time type constraints

        public override Task<CheckTimeConstraintSet_Responses> CheckTimeConstraintSet(CheckTimeConstraintSet_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimeConstraintSet_Responses());
        }

        public override Task<CheckTimeConstraintMaximalExclusive_Responses> CheckTimeConstraintMaximalExclusive(CheckTimeConstraintMaximalExclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimeConstraintMaximalExclusive_Responses());
        }

        public override Task<CheckTimeConstraintMaximalInclusive_Responses> CheckTimeConstraintMaximalInclusive(CheckTimeConstraintMaximalInclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimeConstraintMaximalInclusive_Responses());
        }

        public override Task<CheckTimeConstraintMinimalExclusive_Responses> CheckTimeConstraintMinimalExclusive(CheckTimeConstraintMinimalExclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimeConstraintMinimalExclusive_Responses());
        }

        public override Task<CheckTimeConstraintMinimalInclusive_Responses> CheckTimeConstraintMinimalInclusive(CheckTimeConstraintMinimalInclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimeConstraintMinimalInclusive_Responses());
        }

        public override Task<CheckTimeConstraintMinMax_Responses> CheckTimeConstraintMinMax(CheckTimeConstraintMinMax_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimeConstraintMinMax_Responses());
        }

        #endregion

        #region Timestamp type constraints

        public override Task<CheckTimestampConstraintSet_Responses> CheckTimestampConstraintSet(CheckTimestampConstraintSet_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimestampConstraintSet_Responses());
        }

        public override Task<CheckTimestampConstraintMaximalExclusive_Responses> CheckTimestampConstraintMaximalExclusive(CheckTimestampConstraintMaximalExclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimestampConstraintMaximalExclusive_Responses());
        }

        public override Task<CheckTimestampConstraintMaximalInclusive_Responses> CheckTimestampConstraintMaximalInclusive(CheckTimestampConstraintMaximalInclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimestampConstraintMaximalInclusive_Responses());
        }

        public override Task<CheckTimestampConstraintMinimalExclusive_Responses> CheckTimestampConstraintMinimalExclusive(CheckTimestampConstraintMinimalExclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimestampConstraintMinimalExclusive_Responses());
        }

        public override Task<CheckTimestampConstraintMinimalInclusive_Responses> CheckTimestampConstraintMinimalInclusive(CheckTimestampConstraintMinimalInclusive_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimestampConstraintMinimalInclusive_Responses());
        }

        public override Task<CheckTimestampConstraintMinMax_Responses> CheckTimestampConstraintMinMax(CheckTimestampConstraintMinMax_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckTimestampConstraintMinMax_Responses());
        }

        #endregion

        #region List type constraints

        public override Task<CheckListConstraintElementCount_Responses> CheckListConstraintElementCount(CheckListConstraintElementCount_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckListConstraintElementCount_Responses());
        }

        public override Task<CheckListConstraintMinimalElementCount_Responses> CheckListConstraintMinimalElementCount(CheckListConstraintMinimalElementCount_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckListConstraintMinimalElementCount_Responses());
        }

        public override Task<CheckListConstraintMaximalElementCount_Responses> CheckListConstraintMaximalElementCount(CheckListConstraintMaximalElementCount_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckListConstraintMaximalElementCount_Responses());
        }

        public override Task<CheckListConstraintMinMaxElementCount_Responses> CheckListConstraintMinMaxElementCount(CheckListConstraintMinMaxElementCount_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckListConstraintMinMaxElementCount_Responses());
        }

        #endregion

        #region Binary type constraints

        public override Task<CheckBinaryConstraintLength_Responses> CheckBinaryConstraintLength(CheckBinaryConstraintLength_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckBinaryConstraintLength_Responses());
        }

        public override Task<CheckBinaryConstraintMinimalLength_Responses> CheckBinaryConstraintMinimalLength(CheckBinaryConstraintMinimalLength_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckBinaryConstraintMinimalLength_Responses());
        }

        public override Task<CheckBinaryConstraintMaximalLength_Responses> CheckBinaryConstraintMaximalLength(CheckBinaryConstraintMaximalLength_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckBinaryConstraintMaximalLength_Responses());
        }

        public override Task<CheckBinaryConstraintMinMaxLength_Responses> CheckBinaryConstraintMinMaxLength(CheckBinaryConstraintMinMaxLength_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckBinaryConstraintMinMaxLength_Responses());
        }

        public override Task<CheckBinaryConstraintContentType_Responses> CheckBinaryConstraintContentType(CheckBinaryConstraintContentType_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckBinaryConstraintContentType_Responses());
        }

        public override Task<CheckBinaryConstraintSchema_Responses> CheckBinaryConstraintSchema(CheckBinaryConstraintSchema_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckBinaryConstraintSchema_Responses());
        }

        #endregion

        #region Any type constraints

        public override Task<CheckAllowedTypesConstraint_Responses> CheckAllowedTypesConstraint(CheckAllowedTypesConstraint_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckAllowedTypesConstraint_Responses());
        }

        public override Task<CheckAllowedStructureTypeConstraints_Responses> CheckAllowedStructureTypeConstraints(CheckAllowedStructureTypeConstraints_Parameters request, ServerCallContext context)
        {
            //Validation.ValidateParameter(request.ConstrainedParameter.Value, _silaFeature, new StackTrace().GetFrame(0)?.GetMethod()?.Name, "ConstrainedParameter");
            return Task.FromResult(new CheckAllowedStructureTypeConstraints_Responses());
        }

        #endregion

        #endregion
    }
}
