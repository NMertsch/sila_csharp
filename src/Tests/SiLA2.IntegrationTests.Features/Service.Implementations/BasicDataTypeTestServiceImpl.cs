﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard.Test.Basicdatatypestest.V1;
using SiLA2.Server;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class BasicDataTypeTestServiceImpl : BasicDataTypesTest.BasicDataTypesTestBase
    {
        private readonly ILogger<BasicDataTypeTestServiceImpl> _logger;
        private readonly Feature _silaFeature;

        public BasicDataTypeTestServiceImpl(ISiLA2Server silaServer, ILogger<BasicDataTypeTestServiceImpl> logger)
        {
            _logger = logger;
            _silaFeature = silaServer.ReadFeature(Path.Combine("Features", "BasicDataTypesTest-v1_0.sila.xml"));
        }

        #region Overrides of BasicDataTypesTestBase

        #region String

        public override Task<EchoStringValue_Responses> EchoStringValue(EchoStringValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new EchoStringValue_Responses { ReceivedValue = request.StringValue });
        }

        public override Task<Get_StringValue_Responses> Get_StringValue(Get_StringValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_StringValue_Responses { StringValue = new SiLAFramework.String { Value = "SiLA2_Test_String_Value" } });
        }

        #endregion

        #region Integer

        public override Task<EchoIntegerValue_Responses> EchoIntegerValue(EchoIntegerValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new EchoIntegerValue_Responses { ReceivedValue = request.IntegerValue });
        }

        public override Task<Get_IntegerValue_Responses> Get_IntegerValue(Get_IntegerValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_IntegerValue_Responses { IntegerValue = new SiLAFramework.Integer { Value = 5124 } });
        }

        #endregion

        #region Real

        public override Task<EchoRealValue_Responses> EchoRealValue(EchoRealValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new EchoRealValue_Responses { ReceivedValue = request.RealValue });
        }

        public override Task<Get_RealValue_Responses> Get_RealValue(Get_RealValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_RealValue_Responses { RealValue = new SiLAFramework.Real { Value = 3.1415926 } });
        }

        #endregion

        #region Boolean

        public override Task<EchoBooleanValue_Responses> EchoBooleanValue(EchoBooleanValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new EchoBooleanValue_Responses { ReceivedValue = request.BooleanValue });
        }

        public override Task<Get_BooleanValue_Responses> Get_BooleanValue(Get_BooleanValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_BooleanValue_Responses { BooleanValue = new SiLAFramework.Boolean { Value = true } });
        }

        #endregion

        #region Date

        public override Task<EchoDateValue_Responses> EchoDateValue(EchoDateValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new EchoDateValue_Responses { ReceivedValue = request.DateValue });
        }

        public override Task<Get_DateValue_Responses> Get_DateValue(Get_DateValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_DateValue_Responses { DateValue = new SiLAFramework.Date { Year = 2022, Month = 8, Day = 5, Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 } } });
        }

        #endregion

        #region Time

        public override Task<EchoTimeValue_Responses> EchoTimeValue(EchoTimeValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new EchoTimeValue_Responses { ReceivedValue = request.TimeValue });
        }

        public override Task<Get_TimeValue_Responses> Get_TimeValue(Get_TimeValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_TimeValue_Responses { TimeValue = new SiLAFramework.Time { Hour = 12, Minute = 34, Second = 56, Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 } } });
        }

        #endregion

        #region Timestamp

        public override Task<EchoTimestampValue_Responses> EchoTimestampValue(EchoTimestampValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new EchoTimestampValue_Responses { ReceivedValue = request.TimestampValue });
        }

        public override Task<Get_TimestampValue_Responses> Get_TimestampValue(Get_TimestampValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_TimestampValue_Responses { TimestampValue = new SiLAFramework.Timestamp { Year = 2022, Month = 8, Day = 5, Hour = 12, Minute = 34, Second = 56, Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 } } });
        }

        #endregion

        #endregion
    }
}
