﻿using System.Text;
using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using SiLA2.Commands;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Test.Binarytransfertest.V1;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Server.Utils;
using SiLA2.Utils;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class BinaryTransferTestServiceImpl : BinaryTransferTest.BinaryTransferTestBase
    {
        #region Private members

        private readonly Feature _silaFeature;

        private readonly IBinaryDownloadRepository _binaryDownloadRepository;
        private readonly IBinaryUploadRepository _binaryUploadRepository;

        private readonly IObservableCommandManager<EchoBinariesObservably_Parameters, EchoBinariesObservably_Responses> _echoBinariesObservablyCommandManager;

        private readonly List<byte> _intermediateResponseDataToBeSent = new List<byte>();

        private readonly ILogger<BinaryTransferTestServiceImpl> _logger;

        #endregion

        #region Constructors and destructors

        public BinaryTransferTestServiceImpl(
            ISiLA2Server silaServer,
            IBinaryDownloadRepository binaryDownloadRepository,
            IBinaryUploadRepository binaryUploadRepository,
            IObservableCommandManager<EchoBinariesObservably_Parameters, EchoBinariesObservably_Responses> echoBinariesObservablyCommandManager,
            ILogger<BinaryTransferTestServiceImpl> logger)
        {
            _silaFeature = silaServer.ReadFeature(Path.Combine("Features", "BinaryTransferTest-v1_0.sila.xml"));
            _binaryDownloadRepository = binaryDownloadRepository;
            _binaryUploadRepository = binaryUploadRepository;
            _echoBinariesObservablyCommandManager = echoBinariesObservablyCommandManager;
            _logger = logger;

            silaServer.MetadataManager.CollectMetadataAffections(_silaFeature, this);
        }

        #endregion

        #region Overrides of BinaryTransferTestBase

        public override Task<EchoBinaryValue_Responses> EchoBinaryValue(EchoBinaryValue_Parameters request, ServerCallContext context)
        {
            request.ValidateCommandParameters(_silaFeature, "EchoBinaryValue");

            // check type of binary data
            switch (request.BinaryValue.UnionCase)
            {
                case Binary.UnionOneofCase.Value:
                    return Task.FromResult(new EchoBinaryValue_Responses { ReceivedValue = new Binary { Value = request.BinaryValue.Value } });

                case Binary.UnionOneofCase.BinaryTransferUUID:

                    // check for available upload and copy received data and provide it as response
                    var responseDataUUID = Guid.NewGuid();
                    try
                    {
                        _binaryDownloadRepository.DownloadDataMap.TryAdd(responseDataUUID, _binaryUploadRepository.GetUploadedData(request.BinaryValue.BinaryTransferUUID).Result);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(_silaFeature.GetFullyQualifiedCommandParameterIdentifier("EchoBinaryValue", "BinaryValue"), ex.Message));
                    }

                    // create response
                    return Task.FromResult(new EchoBinaryValue_Responses { ReceivedValue = new Binary { BinaryTransferUUID = responseDataUUID.ToString() } });
            }

            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(_silaFeature.GetFullyQualifiedCommandParameterIdentifier("EchoBinaryValue", "BinaryValue"),
                "The value must either be 'Value' or 'BinaryTransferUUID'"));

            return null;
        }

        public override Task<Get_BinaryValueDirectly_Responses> Get_BinaryValueDirectly(Get_BinaryValueDirectly_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_BinaryValueDirectly_Responses
            {
                BinaryValueDirectly = new Binary { Value = ByteString.CopyFromUtf8("SiLA2_Test_String_Value") }
            });
        }

        public override Task<Get_BinaryValueDownload_Responses> Get_BinaryValueDownload(Get_BinaryValueDownload_Parameters request, ServerCallContext context)
        {
            // create binary download
            var data = new StringBuilder();
            for (var i = 0; i < 100000; i++)
            {
                data.Append("A_slightly_longer_SiLA2_Test_String_Value_used_to_demonstrate_the_binary_download");
            }

            Guid binaryTransferUUID = Guid.NewGuid();
            _binaryDownloadRepository.DownloadDataMap.TryAdd(binaryTransferUUID, ByteString.CopyFromUtf8(data.ToString()).ToByteArray());

            return Task.FromResult(new Get_BinaryValueDownload_Responses
            {
                BinaryValueDownload = new Binary { BinaryTransferUUID = binaryTransferUUID.ToString() }
            });
        }

        #region Command EchoBinariesObservably

        public override async Task<CommandConfirmation> EchoBinariesObservably(EchoBinariesObservably_Parameters request, ServerCallContext context)
        {
            request.ValidateCommandParameters(_silaFeature, "EchoBinariesObservably");

            foreach (var e in request.Binaries)
            {
                if (e.UnionCase == Binary.UnionOneofCase.BinaryTransferUUID)
                {
                    try
                    {
                        await _binaryUploadRepository.CheckBinaryUploadTransferUUID(e.BinaryTransferUUID);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(_silaFeature.GetFullyQualifiedCommandParameterIdentifier("EchoBinariesObservably", "Binaries"), ex.Message));
                    }
                }
            }

            var command = await _echoBinariesObservablyCommandManager.AddCommand(request, (x, request, z) => new EchoBinariesObservably_Responses());
            return command.Confirmation;
        }

        public override async Task EchoBinariesObservably_Info(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                _echoBinariesObservablyCommandManager.GetCommand(cmdExecId);
                await _echoBinariesObservablyCommandManager.RegisterForInfo(cmdExecId, responseStream, context.CancellationToken);
            }
            catch (TaskCanceledException tcex)
            {
                _logger.LogWarning($"Request for CommandExecutionUUID {cmdExecId} terminated with TaskCancellation -> {tcex}");
            }
            catch (Exception e)
            {
                _logger.LogError($"{e}");
                throw;
            }
        }

        public override Task EchoBinariesObservably_Intermediate(CommandExecutionUUID cmdExecId, IServerStreamWriter<EchoBinariesObservably_IntermediateResponses> responseStream, ServerCallContext context)
        {
            cmdExecId.ValidateIntermediateResponseParameters(_silaFeature, "EchoBinariesObservably_Intermediate");
            _echoBinariesObservablyCommandManager.GetCommand(cmdExecId);

            do
            {
                if (_intermediateResponseDataToBeSent.Count > 0)
                {
                    Binary result;
                    lock (_intermediateResponseDataToBeSent)
                    {
                        result = new Binary { Value = ByteString.CopyFrom(_intermediateResponseDataToBeSent.ToArray()) };
                        _intermediateResponseDataToBeSent.Clear();
                    }

                    responseStream.WriteAsync(new EchoBinariesObservably_IntermediateResponses
                    {
                        Binary = result
                    }).Wait();
                }
                Task.Delay(100).Wait();
            } while (!context.CancellationToken.IsCancellationRequested);

            return null;
        }

        public override Task<EchoBinariesObservably_Responses> EchoBinariesObservably_Result(CommandExecutionUUID cmdExecId, ServerCallContext context)
        {
            var command = _echoBinariesObservablyCommandManager.GetCommand(cmdExecId);
            return Task.FromResult(command.Result);
        }

        private Func<IProgress<ExecutionInfo>, EchoBinariesObservably_Parameters, CancellationToken, EchoBinariesObservably_Responses> EchoBinariesObservablyTask()
        {
            return (progress, parameters, cancellationToken) =>
            {
                var result = new List<byte>();
                foreach (var binary in parameters.Binaries)
                {
                    // check type of binary data
                    switch (binary.UnionCase)
                    {
                        case Binary.UnionOneofCase.Value:

                            result.AddRange(binary.Value);
                            lock (_intermediateResponseDataToBeSent)
                            {
                                _intermediateResponseDataToBeSent.AddRange(binary.Value);
                            }
                            break;

                        case Binary.UnionOneofCase.BinaryTransferUUID:

                            // check for available upload and copy received data
                            try
                            {
                                result.AddRange(_binaryUploadRepository.GetUploadedData(binary.BinaryTransferUUID).Result);
                                lock (_intermediateResponseDataToBeSent)
                                {
                                    _intermediateResponseDataToBeSent.AddRange(_binaryUploadRepository.GetUploadedData(binary.BinaryTransferUUID).Result);
                                }
                            }
                            catch (Exception ex)
                            {
                                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(_silaFeature.GetFullyQualifiedCommandParameterIdentifier("EchoBinaryValue", "BinaryValue"), ex.Message));
                            }

                            break;

                        default:

                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(_silaFeature.GetFullyQualifiedCommandParameterIdentifier("EchoBinaryValue", "BinaryValue"),
                                "The value must either be 'Value' or 'BinaryTransferUUID'"));
                            break;
                    }

                    Thread.Sleep(1000);
                }

                // create response
                var responseDataUUID = Guid.NewGuid();
                _binaryDownloadRepository.DownloadDataMap.TryAdd(responseDataUUID, result.ToArray());
                return new EchoBinariesObservably_Responses { JointBinary = new Binary { BinaryTransferUUID = responseDataUUID.ToString() } };
            };
        }

        #endregion

        public override Task<EchoBinaryAndMetadataString_Responses> EchoBinaryAndMetadataString(EchoBinaryAndMetadataString_Parameters request, ServerCallContext context)
        {
            // get value metadata entry
            byte[] value = SilaClientMetadata.GetSilaClientMetadataValue(context.RequestHeaders, _silaFeature.GetFullyQualifiedMetadataIdentifier("String"));

            var metadataString = string.Empty;

            try
            {
                // extract String object
                var metadata = Metadata_String.Parser.ParseFrom(value);
                if (metadata?.String == null)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, "String metadata value could not be parsed (wrong message type)"));
                }

                metadataString = metadata?.String.Value;
            }
            catch (Exception e)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, ErrorHandling.HandleException(e)));
            }

            // check parameters
            request.ValidateCommandParameters(_silaFeature, "EchoBinaryAndMetadataString");

            var response = new EchoBinaryAndMetadataString_Responses { StringMetadata = new Sila2.Org.Silastandard.String { Value = metadataString } };

            // check type of binary data
            switch (request.Binary.UnionCase)
            {
                case Binary.UnionOneofCase.Value:
                    response.Binary = new Binary { Value = request.Binary.Value };
                    break;

                case Binary.UnionOneofCase.BinaryTransferUUID:

                    // check for available upload and copy received data and provide it as response
                    var responseDataUUID = Guid.NewGuid();
                    try
                    {
                        _binaryDownloadRepository.DownloadDataMap.TryAdd(responseDataUUID, _binaryUploadRepository.GetUploadedData(request.Binary.BinaryTransferUUID).Result);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(_silaFeature.GetFullyQualifiedCommandParameterIdentifier("EchoBinaryAndMetadataString", "Binary"), ex.Message));
                    }

                    // create response
                    response.Binary = new Binary { BinaryTransferUUID = responseDataUUID.ToString() };
                    break;

                default:
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(_silaFeature.GetFullyQualifiedCommandParameterIdentifier("EchoBinaryAndMetadataString", "Binary"),
                        "The value must either be 'Value' or 'BinaryTransferUUID'"));
                    break;
            }

            return Task.FromResult(response);
        }

        public override Task<Get_FCPAffectedByMetadata_String_Responses> Get_FCPAffectedByMetadata_String(Get_FCPAffectedByMetadata_String_Parameters request, ServerCallContext context)
        {
            Get_FCPAffectedByMetadata_String_Responses response = new();
            response.AffectedCalls.Add(new Sila2.Org.Silastandard.String { Value = _silaFeature.GetFullyQualifiedCommandIdentifier("EchoBinaryAndMetadataString") });

            return Task.FromResult(response);
        }

        #endregion
    }
}
