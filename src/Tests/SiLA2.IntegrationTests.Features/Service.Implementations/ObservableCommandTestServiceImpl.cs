﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Test.Observablecommandtest.V1;
using SiLA2.Commands;
using SiLA2.Server;
using SiLA2.Server.Services;

namespace SiLA2.IntegrationTests.Features.Service.Implementations
{
    public class ObservableCommandTestServiceImpl : ObservableCommandTest.ObservableCommandTestBase
    {
        private readonly IObservableCommandManager<Count_Parameters, Count_Responses> _observableCountCommandManager;
        private readonly IObservableCommandManager<EchoValueAfterDelay_Parameters, EchoValueAfterDelay_Responses> _observableCommandManagerEchoValueAfterDelay;
        private readonly ILogger<ObservableCommandTestServiceImpl> _logger;
        private readonly Feature _silaFeature;

        public ObservableCommandTestServiceImpl(ISiLA2Server siLA2Server, IObservableCommandManager<Count_Parameters, Count_Responses> observableCommandManagerCmdCofirmation, IObservableCommandManager<EchoValueAfterDelay_Parameters, EchoValueAfterDelay_Responses> observableCommandManagerEchoValueAfterDelay, ILogger<ObservableCommandTestServiceImpl> logger)
        {
            _observableCountCommandManager = observableCommandManagerCmdCofirmation;
            _observableCommandManagerEchoValueAfterDelay = observableCommandManagerEchoValueAfterDelay;
            _logger = logger;
            _silaFeature = siLA2Server.ReadFeature(Path.Combine("Features", "ObservableCommandTest-v1_0.sila.xml"));
        }

        public override async Task<CommandConfirmation> Count(Count_Parameters request, ServerCallContext context)
        {
            request.ValidateCommandParameters(_silaFeature, "Count");
            var command = await _observableCountCommandManager.AddCommand(request, (progress, request, token) =>
            {
                var counter = 0;
                Task.Run(async () =>
                {
                    for (; counter < request.N.Value; counter++)
                    {
                        var execInfo = new ExecutionInfo { ProgressInfo = new Real { Value = counter == 0 ? 0 : (request.N.Value - 1) / counter }, CommandStatus = ExecutionInfo.Types.CommandStatus.Running };

                        if (counter == request.N.Value - 1)
                        {
                            execInfo.EstimatedRemainingTime = new Duration
                            {
                                Seconds = 0,
                                Nanos = 0
                            };
                            execInfo.CommandStatus = ExecutionInfo.Types.CommandStatus.FinishedSuccessfully;
                        }

                        await Task.Delay((int)(request.Delay.Value * 1000));
                        progress.Report(execInfo);
                    }
                }).Wait();

                return new Count_Responses { IterationResponse = new Integer { Value = counter - 1 } };

            });
            return command.Confirmation;
        }

        public override async Task Count_Info(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _observableCountCommandManager.RegisterForInfo(cmdExecId, responseStream, context.CancellationToken);
            }
            catch (TaskCanceledException tcex)
            {
                _logger.LogWarning($"Request for CommandExecutionUUID {cmdExecId} terminated with TaskCancellation -> {tcex}");
            }
        }

        public override Task Count_Intermediate(CommandExecutionUUID cmdExecId, IServerStreamWriter<Count_IntermediateResponses> responseStream, ServerCallContext context)
        {
            var command = _observableCountCommandManager.GetCommand(cmdExecId);
            var cts = new CancellationTokenSource();
            return command.ProcessIntermediateResponses(responseStream, ProcessResponses(_silaFeature, responseStream), cts.Token);
        }

        public override Task<Count_Responses> Count_Result(CommandExecutionUUID cmdExecId, ServerCallContext context)
        {
            var command = _observableCountCommandManager.GetCommand(cmdExecId);
            return Task.FromResult(command.Result);
        }

        public override async Task<CommandConfirmation> EchoValueAfterDelay(EchoValueAfterDelay_Parameters request, ServerCallContext context)
        {
            request.ValidateCommandParameters(_silaFeature, "EchoValueAfterDelay");
            var command = await _observableCommandManagerEchoValueAfterDelay.AddCommand(request, (progress, request, token) =>
            {
                // command is finished right after is has been started delayed
                return new EchoValueAfterDelay_Responses { ReceivedValue = new Integer { Value = request.Value.Value } };
            }, TimeSpan.FromSeconds(request.Delay.Value));
            return command.Confirmation;
        }

        public override async Task EchoValueAfterDelay_Info(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _observableCommandManagerEchoValueAfterDelay.RegisterForInfo(cmdExecId, responseStream, context.CancellationToken);
            }
            catch (TaskCanceledException tcex)
            {
                _logger.LogWarning($"Request for CommandExecutionUUID {cmdExecId} terminated with TaskCancellation -> {tcex}");
            }
        }

        public override Task<EchoValueAfterDelay_Responses> EchoValueAfterDelay_Result(CommandExecutionUUID cmdExecId, ServerCallContext context)
        {
            var command = _observableCommandManagerEchoValueAfterDelay.GetCommand(cmdExecId);
            return Task.FromResult(command.Result);
        }

        private Action<Count_Parameters, CancellationToken> ProcessResponses(Feature feature, IServerStreamWriter<Count_IntermediateResponses> responseStream)
        {
            return (parameter, token) =>
            {
                parameter.ValidateCommandParameters(feature, "Count");
                var count = parameter.N.Value;
                var delay = parameter.Delay.Value * 1000;
                for (int i = 0; i < count; i++)
                {
                    responseStream.WriteAsync(new Count_IntermediateResponses { CurrentIteration = new Integer { Value = i } }, token).Wait();
                    Task.Delay((int)delay).Wait();
                }
            };
        }
    }
}
