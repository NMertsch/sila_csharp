﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard.Core.Lockcontroller.V2;
using Sila2.Org.Silastandard.Test.Lockablecommandprovider.V1;
using SiLA2.Network;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class LockableCommandProviderServiceImpl : LockableCommandProvider.LockableCommandProviderBase
    {
        #region Private members

        private readonly LockControllerService _lockController;
        private readonly IGrpcChannelProvider _grpcChannelProvider;
        private readonly IServerConfig _serverConfig;
        private readonly ILogger<LockableCommandProviderServiceImpl> _logger;

        #endregion

        #region Constructors and destructors

        public LockableCommandProviderServiceImpl(ISiLA2Server silaServer, LockControllerService lockControllerService, IGrpcChannelProvider grpcServerChannelProvider, IServerConfig serverConfig, ILogger<LockableCommandProviderServiceImpl> logger)
        {
            var lockableCommandProviderFeature = silaServer.ReadFeature(Path.Combine("Features", "LockableCommandProvider-v1_0.sila.xml"));

            _lockController = lockControllerService;
            _grpcChannelProvider = grpcServerChannelProvider;
            _serverConfig = serverConfig;

            // specify RequiresLock command as lockable
            var requiresLockCmd = lockableCommandProviderFeature.GetDefinedCommands().Find(match => match.Identifier == "RequiresLock");
            if (requiresLockCmd != null)
            {
                _lockController.LockableItems = new List<string> { lockableCommandProviderFeature.GetFullyQualifiedCommandIdentifier(requiresLockCmd.Identifier) };
            }

            _logger = logger;
        }

        #endregion

        #region Overrides of LockableCommandProviderBase

        public override async Task<RequiresLock_Responses> RequiresLock(RequiresLock_Parameters request, ServerCallContext context)
        {
            var cts = new CancellationTokenSource();

            var client = new LockController.LockControllerClient(await _grpcChannelProvider.GetChannel(_serverConfig.FQHN, _serverConfig.Port, true));

            var response = client.Subscribe_IsLocked(new Subscribe_IsLocked_Parameters(), Metadata.Empty, null, cts.Token);
            bool isLocked = false;
            
            if (response.ResponseStream.MoveNext().Result)
            {
                isLocked = response.ResponseStream.Current.IsLocked.Value;
            }
            cts.Cancel();

            _logger.LogInformation($"RequiresLock called while server was {(isLocked ? string.Empty : "not ")}locked");

            // check if server is locked and (if so) if the lock identifier is valid
            _lockController.CheckLock(context.RequestHeaders);

            return new RequiresLock_Responses();
        }

        #endregion
    }
}
