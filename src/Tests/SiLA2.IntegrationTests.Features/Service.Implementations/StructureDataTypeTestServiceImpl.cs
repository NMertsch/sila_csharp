﻿using System.Globalization;
using System.Text;
using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard.Test.Structuredatatypetest.V1;
using SiLA2.Domain;
using SiLA2.Server;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.IntegrationTests.Features.ServiceImplementations
{
    public class StructureDataTypeTestServiceImpl : StructureDataTypeTest.StructureDataTypeTestBase
    {
        #region Private members

        private readonly ILogger<StructureDataTypeTestServiceImpl> _logger;
        private readonly Feature _silaFeature;

        #endregion

        #region Constructors and destructors

        public StructureDataTypeTestServiceImpl(ISiLA2Server siLA2Server, ILogger<StructureDataTypeTestServiceImpl> logger)
        {
            _logger = logger;
            _silaFeature = siLA2Server.ReadFeature(Path.Combine("Features", "StructureDataTypeTest-v1_0.sila.xml"));
        }

        #endregion

        #region Overrides of StructureDataTypeTestBase

        public override Task<EchoStructureValue_Responses> EchoStructureValue(EchoStructureValue_Parameters request, ServerCallContext context)
        {
            // assemble log message
            var sb = new StringBuilder("Received structure element values:");

            sb.Append($"\n\tString type value: '{request.StructureValue.TestStructure.StringTypeValue.Value}'");
            sb.Append($"\n\tInteger type value: {request.StructureValue.TestStructure.IntegerTypeValue.Value}");
            sb.Append($"\n\tReal type value: {request.StructureValue.TestStructure.RealTypeValue.Value}");
            sb.Append($"\n\tBoolean type value: {request.StructureValue.TestStructure.BooleanTypeValue.Value}");
            sb.Append($"\n\tBinary type value: {request.StructureValue.TestStructure.BinaryTypeValue.Value.ToStringUtf8()}");
            var dateValue = new DateTime(
                (int)request.StructureValue.TestStructure.DateTypeValue.Year,
                (int)request.StructureValue.TestStructure.DateTypeValue.Month,
                (int)request.StructureValue.TestStructure.DateTypeValue.Day);
            sb.Append($"\n\tDate type value: {dateValue.Date}");
            var timeValue = new TimeSpan(
                (int)request.StructureValue.TestStructure.TimeTypeValue.Hour,
                (int)request.StructureValue.TestStructure.TimeTypeValue.Minute,
                (int)request.StructureValue.TestStructure.TimeTypeValue.Second);
            sb.Append($"\n\tTime type value: {timeValue}");
            var timestampValue = new DateTime(
                (int)request.StructureValue.TestStructure.TimestampTypeValue.Year,
                (int)request.StructureValue.TestStructure.TimestampTypeValue.Month,
                (int)request.StructureValue.TestStructure.TimestampTypeValue.Day,
                (int)request.StructureValue.TestStructure.TimestampTypeValue.Hour,
                (int)request.StructureValue.TestStructure.TimestampTypeValue.Minute,
                (int)request.StructureValue.TestStructure.TimestampTypeValue.Second);
            sb.Append($"\n\tTimestamp type value: {timestampValue}");

            var dataType = AnyType.ExtractAnyType(request.StructureValue.TestStructure.AnyTypeValue.Type);
            if (dataType.Item is BasicType)
            {
                string payload = string.Empty;
                switch ((BasicType)dataType.Item)
                {
                    case BasicType.String:
                        payload = SiLAFramework.String.Parser.ParseFrom(request.StructureValue.TestStructure.AnyTypeValue.Payload).Value;
                        break;
                    case BasicType.Integer:
                        payload = SiLAFramework.Integer.Parser.ParseFrom(request.StructureValue.TestStructure.AnyTypeValue.Payload).Value.ToString();
                        break;
                    case BasicType.Real:
                        payload = SiLAFramework.Real.Parser.ParseFrom(request.StructureValue.TestStructure.AnyTypeValue.Payload).Value.ToString(CultureInfo.InvariantCulture);
                        break;
                    case BasicType.Boolean:
                        payload = SiLAFramework.Boolean.Parser.ParseFrom(request.StructureValue.TestStructure.AnyTypeValue.Payload).Value.ToString();
                        break;
                    case BasicType.Binary:
                        var binary = SiLAFramework.Binary.Parser.ParseFrom(request.StructureValue.TestStructure.AnyTypeValue.Payload);
                        if (binary.UnionCase == SiLAFramework.Binary.UnionOneofCase.Value)
                        {
                            payload = SiLAFramework.Boolean.Parser.ParseFrom(request.StructureValue.TestStructure.AnyTypeValue.Payload).Value.ToString();
                            break;
                        }

                        _logger.LogError($"Response contains the wrong union type '{binary.UnionCase}' (expected: '{SiLAFramework.Binary.UnionOneofCase.Value}')");
                        break;
                    case BasicType.Date:
                        payload = SiLAFramework.Date.Parser.ParseFrom(request.StructureValue.TestStructure.AnyTypeValue.Payload).ToString();
                        break;
                    case BasicType.Time:
                        payload = SiLAFramework.Time.Parser.ParseFrom(request.StructureValue.TestStructure.AnyTypeValue.Payload).ToString();
                        break;
                    case BasicType.Timestamp:
                        payload = SiLAFramework.Timestamp.Parser.ParseFrom(request.StructureValue.TestStructure.AnyTypeValue.Payload).ToString();
                        break;
                    default:
                        throw new Exception($"'{dataType.Item}' is not a valid AnyType type");
                }

                sb.Append($"\n\tAny type value: {(BasicType)dataType.Item} type {payload}");
            }

            _logger.LogInformation(sb.ToString());

            return Task.FromResult(new EchoStructureValue_Responses { ReceivedValues = request.StructureValue });
        }

        public override Task<Get_StructureValue_Responses> Get_StructureValue(Get_StructureValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_StructureValue_Responses
            {
                StructureValue = new DataType_TestStructure
                {
                    TestStructure = new DataType_TestStructure.Types.TestStructure_Struct
                    {
                        StringTypeValue = new SiLAFramework.String { Value = "SiLA2_Test_String_Value" },
                        IntegerTypeValue = new SiLAFramework.Integer { Value = 5124 },
                        RealTypeValue = new SiLAFramework.Real { Value = 3.1415926 },
                        BooleanTypeValue = new SiLAFramework.Boolean { Value = true },
                        BinaryTypeValue = new SiLAFramework.Binary { Value = ByteString.CopyFromUtf8("SiLA2_Binary_String_Value") },
                        DateTypeValue = new SiLAFramework.Date { Year = 2022, Month = 8, Day = 5 },
                        TimeTypeValue = new SiLAFramework.Time { Hour = 12, Minute = 34, Second = 56 },
                        TimestampTypeValue = new SiLAFramework.Timestamp { Year = 2022, Month = 8, Day = 5, Hour = 12, Minute = 34, Second = 56 },
                        AnyTypeValue = AnyType.CreateAnyTypeObject("SiLA2_Any_Type_String_Value")
                    }
                }
            });
        }

        public override Task<EchoDeepStructureValue_Responses> EchoDeepStructureValue(EchoDeepStructureValue_Parameters request, ServerCallContext context)
        {
            // assemble log message
            var sb = new StringBuilder("Received outer structure element values:");

            sb.Append($"\n\tOuter String type value: '{request.DeepStructureValue.DeepStructure.OuterStringTypeValue.Value}'");
            sb.Append($"\n\tOuter Integer type value: {request.DeepStructureValue.DeepStructure.OuterIntegerTypeValue.Value}");
            sb.Append("\n\tMiddle structure element values:");
            sb.Append($"\n\t\tMiddle String type value: '{request.DeepStructureValue.DeepStructure.MiddleStructure.MiddleStringTypeValue.Value}'");
            sb.Append($"\n\t\tMiddle Integer type value: {request.DeepStructureValue.DeepStructure.MiddleStructure.MiddleIntegerTypeValue.Value}");
            sb.Append("\n\t\tInner structure element values:");
            sb.Append($"\n\t\t\tInner String type value: '{request.DeepStructureValue.DeepStructure.MiddleStructure.InnerStructure.InnerStringTypeValue.Value}'");
            sb.Append($"\n\t\t\tInner Integer type value: {request.DeepStructureValue.DeepStructure.MiddleStructure.InnerStructure.InnerIntegerTypeValue.Value}");

            _logger.LogInformation(sb.ToString());

            return Task.FromResult(new EchoDeepStructureValue_Responses { ReceivedValues = request.DeepStructureValue });
        }

        public override Task<Get_DeepStructureValue_Responses> Get_DeepStructureValue(Get_DeepStructureValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_DeepStructureValue_Responses
            {
                DeepStructureValue = new DataType_DeepStructure
                {
                    DeepStructure = new DataType_DeepStructure.Types.DeepStructure_Struct
                    {
                        OuterStringTypeValue = new SiLAFramework.String { Value = "Outer_Test_String" },
                        OuterIntegerTypeValue = new SiLAFramework.Integer { Value = 1111 },
                        MiddleStructure = new DataType_DeepStructure.Types.DeepStructure_Struct.Types.MiddleStructure_Struct
                        {
                            MiddleStringTypeValue = new SiLAFramework.String { Value = "Middle_Test_String" },
                            MiddleIntegerTypeValue = new SiLAFramework.Integer { Value = 2222 },
                            InnerStructure = new DataType_DeepStructure.Types.DeepStructure_Struct.Types.MiddleStructure_Struct.Types.InnerStructure_Struct
                            {
                                InnerStringTypeValue = new SiLAFramework.String { Value = "Inner_Test_String" },
                                InnerIntegerTypeValue = new SiLAFramework.Integer { Value = 3333 }
                            }
                        }
                    }
                }
            });
        }

        #endregion
    }
}
