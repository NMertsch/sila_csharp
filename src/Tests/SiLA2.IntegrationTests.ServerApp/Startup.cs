﻿using SiLA2.AspNetCore;
using SiLA2.Commands;
using SiLA2.IntegrationTests.Features.Service.Implementations;
using SiLA2.IntegrationTests.Features.ServiceImplementations;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Utils.Config;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using System.Reflection;

namespace SiLA2.IntegrationTests.ServerApp
{
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc(options =>
            {
                options.EnableDetailedErrors = true;
                options.Interceptors.Add<Server.Interceptors.LoggingInterceptor>();
                options.Interceptors.Add<Server.Interceptors.MetadataValidationInterceptor>();
                options.Interceptors.Add<Server.Interceptors.ParameterValidationInterceptor>();
            });
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<MetadataManager>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.AddSingleton<ICertificateProvider, CertificateProvider>();
            services.AddSingleton<CertificateContext>();
            services.AddSingleton<ICertificateRepository, CertificateRepository>();
            services.AddSingleton<IBinaryDownloadRepository, BinaryDownloadRepository>();
            services.AddSingleton<IBinaryUploadRepository, BinaryUploadRepository>();
            services.AddSingleton<LockControllerService>();
            services.AddSingleton<AnyTypeTestServiceImpl>();
            services.AddSingleton<BasicDataTypeTestServiceImpl>();
            services.AddSingleton<BinaryTransferTestServiceImpl>();
            services.AddSingleton<ErrorHandlingTestServiceImpl>();
            services.AddSingleton<ListDataTypeTestServiceImpl>();
            services.AddSingleton<LockableCommandProviderServiceImpl>();
            services.AddSingleton<MetadataConsumerTestServiceImpl>();
            services.AddSingleton<MetadataProviderServiceImpl>();
            services.AddSingleton<ParameterConstraintsTestServiceImpl>();
            services.AddSingleton<StructureDataTypeTestServiceImpl>();
            services.AddSingleton<UnobservableCommandTestServiceImpl>();
            services.AddSingleton<UnobservablePropertyTestServiceImpl>();
            services.AddSingleton<ObservableCommandTestServiceImpl>();
            services.AddSingleton<ObservablePropertyTestServiceImpl>();
            services.AddSingleton<IServerConfig>(new ServerConfig(_configuration["ServerConfig:Name"],
                                                                  Guid.Parse(_configuration["ServerConfig:UUID"]),
                                                                  _configuration["ServerConfig:FQHN"],
                                                                  int.Parse(_configuration["ServerConfig:Port"]),
                                                                  _configuration["ServerConfig:NetworkInterface"],
                                                                  _configuration["ServerConfig:DiscoveryServiceName"]));
#if DEBUG
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
            services.ConfigureWritable<ServerConfig>(_configuration.GetSection("ServerConfig"), configFile);
        }

        //Note: Services _<x> are injected for calling its constructor in which the SilaFeature is added to the SiLA2.Server feature collection
        public void Configure(IApplicationBuilder app,
                    IWebHostEnvironment env,
                    ISiLA2Server siLA2Server,
                    ServerInformation serverInformation,
                    AnyTypeTestServiceImpl _01,
                    BasicDataTypeTestServiceImpl _02,
                    IBinaryDownloadRepository _03,
                    IBinaryUploadRepository _04,
                    BinaryTransferTestServiceImpl _05,
                    ErrorHandlingTestServiceImpl _06,
                    ListDataTypeTestServiceImpl _07,
                    LockableCommandProviderServiceImpl _08,
                    MetadataConsumerTestServiceImpl _09,
                    MetadataProviderServiceImpl _10,
                    ParameterConstraintsTestServiceImpl _11,
                    StructureDataTypeTestServiceImpl _12,
                    UnobservableCommandTestServiceImpl _13,
                    UnobservablePropertyTestServiceImpl _14,
                    ObservableCommandTestServiceImpl _15,
                    ObservablePropertyTestServiceImpl _16,
                    ILogger<Startup> logger)
        {
            siLA2Server.ReadFeature(Path.Combine("Features", "ErrorRecoveryService-v1_0.sila.xml"));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<SiLAService>();
                endpoints.MapGrpcService<SiLABinaryUploadService>();
                endpoints.MapGrpcService<SiLABinaryDownloadService>();
                endpoints.MapGrpcService<LockControllerService>();
                endpoints.MapGrpcService<AnyTypeTestServiceImpl>();
                endpoints.MapGrpcService<BasicDataTypeTestServiceImpl>();
                endpoints.MapGrpcService<BinaryTransferTestServiceImpl>();
                endpoints.MapGrpcService<BinaryDownloadRepository>();
                endpoints.MapGrpcService<BinaryUploadRepository>();
                endpoints.MapGrpcService<ErrorHandlingTestServiceImpl>();
                endpoints.MapGrpcService<ListDataTypeTestServiceImpl>();
                endpoints.MapGrpcService<LockableCommandProviderServiceImpl>();
                endpoints.MapGrpcService<MetadataConsumerTestServiceImpl>();
                endpoints.MapGrpcService<MetadataProviderServiceImpl>();
                endpoints.MapGrpcService<ParameterConstraintsTestServiceImpl>();
                endpoints.MapGrpcService<StructureDataTypeTestServiceImpl>();
                endpoints.MapGrpcService<UnobservableCommandTestServiceImpl>();
                endpoints.MapGrpcService<UnobservablePropertyTestServiceImpl>();
                endpoints.MapGrpcService<ObservableCommandTestServiceImpl>();
                endpoints.MapGrpcService<ObservablePropertyTestServiceImpl>();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });

            logger.LogInformation($"{serverInformation}");
            logger.LogInformation("Starting Server Announcement...");
            siLA2Server.Start();
            logger.LogInformation($"{DateTime.Now} : Started {GetType().Assembly.FullName}");
        }
    }
}
