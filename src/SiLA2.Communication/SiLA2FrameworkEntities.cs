﻿using ProtoBuf;
using System.ComponentModel;

namespace Sila2.Org.Silastandard

{
    [ProtoContract()]
    public class String
    {
        [ProtoMember(1, Name = @"value")]
        [DefaultValue("")]
        public string Value { get; set; } = "";
    }

    [ProtoContract()]
    public class Integer
    {
        [ProtoMember(1, Name = @"value")]
        public long Value { get; set; }
    }

    [ProtoContract()]
    public class Real
    {
        [ProtoMember(1, Name = @"value")]
        public double Value { get; set; }
    }

    [ProtoContract()]
    public class Boolean
    {
        [ProtoMember(1, Name = @"value")]
        public bool Value { get; set; }
    }

    [ProtoContract()]
    public class Binary
    {
        [ProtoMember(1, Name = @"value")]
        public byte[] Value
        {
            get => __pbn__union.Is(1) ? (byte[])__pbn__union.Object : default;
            set => __pbn__union = new DiscriminatedUnionObject(1, value);
        }
        public bool ShouldSerializeValue() => __pbn__union.Is(1);
        public void ResetValue() => DiscriminatedUnionObject.Reset(ref __pbn__union, 1);

        private DiscriminatedUnionObject __pbn__union;

        [ProtoMember(2)]
        [DefaultValue("")]
        public string binaryTransferUUID
        {
            get => __pbn__union.Is(2) ? (string)__pbn__union.Object : "";
            set => __pbn__union = new DiscriminatedUnionObject(2, value);
        }
        public bool ShouldSerializebinaryTransferUUID() => __pbn__union.Is(2);
        public void ResetbinaryTransferUUID() => DiscriminatedUnionObject.Reset(ref __pbn__union, 2);

    }

    [ProtoContract()]
    public class Date
    {
        [ProtoMember(1, Name = @"day")]
        public uint Day { get; set; }

        [ProtoMember(2, Name = @"month")]
        public uint Month { get; set; }

        [ProtoMember(3, Name = @"year")]
        public uint Year { get; set; }

        [ProtoMember(4, Name = @"timezone")]
        public Timezone Timezone { get; set; }
    }

    [ProtoContract()]
    public class Time
    {
        [ProtoMember(1, Name = @"second")]
        public uint Second { get; set; }

        [ProtoMember(2, Name = @"minute")]
        public uint Minute { get; set; }

        [ProtoMember(3, Name = @"hour")]
        public uint Hour { get; set; }

        [ProtoMember(4, Name = @"timezone")]
        public Timezone Timezone { get; set; }
    }

    [ProtoContract()]
    public class Timestamp
    {
        [ProtoMember(1, Name = @"second")]
        public uint Second { get; set; }

        [ProtoMember(2, Name = @"minute")]
        public uint Minute { get; set; }

        [ProtoMember(3, Name = @"hour")]
        public uint Hour { get; set; }

        [ProtoMember(4, Name = @"day")]
        public uint Day { get; set; }

        [ProtoMember(5, Name = @"month")]
        public uint Month { get; set; }

        [ProtoMember(6, Name = @"year")]
        public uint Year { get; set; }

        [ProtoMember(7, Name = @"timezone")]
        public Timezone Timezone { get; set; }
    }

    [ProtoContract()]
    public class Timezone
    {
        [ProtoMember(1, Name = @"hours")]
        public int Hours { get; set; }

        [ProtoMember(2, Name = @"minutes")]
        public uint Minutes { get; set; }
    }

    [ProtoContract()]
    public class Any
    {
        [ProtoMember(1, Name = @"type")]
        [DefaultValue("")]
        public string Type { get; set; } = "";

        [ProtoMember(2, Name = @"payload")]
        public byte[] Payload { get; set; }
    }

    [ProtoContract()]
    public class Duration
    {
        [ProtoMember(1, Name = @"seconds")]
        public long Seconds { get; set; }

        [ProtoMember(2, Name = @"nanos")]
        public int Nanos { get; set; }
    }

    [ProtoContract()]
    public class CommandExecutionUUID
    {
        [ProtoMember(1, Name = @"value")]
        [DefaultValue("")]
        public string Value { get; set; } = "";
    }

    [ProtoContract()]
    public class CommandConfirmation
    {
        [ProtoMember(1)]
        public CommandExecutionUUID commandExecutionUUID { get; set; }

        [ProtoMember(2)]
        public Duration lifetimeOfExecution { get; set; }
    }

    [ProtoContract()]
    public class ExecutionInfo
    {
        [ProtoMember(1)]
        public CommandStatus commandStatus { get; set; }

        [ProtoMember(2)]
        public Real progressInfo { get; set; }

        [ProtoMember(3)]
        public Duration estimatedRemainingTime { get; set; }

        [ProtoMember(4)]
        public Duration updatedLifetimeOfExecution { get; set; }

        [ProtoContract()]
        public enum CommandStatus
        {
            [ProtoEnum(Name = @"waiting")]
            Waiting = 0,
            [ProtoEnum(Name = @"running")]
            Running = 1,
            [ProtoEnum(Name = @"finishedSuccessfully")]
            finishedSuccessfully = 2,
            [ProtoEnum(Name = @"finishedWithError")]
            finishedWithError = 3,
        }
    }

    [ProtoContract()]
    public class SiLAError
    {
        [ProtoMember(1)]
        public ValidationError validationError
        {
            get => __pbn__error.Is(1) ? (ValidationError)__pbn__error.Object : default;
            set => __pbn__error = new DiscriminatedUnionObject(1, value);
        }
        public bool ShouldSerializevalidationError() => __pbn__error.Is(1);
        public void ResetvalidationError() => DiscriminatedUnionObject.Reset(ref __pbn__error, 1);

        private DiscriminatedUnionObject __pbn__error;

        [ProtoMember(2)]
        public DefinedExecutionError definedExecutionError
        {
            get => __pbn__error.Is(2) ? (DefinedExecutionError)__pbn__error.Object : default;
            set => __pbn__error = new DiscriminatedUnionObject(2, value);
        }
        public bool ShouldSerializedefinedExecutionError() => __pbn__error.Is(2);
        public void ResetdefinedExecutionError() => DiscriminatedUnionObject.Reset(ref __pbn__error, 2);

        [ProtoMember(3)]
        public UndefinedExecutionError undefinedExecutionError
        {
            get => __pbn__error.Is(3) ? (UndefinedExecutionError)__pbn__error.Object : default;
            set => __pbn__error = new DiscriminatedUnionObject(3, value);
        }
        public bool ShouldSerializeundefinedExecutionError() => __pbn__error.Is(3);
        public void ResetundefinedExecutionError() => DiscriminatedUnionObject.Reset(ref __pbn__error, 3);

        [ProtoMember(4)]
        public FrameworkError frameworkError
        {
            get => __pbn__error.Is(4) ? (FrameworkError)__pbn__error.Object : default;
            set => __pbn__error = new DiscriminatedUnionObject(4, value);
        }
        public bool ShouldSerializeframeworkError() => __pbn__error.Is(4);
        public void ResetframeworkError() => DiscriminatedUnionObject.Reset(ref __pbn__error, 4);
    }

    [ProtoContract()]
    public class ValidationError
    {
        [ProtoMember(1, Name = @"parameter")]
        [DefaultValue("")]
        public string Parameter { get; set; } = "";

        [ProtoMember(2, Name = @"message")]
        [DefaultValue("")]
        public string Message { get; set; } = "";
    }

    [ProtoContract()]
    public class DefinedExecutionError
    {
        [ProtoMember(1)]
        [DefaultValue("")]
        public string errorIdentifier { get; set; } = "";

        [ProtoMember(2, Name = @"message")]
        [DefaultValue("")]
        public string Message { get; set; } = "";
    }

    [ProtoContract()]
    public class UndefinedExecutionError
    {
        [ProtoMember(1, Name = @"message")]
        [DefaultValue("")]
        public string Message { get; set; } = "";
    }

    [ProtoContract()]
    public class FrameworkError
    {
        [ProtoMember(1)]
        public ErrorType errorType { get; set; }

        [ProtoMember(2, Name = @"message")]
        [DefaultValue("")]
        public string Message { get; set; } = "";

        [ProtoContract()]
        public enum ErrorType
        {
            [ProtoEnum(Name = @"COMMAND_EXECUTION_NOT_ACCEPTED")]
            CommandExecutionNotAccepted = 0,
            [ProtoEnum(Name = @"INVALID_COMMAND_EXECUTION_UUID")]
            InvalidCommandExecutionUuid = 1,
            [ProtoEnum(Name = @"COMMAND_EXECUTION_NOT_FINISHED")]
            CommandExecutionNotFinished = 2,
            [ProtoEnum(Name = @"INVALID_METADATA")]
            InvalidMetadata = 3,
            [ProtoEnum(Name = @"NO_METADATA_ALLOWED")]
            NoMetadataAllowed = 4,
        }
    }
}
