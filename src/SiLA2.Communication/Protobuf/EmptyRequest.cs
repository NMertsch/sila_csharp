﻿namespace SiLA2.Communication.Protobuf
{
    public sealed class EmptyRequest
    {
        private class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }
            // Fully lazy instantiation of Singleton
            internal static readonly EmptyRequest Instance = new EmptyRequest();
        }

        private static readonly byte[] Payload = new byte[0];

        public static EmptyRequest Instance { get { return Nested.Instance; } }
        public static ByteSerializer<EmptyRequest> Marshaller => new ByteSerializer<EmptyRequest>(ToByteArray, FromByteArray);

        private EmptyRequest() { }

        private static byte[] ToByteArray(EmptyRequest empty)
        {
            return Payload;
        }

        private static EmptyRequest FromByteArray(byte[] data)
        {
            return Instance;
        }
    }
}
