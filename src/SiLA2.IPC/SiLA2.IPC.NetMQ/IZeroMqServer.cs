﻿using NetMQ;
using System;
using System.Threading.Tasks;

namespace SiLA2.IPC.NetMQ
{
    public interface IZeroMqServer
    {
        Task Run();
        void SetRequestProssing(Func<NetMQMessage, string> requestProcessing);
    }
}