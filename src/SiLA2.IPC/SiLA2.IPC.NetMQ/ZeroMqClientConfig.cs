﻿using System;

namespace SiLA2.IPC.NetMQ
{
    public class ZeroMqClientConfig : IZeroMqClientConfig
    {
        public string ServerSocketAddress { get; }
        public TimeSpan Timeout { get; }
        public bool UseSingleFrame { get; set; } = true;
        public bool ExpectServerResponse { get; set; } = true;

        public ZeroMqClientConfig(string serverSocketAddress, TimeSpan timeout)
        {
            ServerSocketAddress = serverSocketAddress;
            Timeout = timeout;
        }
    }
}
