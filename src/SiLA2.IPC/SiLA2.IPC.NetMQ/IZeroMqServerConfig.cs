﻿using NetMQ;
using System;

namespace SiLA2.IPC.NetMQ
{
    public interface IZeroMqServerConfig
    {
        string ServerSocketAddress { get; }
        public Func<NetMQMessage, string> ProcessRequest { get; set; }
    }
}