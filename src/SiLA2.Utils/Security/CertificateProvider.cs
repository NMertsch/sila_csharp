﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SiLA2.Utils.Extensions;
using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace SiLA2.Utils.Security
{
    /// <summary>
    /// Denotes a helper class used to generate certificates on the fly
    /// </summary>
    public class CertificateProvider : ICertificateProvider
    {
        private readonly ICertificateRepository _certificateRepository;
        private readonly IConfiguration _configuration;
        private readonly ILogger<ICertificateProvider> _logger;

        public CertificateProvider(ICertificateRepository certificateRepository, IConfiguration configuration, ILogger<ICertificateProvider> logger)
        {
            _certificateRepository = certificateRepository;
            _configuration = configuration;
            _logger = logger;
        }

        /// <summary>
        /// Creates a new server certificate
        /// </summary>
        /// <param name="baseCertificate">The base CA certificate (must include private key) or null, if a self-signed certificate should be used</param>
        /// <param name="key">The key used for the certificate</param>
        /// <param name="serverGuid">The UUID of the server for which to generate the certificate</param>
        /// <returns>A X509 certificate that can be used for the server</returns>
        public X509Certificate2 CreateServerCertificate(X509Certificate2 baseCertificate, RSA key, Guid? serverGuid)
        {
            var request = new CertificateRequest("CN=SiLA2", key, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);

            request.CertificateExtensions.Add(
                new X509BasicConstraintsExtension(false, false, 0, true));
            request.CertificateExtensions.Add(
                new X509KeyUsageExtension(
                    X509KeyUsageFlags.DigitalSignature | X509KeyUsageFlags.KeyEncipherment, true));

            if (serverGuid.HasValue)
            {
                // add server uuid
                request.CertificateExtensions.Add(new X509Extension("1.3.6.1.4.1.58583", Encoding.ASCII.GetBytes(serverGuid.Value.ToString()), false));
            }

            // Enhanced key usages
            request.CertificateExtensions.Add(
                new X509EnhancedKeyUsageExtension(
                    new OidCollection {
                    new Oid("1.3.6.1.5.5.7.3.2"), // TLS Client auth
                    new Oid("1.3.6.1.5.5.7.3.1")  // TLS Server auth
                    },
                    false));
            // add this subject key identifier
            request.CertificateExtensions.Add(
                new X509SubjectKeyIdentifierExtension(request.PublicKey, false));

            // cert serial is the epoch/unix timestamp
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var unixTime = Convert.ToInt64((DateTime.UtcNow - epoch).TotalSeconds);
            var serial = BitConverter.GetBytes(unixTime);

            if (baseCertificate != null)
            {
                var cert = request.Create(baseCertificate, DateTimeOffset.UtcNow.Subtract(TimeSpan.FromMinutes(1)), baseCertificate.NotAfter, serial);
                return cert.CopyWithPrivateKey(key);
            }
            else
            {
                return request.CreateSelfSigned(DateTimeOffset.UtcNow.Subtract(TimeSpan.FromMinutes(1)), DateTimeOffset.UtcNow.AddYears(100));
            }
        }

        /// <summary>
        /// Generates a new root certificate
        /// </summary>
        /// <param name="key">The key for which to generate the root certificate</param>
        /// <returns>A certificate that can be used as CA</returns>
        public X509Certificate2 CreateRootCertificate(RSA key)
        {
            var request = new CertificateRequest("CN=Root", key, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            request.CertificateExtensions.Add(
                new X509BasicConstraintsExtension(true, false, 0, true));
            request.CertificateExtensions.Add(
                new X509KeyUsageExtension(
                    X509KeyUsageFlags.KeyCertSign,
                    true));
            return request.CreateSelfSigned(DateTime.Now.Subtract(TimeSpan.FromMinutes(1)), DateTime.Now.AddYears(100));
        }

        public X509Certificate2 GetServerCertificate(bool safeIfNotExists)
        {
            X509Certificate2 x509Certificate2;
            X509Certificate2 x509Certificate2Root;

            if (_certificateRepository.HasCertificate)
            {
                return _certificateRepository.ServerCertificate;
            }
            else
            {
                var rootKey = RSA.Create();

                if (!_certificateRepository.HasCertificateAuthority)
                {
                    x509Certificate2Root = CreateRootCertificate(rootKey);
                    _certificateRepository.SaveCertificateAuthority(x509Certificate2Root, rootKey).Wait();
                }

                x509Certificate2 = GetServerCertificate(_certificateRepository.CertificateContext, Guid.Parse(_configuration["ServerConfig:UUID"]), out RSA key);

                if (safeIfNotExists)
                {
                    _certificateRepository.SaveCertificate(x509Certificate2, key);
                }
            }

            return x509Certificate2;
        }

        /// <summary>
        /// Gets the server certificate to use
        /// </summary>
        /// <param name="context">The certificate context</param>
        /// <param name="serverUuid">The UUID of the server or null</param>
        /// <param name="certificateAuthority">The certificate authority used</param>
        /// <returns>A certificate that the server shall use for encrypted connections</returns>
        public X509Certificate2 GetServerCertificate(CertificateContext context, Guid? serverUuid, out RSA key)
        {
            key = null;
            if (context != null)
            {
                key = RSA.Create();
                try
                {
                    var ca = context.CertificateAuthority != null ? LoadCertificateFromPem(context.CertificateAuthority) : null;
                    if (ca != null && context.CertificateAuthorityKey != null)
                    {
                        var caKey = RSA.Create();
                        caKey.ImportPkcs8PrivateKey(context.CertificateAuthorityKey.ExtractFromPem(), out int bytes);
                        ca = ca.CopyWithPrivateKey(caKey);
                    }

                    if (context.Key != null)
                    {
                        key.ImportRSAPrivateKey(context.Key.ExtractFromPem(), out _);
                    }

                    if (context.Certificate != null)
                    {
                        var certificate = LoadCertificateFromPem(context.Certificate);
                        if (certificate.NotBefore < DateTime.Now && certificate.NotAfter > DateTime.Now)
                        {
                            return certificate.CopyWithPrivateKey(key).CompensateWindowsEpheralKeysProblem(); ;
                        }
                        else
                        {
                            _logger.LogWarning("The server certificate has expired.");
                        }
                    }

                    if (ca != null && ca.HasPrivateKey && ca.NotBefore < DateTime.Now && ca.NotAfter > DateTime.Now)
                    {
                        return CreateServerCertificate(ca, key, serverUuid).CompensateWindowsEpheralKeysProblem();
                    }
                    else
                    {
                        _logger.LogWarning("No certificate authority set or certificate authority expired");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error while trying to create certificate");
                    throw;
                }

                return CreateServerCertificate(null, key, serverUuid).CompensateWindowsEpheralKeysProblem(); ;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Loads the certificate encoded in the provided PEM-encoding of the certificate into memory
        /// </summary>
        /// <param name="certificate">The PEM-encoded certificate</param>
        /// <returns>A X509Certificate2 representation of the certificate</returns>
        public X509Certificate2 LoadCertificateFromPem(string certificate)
        {
            return new X509Certificate2(certificate.ExtractFromPem());
        }
    }
}
