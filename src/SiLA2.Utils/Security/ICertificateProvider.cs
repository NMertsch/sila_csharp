﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace SiLA2.Utils.Security
{
    public interface ICertificateProvider
    {
        X509Certificate2 CreateRootCertificate(RSA key);
        X509Certificate2 CreateServerCertificate(X509Certificate2 baseCertificate, RSA key, Guid? serverGuid);
        X509Certificate2 GetServerCertificate(bool safeIfNotExists);
        X509Certificate2 GetServerCertificate(CertificateContext context, Guid? serverUuid, out RSA key);
        X509Certificate2 LoadCertificateFromPem(string certificate);
    }
}