﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace SiLA2.Utils.Security
{
    public interface ICertificateRepository
    {
        CertificateContext CertificateContext { get; }
        bool HasCertificate { get; }
        bool HasCertificateAuthority { get; }
        X509Certificate2 ServerCertificate { get; }
        bool IsServerCertificateValid();
        X509Certificate2 ServerCertificateAuthority { get; }
        byte[] ServerCertificateAuthorityKey { get; }
        void LoadCertificateItems();
        Task SaveCertificate(X509Certificate2 serverCertificate, RSA serverKey);
        Task SaveCertificateAuthority(X509Certificate2 serverCertificate, RSA serverKey);
        IDictionary<string, string> GetSiLA2FormattedCertificateAuthority();
        X509Certificate2 GetCaFromFormattedCa(string ca);
        bool DeleteCertificateFile(bool onlyServerCertItems = true);
    }
}