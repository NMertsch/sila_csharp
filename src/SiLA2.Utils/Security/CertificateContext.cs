﻿using System.IO;
using System.Reflection;

namespace SiLA2.Utils.Security
{
    /// <summary>
    /// Denotes a base class for a certificate context
    /// </summary>
    public class CertificateContext
    {
        public string CERTIFICATE_FILE_PATH => Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Constants.SERVER_CRT_FILENAME);
        public string CERTIFICATE_KEY_FILE_PATH => Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Constants.SERVER_KEY_FILENAME);
        public string CERTIFICATE_AUTHORITY_FILE_PATH => Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Constants.CA_CRT_FILENAME);
        public string CERTIFICATE_AUTHORITY_KEY_FILE_PATH => Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Constants.CA_KEY_FILENAME);

        /// <summary>
        /// Gets the PEM-encoded certificate
        /// </summary>
        public string Certificate { get; set; }

        /// <summary>
        /// Gets the PEM-encoded private key for the certificate
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets the PEM-encoded certificate authority used in this context
        /// </summary>
        public string CertificateAuthority { get; set; }

        /// <summary>
        /// Gets the PEM-encoded private key of the certificate authority used in this context
        /// </summary>
        public string CertificateAuthorityKey { get; set; }
    }
}
