﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.Network;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SiLA2.Utils.Security
{
    public class CertificateRepository : ICertificateRepository
    {
        public const string BEGIN_CERTIFICATE = "-----BEGIN CERTIFICATE-----";
        public const string END_CERTIFICATE = "-----END CERTIFICATE-----";
        private readonly IServerConfig _serverConfig;
        private readonly ILogger<ICertificateRepository> _logger;

        public CertificateContext CertificateContext { get; }
        public bool HasCertificate => !string.IsNullOrEmpty(CertificateContext.Certificate) && !string.IsNullOrEmpty(CertificateContext.Key);
        public bool HasCertificateAuthority => !string.IsNullOrEmpty(CertificateContext.CertificateAuthority) && !string.IsNullOrEmpty(CertificateContext.CertificateAuthorityKey);

        public X509Certificate2 ServerCertificate
        {
            get
            {
                if (HasCertificate)
                {
                    return X509Certificate2.CreateFromPem(CertificateContext.Certificate, CertificateContext.Key).CompensateWindowsEpheralKeysProblem();
                }
                return null;
            }
        }

        public X509Certificate2 ServerCertificateAuthority
        {
            get
            {
                return string.IsNullOrEmpty(CertificateContext.CertificateAuthorityKey) ? 
                    X509Certificate2.CreateFromPem(CertificateContext.CertificateAuthority) : X509Certificate2.CreateFromPem(CertificateContext.CertificateAuthority, CertificateContext.CertificateAuthorityKey);
            }
        }
        public byte[] ServerCertificateAuthorityKey => CertificateContext.CertificateAuthorityKey.ExtractFromPem();

        public bool IsServerCertificateValid()
        {
            try
            {
                LoadCertificateItems();
                if (!HasCertificate || string.IsNullOrEmpty(CertificateContext.CertificateAuthority))
                    return false;

                using var handler = new HttpClientHandler();
                var rootCertificates = new X509Certificate2Collection(ServerCertificateAuthority);
                handler.ServerCertificateCustomValidationCallback = CertificateValidator.CreateCustomRootValidator(rootCertificates);
                var httpClient = new HttpClient(handler);

                var result = httpClient.GetAsync($"https://{_serverConfig.FQHN}:{_serverConfig.Port}/security").Result;

                return result.StatusCode == System.Net.HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"{ex}");
                return false;
            }
        }

        public CertificateRepository(CertificateContext certificateContext, IServerConfig serverConfig, ILogger<ICertificateRepository> logger)
        {
            CertificateContext = certificateContext;
            _serverConfig = serverConfig;
            LoadCertificateItems();
            _logger = logger;
        }

        public void LoadCertificateItems()
        {
            CertificateContext.Certificate = File.Exists(CertificateContext.CERTIFICATE_FILE_PATH) ? File.ReadAllText(CertificateContext.CERTIFICATE_FILE_PATH) : null;
            CertificateContext.Key = File.Exists(CertificateContext.CERTIFICATE_KEY_FILE_PATH) ? File.ReadAllText(CertificateContext.CERTIFICATE_KEY_FILE_PATH) : null;
            CertificateContext.CertificateAuthority = File.Exists(CertificateContext.CERTIFICATE_AUTHORITY_FILE_PATH) ? File.ReadAllText(CertificateContext.CERTIFICATE_AUTHORITY_FILE_PATH) : null;
            CertificateContext.CertificateAuthorityKey = File.Exists(CertificateContext.CERTIFICATE_AUTHORITY_KEY_FILE_PATH) ? File.ReadAllText(CertificateContext.CERTIFICATE_AUTHORITY_KEY_FILE_PATH) : null;
        }

        public async Task SaveCertificate(X509Certificate2 serverCertificate, RSA serverKey)
        {
            try
            {
                CertificateContext.Certificate = new string(PemEncoding.Write("CERTIFICATE", serverCertificate.RawData));
                await File.WriteAllTextAsync(CertificateContext.CERTIFICATE_FILE_PATH, CertificateContext.Certificate);
                CertificateContext.Key = new string(PemEncoding.Write("PRIVATE KEY", serverKey.ExportPkcs8PrivateKey()));
                await File.WriteAllTextAsync(CertificateContext.CERTIFICATE_KEY_FILE_PATH, CertificateContext.Key);
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"{ex}");
            }
        }

        public async Task SaveCertificateAuthority(X509Certificate2 serverCertificate, RSA serverKey)
        {
            try
            {
                CertificateContext.CertificateAuthority = new string(PemEncoding.Write("CERTIFICATE", serverCertificate.RawData));
                await File.WriteAllTextAsync(CertificateContext.CERTIFICATE_AUTHORITY_FILE_PATH, CertificateContext.CertificateAuthority);
                CertificateContext.CertificateAuthorityKey = new string(PemEncoding.Write("PRIVATE KEY", serverKey.ExportPkcs8PrivateKey()));
                await File.WriteAllTextAsync(CertificateContext.CERTIFICATE_AUTHORITY_KEY_FILE_PATH, CertificateContext.CertificateAuthorityKey);
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"{ex}");
            }
        }

        public bool DeleteCertificateFile(bool onlyServerCertItems = true)
        {
            try
            {
                if (!onlyServerCertItems)
                {
                    DeleteInternal(CertificateContext.CERTIFICATE_AUTHORITY_FILE_PATH);
                    DeleteInternal(CertificateContext.CERTIFICATE_AUTHORITY_KEY_FILE_PATH);
                }
                DeleteInternal(CertificateContext.CERTIFICATE_FILE_PATH);
                DeleteInternal(CertificateContext.CERTIFICATE_KEY_FILE_PATH);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, null);
                return false;
            }
        }

        private void DeleteInternal(string path)
        {
            if (File.Exists(path))
                File.Delete(path);
        }

        public IDictionary<string, string> GetSiLA2FormattedCertificateAuthority()
        {
            var formattedAuthorityMap = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(CertificateContext.CertificateAuthority))
            {
                var certLines = CertificateContext.CertificateAuthority.Split("\n");

                for (int i = 0; i < certLines.Length; i++)
                {
                    formattedAuthorityMap.Add($"ca{i}", $"{certLines[i].TrimEnd('\r')}");
                }
            }

            return formattedAuthorityMap;
        }

        public X509Certificate2 GetCaFromFormattedCa(string ca)
        {
            X509Certificate2 cacert = null;
            try
            {
                var sb = new StringBuilder();
                using (var caReader = new StringReader(ca))
                {
                    while (true)
                    {
                        var line = caReader.ReadLine();
                        if (line == null)
                            break;
                        var firstEqualsSign = line.IndexOf('=');
                        if (firstEqualsSign < 0)
                            continue;
                        sb.AppendLine(line.Substring(firstEqualsSign + 1));
                    }
                }
                cacert = X509Certificate2.CreateFromPem(sb.ToString());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, null);
            }
            return cacert;
        }
    }
}
