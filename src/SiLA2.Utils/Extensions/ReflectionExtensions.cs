﻿using System.Reflection;
using System.Threading.Tasks;

namespace SiLA2.Utils.Extensions
{
    public static class ReflectionExtensions
    {
        public static bool IsAwaitableMethod(this MethodInfo methodInfo)
        {
            return methodInfo.ReturnType.GetMethod(nameof(Task.GetAwaiter)) != null;
        }
    }
}
