using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Reflection;
using CommandLine;
using SiLA2.Utils.Network;

namespace Sila2.Utils
{
    public static class CmdLineArgExtensions
    {
        public static void ParseServerCmdLineArgs<TCmdLineArgs>(this string[] args, IServerConfig serverConfig)
            where TCmdLineArgs : CmdLineServerArgs
        {
            var parser = new Parser(with => with.AutoVersion = false);
            parser.ParseArguments<TCmdLineArgs>(args)
                .WithParsed(o =>
                {
                    if (!string.IsNullOrEmpty(o.Id))
                    {
                        serverConfig.Uuid = Guid.Parse(o.Id);
                    }
                })
                 .WithParsed(o =>
                 {
                     if (!string.IsNullOrEmpty(o.Fqhn))
                     {
                         serverConfig.FQHN = o.Fqhn;
                     }
                 })
                 .WithParsed(o =>
                 {
                     if (o.Port > 0)
                     {
                         serverConfig.Port = o.Port;
                     }
                 })
                 .WithParsed(o =>
                 {
                     if (!string.IsNullOrEmpty(o.Nic))
                     {
                         serverConfig.NetworkInterface = o.Nic;
                     }
                 })
                 .WithParsed(o =>
                 {
                     if (!string.IsNullOrEmpty(o.MdnsSuffix))
                     {
                         serverConfig.NetworkInterface = o.MdnsSuffix;
                     }
                 })
                .WithParsed(o =>
                {
                    if (o.ListInterfaces)
                    {
                        DebugInfo.PrintNetworkInterfaces();
                        Environment.Exit(0);
                    }
                })
                //Removed MsBuildGitHash due to Linux Docker Build Problem (5.9.2022)
                .WithParsed(o =>
                {
                    if (o.ShowVersion)
                    {
                        DebugInfo.PrintVersion();
                        Environment.Exit(0);
                    }
                })
                .WithNotParsed(HandleError);
        }

        public static void ParseClientCmdLineArgs<TCmdLineArgs>(this string[] args, IClientConfig serverConfig)
            where TCmdLineArgs : CmdLineClientArgs
        {
            var parser = new Parser(with => with.AutoVersion = false);
            parser.ParseArguments<TCmdLineArgs>(args)
                 .WithParsed(o =>
                 {
                     if (!string.IsNullOrEmpty(o.Fqhn))
                     {
                         serverConfig.IpOrCdirOrFullyQualifiedHostName = o.Fqhn;
                     }
                 })
                 .WithParsed(o =>
                 {
                     if (o.Port > 0)
                     {
                         serverConfig.Port = o.Port;
                     }
                 })
                 .WithParsed(o =>
                 {
                     if (!string.IsNullOrEmpty(o.Nic))
                     {
                         serverConfig.NetworkInterface = o.Nic;
                     }
                 })
                 .WithParsed(o =>
                 {
                     if (!string.IsNullOrEmpty(o.MdnsSuffix))
                     {
                         serverConfig.NetworkInterface = o.MdnsSuffix;
                     }
                 })
                .WithNotParsed(HandleError);
        }

        private static void HandleError(IEnumerable<Error> errs)
        {
            Environment.Exit(1);
        }
    }

    [Verb("debug-info", HelpText = "Lists debug information such as list of available network interfaces")]
    public class DebugInfo
    {
        /// <summary>
        /// Prints the list of network interfaces available on the system
        /// </summary>
        public static void PrintNetworkInterfaces()
        {
            var interfaces = NetworkInterface.GetAllNetworkInterfaces();
            Console.WriteLine($"Available Network Interfaces:{Environment.NewLine}");
            foreach (var item in interfaces)
            {
                Console.WriteLine($"\t{item.Name}");
                Console.WriteLine($"\t{item.Id}");
                Console.WriteLine($"\t{item.NetworkInterfaceType}");
                Console.WriteLine($"\t{item.GetPhysicalAddress()}");
                Console.WriteLine($"\tStatus: {item.OperationalStatus}");
                foreach (var ip in item.GetIPProperties().UnicastAddresses)
                {
                    Console.WriteLine($"\t\t{ip.Address}");
                }

                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            }
        }

        /// <summary>
        /// Gets the git hash value from the assembly or null if it cannot be found. 
        /// </summary>
        /// <returns></returns>
        public static void PrintVersion()
        {
            Console.WriteLine();
            var asm = Assembly.Load("SiLA2");
            var attrs = asm.GetCustomAttributes<AssemblyMetadataAttribute>();
            Console.WriteLine($"Based on assembly {asm}");
            Console.WriteLine();
        }
    }
}