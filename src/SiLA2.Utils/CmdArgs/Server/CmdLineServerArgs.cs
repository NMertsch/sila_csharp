﻿using System.Runtime.Serialization;
using CommandLine;

namespace Sila2.Utils
{
    public class CmdLineServerArgs
    {
        [DataMember]
        [Option('g', "guid", HelpText = "ID (must be Guid)")]
        public string Id { get; set; }

        [DataMember]
        [Option('n', "fqhn", HelpText = "IP or CDIR or Fully Qualified Host Name")]
        public string Fqhn { get; set; }

        [DataMember]
        [Option('p', "port", HelpText = "Server Port")]
        public int Port { get; set; }

        [DataMember]
        [Option('i', "nic", HelpText = "Network interface/IP Address/ Mask to use for discovery (e.g. lo, wlp1s0, eth0, 192.168.1.0/24, 192.168.1.0)")]
        public string Nic { get; set; }

        [DataMember]
        [Option('m', "mdnsSuffix", HelpText = "Domain suffix for server discovery")]
        public string MdnsSuffix { get; set; }

        [DataMember]
        [Option('l', "list-interfaces", Default = false, Required = false, HelpText = "List available network interfaces")]
        public bool ListInterfaces { get; set; }

        [DataMember]
        [Option('v', "version", Default = false, Required = false, HelpText = "Show version of SiLA2.dll")]
        public bool ShowVersion { get; set; }
    }
}