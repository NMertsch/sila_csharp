﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;

namespace SiLA2.Utils.Network
{
    public interface INetworkService
    {
        Func<IEnumerable<NetworkInterface>, IEnumerable<NetworkInterface>> CreateFilterFunc(params NetworkInterface[] selectedInterfaces);
        NetworkInterface FindValidNetworkInterface();
        NetworkInterface GetNetworkInterface(string interfaceNameOrCIDR);
        NetworkInterface GetNetworkInterfaceByCIDR(string mask);
        NetworkInterface GetNetworkInterfaceByName(string interfaceName);
        IEnumerable<IPAddress> ListInterNetworkAddresses(NetworkInterface networkInterface);
    }
}