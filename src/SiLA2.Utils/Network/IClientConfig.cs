﻿namespace SiLA2.Utils.Network
{
    public interface IClientConfig
    {
        string ClientName { get; set; }
        string DiscoveryServiceName { get; set; }
        string IpOrCdirOrFullyQualifiedHostName { get; set; }
        string NetworkInterface { get; set; }
        int Port { get; set; }
    }
}