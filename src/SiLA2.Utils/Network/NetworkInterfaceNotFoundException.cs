namespace SiLA2.Utils.Network
{
    public class NetworkInterfaceNotFoundException : System.Exception
    {
        public NetworkInterfaceNotFoundException(string message) : base(message)
        { }
    }
}