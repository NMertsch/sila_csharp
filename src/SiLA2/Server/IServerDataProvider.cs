﻿using Grpc.Net.Client;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace SiLA2.Server
{
    public interface IServerDataProvider
    {
        Task<ServerData> GetServerData(string host, int port, bool acceptAnyServerCertificate, string silaCA);
        Task<ServerData> GetServerData(string host, int port);
    }
}