﻿using System;
using System.Threading.Tasks;
using Google.Protobuf;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.Logging;
using SiLA2.Server.Services;

namespace SiLA2.Server.Interceptors
{
    public class ParameterValidationInterceptor : Interceptor
    {
        //private readonly IRequestValidationService _requestValidationService;
        private readonly ILogger _logger;


        public ParameterValidationInterceptor(/*IRequestValidationService requestValidationService,*/ ILogger<ParameterValidationInterceptor> logger)
        {
            //_requestValidationService = requestValidationService;
            _logger = logger;
        }

        public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request, ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
        {
            //TODO: call parameter validation

            try
            {
                return await continuation(request, context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error thrown by {context.Method}.");
                throw;
            }
        }
    }
}
