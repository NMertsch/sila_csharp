﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.Logging;
using SiLA2.Server.Services;
using SiLA2.Server.Utils;

namespace SiLA2.Server.Interceptors
{
    public class LoggingInterceptor : Interceptor
    {
        private IRequestValidationService _requestValidationService;
        private readonly ILogger _logger;

        public LoggingInterceptor(/*IRequestValidationService requestValidationService, */ILogger<LoggingInterceptor> logger)
        {
            /*_requestValidationService = requestValidationService;*/
            _logger = logger;
        }

        public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request, ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
        {
            _logger.LogInformation($"Received {MethodType.Unary} call to {context.Method}");
            try
            {
                return await continuation(request, context);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error raised by {context.Method}: {ErrorHandling.HandleException(ex)}");
                throw;
            }
        }

        public override async Task<TResponse> ClientStreamingServerHandler<TRequest, TResponse>(IAsyncStreamReader<TRequest> requestStream, ServerCallContext context, ClientStreamingServerMethod<TRequest, TResponse> continuation)
        {
            _logger.LogInformation($"Received {MethodType.ClientStreaming} call to {context.Method}");
            try
            {
                return await base.ClientStreamingServerHandler(requestStream, context, continuation);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error raised by {context.Method}: {ErrorHandling.HandleException(ex)}");
                throw;
            }
        }

        public override async Task ServerStreamingServerHandler<TRequest, TResponse>(TRequest request, IServerStreamWriter<TResponse> responseStream, ServerCallContext context, ServerStreamingServerMethod<TRequest, TResponse> continuation)
        {
            _logger.LogInformation($"Received {MethodType.ServerStreaming} call to {context.Method}");
            try
            {
                await base.ServerStreamingServerHandler(request, responseStream, context, continuation);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error raised by {context.Method}: {ErrorHandling.HandleException(ex)}");
                throw;
            }
        }

        public override async Task DuplexStreamingServerHandler<TRequest, TResponse>(IAsyncStreamReader<TRequest> requestStream, IServerStreamWriter<TResponse> responseStream, ServerCallContext context, DuplexStreamingServerMethod<TRequest, TResponse> continuation)
        {
            _logger.LogInformation($"Received {MethodType.DuplexStreaming} call to {context.Method}");
            try
            {
                await base.DuplexStreamingServerHandler(requestStream, responseStream, context, continuation);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error raised by {context.Method}: {ErrorHandling.HandleException(ex)}");
                throw;
            }
        }
    }
}

