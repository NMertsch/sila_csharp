namespace SiLA2.Server
{
    using System;
    using SiLA2.Utils.Config;

    public class ServerData
    {
        public ServerConfig Config { get; }
        public ServerInformation Info { get; }
        public string SilaCA { get; set; }

        /// <summary>
        /// Server model constructor (immutable object)
        /// </summary>
        /// <param name="serverConfig">Server configuration data</param>
        /// <param name="serverInformation">Server information</param>
        public ServerData(ServerConfig serverConfig, ServerInformation serverInformation)
        {
            Config = serverConfig;
            Info = serverInformation;
        }

        /// <summary>
        /// Overriden method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Config}{Environment.NewLine}{Info}";
        }
    }
}