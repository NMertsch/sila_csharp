﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace SiLA2.Server
{
    public interface IBinaryUploadRepository
    {
        public struct UploadData
        {
            public ulong ExpectedLength;
            public byte[][] Data;
        }

        ConcurrentDictionary<Guid, UploadData> UploadDataMap { get; }

        Task<byte[]> GetUploadedData(string binaryTransferUUIDString);

        Task<ulong> GetUploadedDataSize(Guid binaryTransferUUID);

        Task<Guid> CheckBinaryUploadTransferUUID(string binaryTransferUUIDString);
    }
}