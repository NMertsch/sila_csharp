﻿using Sila2.Org.Silastandard;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{
    public interface ISiLAServerMessageService
    {
        ConcurrentQueue<SiLAServerMessage> ServerMessageResponses { get; }
        Task<SiLAServerMessage> ProcessSilaClientRequest(SiLAClientMessage siLAClientMessage);
    }
}