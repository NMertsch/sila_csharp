﻿using CommandLine;
using Google.Protobuf.Collections;
using SiLA2.Server.Utils;
using SiLA2.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace SiLA2.Server.Services
{
    public class MetadataManager
    {
        private class MetadataAffection
        {
            public string FullyQualifiedCallIdentifier { get; set; }

            public string MethodCallRegexPattern { get; set; }

            public string MetadataIdentifier { get; set; }

            public FeatureMetadata MetadataDefinition { get; set; }
        }

        private readonly List<MetadataAffection> _requiredMetadata = new();

        public void CollectMetadataAffections(Feature feature, object classInstance)
        {
            if (classInstance == null)
            {
                throw new ApplicationException($"[{new StackTrace().GetFrame(0)?.GetMethod()?.Name}] Parameter 'classInstance' must not be null (called by '{new StackTrace().GetFrame(1)?.GetMethod()?.Name}')");
            }

            var definedMetadata = feature.GetDefinedMetadata();
            foreach (var featureMetadata in definedMetadata)
            {
                var methodName = $"Get_FCPAffectedByMetadata_{featureMetadata.Identifier}";
                var methodInfo = classInstance.GetType().GetMethod(methodName);

                if (methodInfo == null)
                {
                    throw new ApplicationException($"[{new StackTrace().GetFrame(0)?.GetMethod()?.Name}] Missing method '{methodName}' in class '{classInstance.GetType()}'");
                }

                var parameters = methodInfo.GetParameters();
                if (parameters.Length != 2)
                {
                    throw new ApplicationException($"[{new StackTrace().GetFrame(0)?.GetMethod()?.Name}] Invalid number of parameters ({parameters.Length}) in method '{methodName}' of class '{classInstance.GetType()}' (expected: 2)");
                }

                var result = methodInfo.Invoke(classInstance, new object[] { null, null });
                if (result == null)
                {
                    throw new ApplicationException($"[{new StackTrace().GetFrame(0)?.GetMethod()?.Name}] Call to '{methodName}' delivered no result");
                }

                //var resultType = $"{methodName}_Responses";

                var resultField = result.GetType().GetProperty("Result")?.GetValue(result);
                if (resultField == null)
                {
                    throw new ApplicationException($"[{new StackTrace().GetFrame(0)?.GetMethod()?.Name}] Result of call to '{methodName}' contains no 'Result' field");
                }

                var affectedCallsField = resultField.GetType().GetProperty("AffectedCalls")?.GetValue(resultField);
                if (affectedCallsField == null)
                {
                    throw new ApplicationException($"[{new StackTrace().GetFrame(0)?.GetMethod()?.Name}] Result of call to '{methodName}' in '{new StackTrace().GetFrame(1)?.GetMethod()?.Name}' does not contain a 'AffectedCalls' field");
                }

                foreach (var ac in affectedCallsField.Cast<RepeatedField<Sila2.Org.Silastandard.String>>())
                {
                    _requiredMetadata.Add(new MetadataAffection
                    {
                        FullyQualifiedCallIdentifier = ac.Value,
                        MethodCallRegexPattern = FullyQualifiedIdentifier2RegexPattern(ac.Value),
                        MetadataIdentifier = SilaClientMetadata.ConvertMetadataIdentifierToWireFormat(feature.GetFullyQualifiedMetadataIdentifier(featureMetadata.Identifier)),
                        MetadataDefinition = featureMetadata

                    });
                }
            }
        }

        public List<KeyValuePair<string, FeatureMetadata>> GetRequiredMetadataForCall(string methodName)
        {
            return _requiredMetadata.Where(x => Regex.Match(methodName, x.MethodCallRegexPattern).Success).Select(entry => new KeyValuePair<string, FeatureMetadata>(entry.MetadataIdentifier, entry.MetadataDefinition)).ToList();
        }

        public List<KeyValuePair<string, FeatureMetadata>> GetRequiredMetadataForFullyQualifiedIdentifier(string fullyQualifiedIdentifier)
        {
            return _requiredMetadata.Where(x => fullyQualifiedIdentifier.StartsWith(x.FullyQualifiedCallIdentifier)).Select(entry => new KeyValuePair<string, FeatureMetadata>(entry.MetadataIdentifier, entry.MetadataDefinition)).ToList();
        }

        private static string FullyQualifiedIdentifier2RegexPattern(string fullyQualifiedIdentifier)
        {
            if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedIdentifier(fullyQualifiedIdentifier))
            {
                throw new ApplicationException($"Given identifier '{fullyQualifiedIdentifier}' is not a fully qualified SiLA 2 identifier");
            }

            //// example:   org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition ->  /sila2.org.silastandard.core.silaservice.v1.SiLAService
            var tokens = fullyQualifiedIdentifier.Split('/');
            var patternPrefix = $"\\/sila2\\.{tokens[0]}\\.{tokens[1]}\\.{tokens[2].ToLower()}\\.{tokens[3]}\\.{tokens[2]}\\/";

            if (FullyQualifiedIdentifierUtils.IsFullyQualifiedFeatureIdentifier(fullyQualifiedIdentifier))
            {
                return patternPrefix + "(Get_|Subscribe_)?[A-Z][a-zA-Z0-9]*";
            }

            if (FullyQualifiedIdentifierUtils.IsFullyQualifiedCommandIdentifier(fullyQualifiedIdentifier))
            {
                return patternPrefix + tokens[5]; //"[A-Z][a-zA-Z0-9]*";
            }

            if (FullyQualifiedIdentifierUtils.IsFullyQualifiedPropertyIdentifier(fullyQualifiedIdentifier))
            {
                return patternPrefix + $"(Get_|Subscribe_){tokens[5]}"; //[A-Z][a-zA-Z0-9]*";
            }

            throw new ApplicationException($"Given fully qualified identifier '{fullyQualifiedIdentifier}' is not a fully qualified feature/command/property identifier");

            ////Feature: .../ (Get_ | Subscribe_)?[A - Z][a - zA - Z0 - 9] *
            ////Property: .../ (Get_ | Subscribe_)[A - Z][a - zA - Z0 - 9] *
            ////Command: .../[A - Z][a - zA - Z0 - 9] *
        }
    }
}
