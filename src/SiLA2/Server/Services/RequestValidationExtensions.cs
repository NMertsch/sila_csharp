﻿using Google.Protobuf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;

namespace SiLA2.Server.Services
{
    public static class RequestValidationExtensions
    {
        public static void ValidateCommandParameters<T>(this T request, Feature feature, string cmdId, string nspace = Constants.SILA_ORG_SILASTANDARD) where T : IMessage<T>
        {
            var parameterList = new List<SiLAElement>(feature.GetDefinedCommands().Single(x => x.Identifier == cmdId).Parameter);
            var requestProperties = request.GetType().GetProperties();

            foreach (var parameter in parameterList)
            {
                try
                {
                    // check if request contains parameter with expected name
                    var property = requestProperties.FirstOrDefault(p => p.Name == parameter.Identifier);
                    if (property == null)
                    {
                        throw new Exception("Parameter not transmitted");
                    }

                    // check if a value has been set
                    var parameterValue = property.GetValue(request);
                    if (parameterValue == null)
                    {
                        throw new Exception("No parameter value transmitted");
                    }

                    ValidateParameter(parameter, feature, property, parameterValue);
                }
                catch (Exception ex)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(feature.GetFullyQualifiedCommandParameterIdentifier(cmdId, parameter.Identifier), ex.Message));
                }
            }
        }

        private static void ValidateParameter(SiLAElement expectedParameter, Feature feature, PropertyInfo property, object parameterValue)
        {
            // get parameter data type
            DataTypeType dataType;
            if (expectedParameter.DataType.Item is string s)
            {
                // get according data type definition
                if (feature.Items.FirstOrDefault(i => i is SiLAElement e && e.Identifier == s) is not SiLAElement silaElement) { return; }

                dataType = silaElement.DataType;
            }
            else
            {
                dataType = expectedParameter.DataType;
            }

            switch (dataType.Item)
            {
                // check if parameter type matches the type of the given parameter value
                case BasicType:

                    ValidateSilaBasicTypeParameter(dataType, property);
                    break;

                case ListType type:

                    // check if a list has been passed
                    if (!property.PropertyType.Name.StartsWith("RepeatedField") || property.PropertyType.Namespace != "Google.Protobuf.Collections")
                    {
                        throw new Exception($"Wrong type {property.PropertyType.Namespace}.{property.PropertyType.Name} transmitted (expected: {type.ToString()})");
                    }

                    var expectedListElementsType = type.DataType;

                    if (!property.PropertyType.GenericTypeArguments.Any(a => a.Namespace.StartsWith(Constants.SILA_ORG_SILASTANDARD) && a.Name == expectedListElementsType.Item.ToString()))
                    {
                        throw new Exception($"Wrong list elements data type (expected: {expectedListElementsType})");
                    }

                    // check types of all list members
                    var expectedElementsType = Type.GetType($"{Constants.SILA_ORG_SILASTANDARD}.{expectedListElementsType.Item}");
                    foreach (var m in (IEnumerable)parameterValue)
                    {
                        if (m.GetType().ToString() != $"{Constants.SILA_ORG_SILASTANDARD}.{expectedListElementsType.Item}")
                        {
                            throw new Exception($"Wrong data type {m.GetType()} of list element (expected list element type: {Constants.SILA_ORG_SILASTANDARD}.{expectedListElementsType.Item.ToString()})");
                        }

                        CheckedParameterContent(m, expectedElementsType);
                    }

                    break;

                case StructureType:

                    break;

                case ConstrainedType:

                    //TODO:
                    if (parameterValue is Binary binary && binary.UnionCase == Binary.UnionOneofCase.Value)
                    {
                        // skip directly transmitted binary value for now - TODO
                        return;
                    }
                    var obj = JsonConvert.DeserializeObject(parameterValue.ToString(), Type.GetType($"{Constants.SILA_ORG_SILASTANDARD}.{dataType.Item}"));
                    break;

                default: break;
            }
        }

        private static void CheckedParameterContent(object parameterObject, Type parameterType)
        {
            if (parameterType == typeof(Binary))
            {
                var typedValue = (Binary)parameterObject;
                switch (typedValue.UnionCase)
                {
                    case Binary.UnionOneofCase.Value:

                        if (typedValue.Value == null)
                        {
                            throw new Exception("Value member of Binary value with UnionCase Value MUST not be empty");
                        }
                        break;

                    case Binary.UnionOneofCase.BinaryTransferUUID:

                        if (typedValue.BinaryTransferUUID == null || !Guid.TryParse(typedValue.BinaryTransferUUID, out _))
                        {
                            throw new Exception($"Binary transfer UUID {typedValue.BinaryTransferUUID} is not valid");
                        }

                        break;

                    default:
                        throw new Exception($"The wrong UnionCase member value {typedValue.UnionCase} must either be 'Value' or 'BinaryTransferUUID'");
                }
            }
            else
            {
                object typedParameterObject = null;
                try
                {
                    typedParameterObject = JsonConvert.DeserializeObject(parameterObject.ToString(), parameterType);
                }
                catch(Exception ex)
                {
                    var s = ex.Message;
                }

                if (typedParameterObject == null)
                {
                    throw new Exception($"Content element {parameterObject} does not fit to expected parameter type: {parameterType.FullName}");
                }

            }
        }

        private static void ValidateSilaBasicTypeParameter(DataTypeType expectedType, PropertyInfo property)
        {
            if (property.PropertyType.Namespace == null || !property.PropertyType.Namespace.StartsWith(Constants.SILA_ORG_SILASTANDARD))
            {
                throw new Exception("No basic type namespace found");
            }

            if (property.PropertyType.Name != expectedType.Item.ToString())
            {
                throw new Exception($"Wrong basic type {property.PropertyType.Name} (expected: {expectedType.Item?.ToString()})");
            }

            //var valueObject = property.GetValue(property.);
        }

        //TODO: this should be removed once, the IntermediateResponse handling is integrated into the observable command manager
        public static void ValidateIntermediateResponseParameters<T>(this T request, Feature feature, string cmdId, string nspace = Constants.SILA_ORG_SILASTANDARD) where T : IMessage<T>
        {
            var requestProperties = request.GetType().GetProperties().Where(x => x.PropertyType.Namespace.StartsWith(nspace));

            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid, "No Binary Transfer UUID parameter transmitted"));
        }
    }
}
