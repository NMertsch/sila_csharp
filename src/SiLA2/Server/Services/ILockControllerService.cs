﻿using Grpc.Core;
using Sila2.Org.Silastandard.Core.Lockcontroller.V2;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{
    public interface ILockControllerService
    {
        List<string> LockableItems { set; }

        void CheckLock(Metadata metadata);
        Task<Get_FCPAffectedByMetadata_LockIdentifier_Responses> Get_FCPAffectedByMetadata_LockIdentifier(Get_FCPAffectedByMetadata_LockIdentifier_Parameters request, ServerCallContext context);
        Task<LockServer_Responses> LockServer(LockServer_Parameters request, ServerCallContext context);
        Task Subscribe_IsLocked(Subscribe_IsLocked_Parameters request, IServerStreamWriter<Subscribe_IsLocked_Responses> responseStream, ServerCallContext context);
        Task<UnlockServer_Responses> UnlockServer(UnlockServer_Parameters request, ServerCallContext context);
    }
}