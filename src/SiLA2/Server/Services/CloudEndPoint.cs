﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;
using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{

    public class CloudEndPoint : CloudClientEndpoint.CloudClientEndpointBase
    {
        private readonly ISiLAClientMessageService _silaClientMessageService;
        private readonly ILogger<CloudEndPoint> _logger;

        public CloudEndPoint(ISiLAClientMessageService silaClientMessageService,ILogger<CloudEndPoint> logger)
        {
            _silaClientMessageService = silaClientMessageService;
            _logger = logger;
        }


        public override async Task ConnectSiLAServer(IAsyncStreamReader<SiLAServerMessage> requestStream, IServerStreamWriter<SiLAClientMessage> responseStream, ServerCallContext context)
        {
            await Task.Run(async() => {
                while (true)
                {
                    if (_silaClientMessageService.ClientMessageRequests.TryDequeue(out SiLAClientMessage siLAClientMessage))
                    {
                        await responseStream.WriteAsync(siLAClientMessage);
                        await HandleRequest(requestStream);
                    }

                    await Task.Delay(Constants.SERVER_INITIATED_MSG_QUEUE_DELAY);
                }
            });
        }

        [Description("Work in progress...this method needs to be overwritten !")]
        protected virtual async Task HandleRequest(IAsyncStreamReader<SiLAServerMessage> requestStream)
        {
            await requestStream.MoveNext();
            try
            {
                throw new NotImplementedException("TODO: Needs to be implemented...please overwrite this method 'protected virtual async Task HandleRequest(IAsyncStreamReader<SiLAServerMessage> requestStream)' !");

                var serverName = requestStream.Current.UnobservablePropertyValue.Value.ToStringUtf8();
                await _silaClientMessageService.AddServerMessageResponse(requestStream.Current);
                System.Diagnostics.Debug.WriteLine($">>>>>>>>>>>>>>>>>>>>>>>>>> {await _silaClientMessageService.GetServerResponse(Guid.Parse(requestStream.Current.RequestUUID))} <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }
    }
}
