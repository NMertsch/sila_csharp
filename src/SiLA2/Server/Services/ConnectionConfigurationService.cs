﻿using Grpc.Core;
using Microsoft.Extensions.Configuration;
using Sila2.Org.Silastandard.Core.Connectionconfigurationservice.V1;
using System.Threading.Tasks;
using System.Linq;
using static Sila2.Org.Silastandard.Core.Connectionconfigurationservice.V1.ConnectionConfigurationService;
using System.Collections.Generic;
using SiLA2.Utils.Config;
using Sila2.Org.Silastandard;
using System;
using SiLA2.Server.Utils;
using String = Sila2.Org.Silastandard.String;
using SiLA2.Utils.gRPC;

namespace SiLA2.Server.Services
{
    public class ConnectionConfigurationService : ConnectionConfigurationServiceBase
    {
        private bool _isServerInitiatedConnectionModeEnabled = false;
        private readonly IGrpcChannelProvider _grpcChannelProvider;
        private readonly ISiLAServerMessageService _siLAServerMessageService;

        public IDictionary<ClientConfig, CloudClientEndpoint.CloudClientEndpointClient> KnownClients { get; } = new Dictionary<ClientConfig, CloudClientEndpoint.CloudClientEndpointClient>();

        public ConnectionConfigurationService(IGrpcChannelProvider grpcChannelProvider, ISiLAServerMessageService siLAServerMessageService, IConfiguration configuration)
        {
            var clientConfigs = configuration.GetSection("KnownClients").Get<List<ClientConfig>>();
            foreach (var item in clientConfigs)
            {
                KnownClients.Add(item, null);
            }
            _grpcChannelProvider = grpcChannelProvider;
            _siLAServerMessageService = siLAServerMessageService;
        }

        public override Task<Get_ConfiguredSiLAClients_Responses> Get_ConfiguredSiLAClients(Get_ConfiguredSiLAClients_Parameters request, ServerCallContext context)
        {
            var responses = new Get_ConfiguredSiLAClients_Responses();

            foreach (var key in KnownClients.Keys)
            {
                responses.ConfiguredSiLAClients.Add(new Get_ConfiguredSiLAClients_Responses.Types.ConfiguredSiLAClients_Struct
                {
                    ClientName = new String { Value = key.ClientName },
                    SiLAClientHost = new String { Value = key.IpOrCdirOrFullyQualifiedHostName },
                    SiLAClientPort = new Integer { Value = key.Port }
                });
            }

            return Task.FromResult(responses);
        }

        public override Task<Get_ServerInitiatedConnectionModeStatus_Responses> Get_ServerInitiatedConnectionModeStatus(Get_ServerInitiatedConnectionModeStatus_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_ServerInitiatedConnectionModeStatus_Responses { ServerInitiatedConnectionModeStatus = new Sila2.Org.Silastandard.Boolean { Value = _isServerInitiatedConnectionModeEnabled } });
        }

        public override async Task<ConnectSiLAClient_Responses> ConnectSiLAClient(ConnectSiLAClient_Parameters request, ServerCallContext context)
        {
            var client = KnownClients.Keys.SingleOrDefault(key => key.IpOrCdirOrFullyQualifiedHostName == request.SiLAClientHost.Value && key.Port == request.SiLAClientPort.Value);
            if (client != null)
            {
                try
                {
                    var channel = await _grpcChannelProvider.GetChannel(client.IpOrCdirOrFullyQualifiedHostName, client.Port, true);
                    var cloudClient = new CloudClientEndpoint.CloudClientEndpointClient(channel);
                    KnownClients[client] = cloudClient;
                    var duplexStream = cloudClient.ConnectSiLAServer();

                    await foreach (SiLAClientMessage? response in duplexStream.ResponseStream.ReadAllAsync())
                    {
                        var serverMessage = await _siLAServerMessageService.ProcessSilaClientRequest(response);
                        await duplexStream.RequestStream.WriteAsync(serverMessage);
                    }

                }
                catch (Exception ex)
                {
                    KnownClients[client] = null;
                    ErrorHandling.HandleException(ex);
                }
            }
            return new ConnectSiLAClient_Responses();
        }

        public override Task<DisableServerInitiatedConnectionMode_Responses> DisableServerInitiatedConnectionMode(DisableServerInitiatedConnectionMode_Parameters request, ServerCallContext context)
        {
            _isServerInitiatedConnectionModeEnabled = false;
            var disableServerInitiatedConnectionMode_Responses = new DisableServerInitiatedConnectionMode_Responses();
            return Task.FromResult(disableServerInitiatedConnectionMode_Responses);
        }

        public override Task<DisconnectSiLAClient_Responses> DisconnectSiLAClient(DisconnectSiLAClient_Parameters request, ServerCallContext context)
        {
            var client = KnownClients.Keys.SingleOrDefault(key => new String { Value = key.ClientName } == request.ClientName);
            if (client != null)
            {
                KnownClients[client] = null;
            }
            return Task.FromResult(new DisconnectSiLAClient_Responses());
        }

        public override Task<EnableServerInitiatedConnectionMode_Responses> EnableServerInitiatedConnectionMode(EnableServerInitiatedConnectionMode_Parameters request, ServerCallContext context)
        {
            _isServerInitiatedConnectionModeEnabled = true;
            var enableServerInitiatedConnectionMode_Responses = new EnableServerInitiatedConnectionMode_Responses();
            return Task.FromResult(enableServerInitiatedConnectionMode_Responses);
        }
    }
}
