﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;
using System;
using System.Threading.Tasks;
using SiLA2.Utils;

namespace SiLA2.Server.Services
{
    public class SiLABinaryUploadService : BinaryUpload.BinaryUploadBase
    {
        private readonly ISiLA2Server _siLA2Server;
        private readonly IBinaryUploadRepository _binaryDataRepository;
        private readonly ILogger<SiLABinaryUploadService> _logger;

        public SiLABinaryUploadService(ISiLA2Server siLA2Server, IBinaryUploadRepository binaryDataRepository, ILogger<SiLABinaryUploadService> logger)
        {
            _siLA2Server = siLA2Server;
            _binaryDataRepository = binaryDataRepository;
            _logger = logger;
        }

        #region Overrides of BinaryUploadBase

        public override Task<CreateBinaryResponse> CreateBinary(CreateBinaryRequest request, ServerCallContext context)
        {
            try
            {
                // check if specified parameter matches pattern for fully qualified command parameter identifier
                if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedCommandParameterIdentifier(request.ParameterIdentifier))
                {
                    // not a fully qualified command parameter identifier
                    ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                        $"Specified parameter identifier '{request.ParameterIdentifier}' is not a valid fully qualified command parameter identifier");
                }

                // check if specified parameter is defined in a feature and has Binary type
                var feature = _siLA2Server.GetFeatureOfElement(request.ParameterIdentifier);
                if (feature == null)
                {
                    // specified feature does not exist
                    ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                        $"Found no implemented feature that matches the parameter identifier '{request.ParameterIdentifier}'");
                }

                var obj = feature.GetMatchingElement(request.ParameterIdentifier);
                if (obj == null)
                {
                    // specified command or parameter does not exist
                    ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                        $"No command/parameter found that matches the parameter identifier '{request.ParameterIdentifier}'");
                }

                if (((SiLAElement)obj).DataType.Item is not BasicType.Binary)
                {
                    // wrong data type
                    ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                        $"Specified parameter identifier '{request.ParameterIdentifier}' is not of type Binary");
                }

                // check whether metadata is required for it the regarding command
                var containedMetadata = SilaClientMetadata.GetAllSilaClientMetadataIdentifiers(context.RequestHeaders);
                foreach (var metadata in _siLA2Server.MetadataManager.GetRequiredMetadataForFullyQualifiedIdentifier(request.ParameterIdentifier))
                {
                    if (!containedMetadata.Contains(metadata.Key))
                    {
                        ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, $"No metadata '{metadata.Key}' found in client request to 'CreateBinary'"));
                    }
                }

                //TODO: compare size with available resources

                // create storage for data
                var binaryTransferUUID = Guid.NewGuid();
                _binaryDataRepository.UploadDataMap[binaryTransferUUID] = new IBinaryUploadRepository.UploadData
                {
                    ExpectedLength = request.BinarySize,
                    Data = new byte[request.ChunkCount][]
                };

                return Task.FromResult(new CreateBinaryResponse
                {
                    BinaryTransferUUID = binaryTransferUUID.ToString(),
                    LifetimeOfBinary = new Duration()   ////TODO: add lifetime of binary data
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                throw ex;
            }
        }

        public override async Task UploadChunk(IAsyncStreamReader<UploadChunkRequest> requestStream, IServerStreamWriter<UploadChunkResponse> responseStream, ServerCallContext context)
        {
            try
            {
                while (await requestStream.MoveNext(context.CancellationToken))
                {
                    Guid binaryTransferUUID = new();
                    try
                    {
                        binaryTransferUUID = await _binaryDataRepository.CheckBinaryUploadTransferUUID(requestStream.Current.BinaryTransferUUID);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.InvalidBinaryTransferUuid, ex.Message);
                    }

                    // check for maximum chunk size of 2 MiB
                    if (requestStream.Current.Payload.Length > 2 * 1024 * 1024)
                    {
                        ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                            $"The sent chunk size ({requestStream.Current.Payload.Length}) is out of bounds (maximum chunk size is 2 MiB)");
                    }

                    // check for valid chunk index
                    if (requestStream.Current.ChunkIndex >= _binaryDataRepository.UploadDataMap[binaryTransferUUID].Data.Length)
                    {
                        ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                            $"Chunk index ({requestStream.Current.ChunkIndex}) is out of bounds (must be between 0 and {_binaryDataRepository.UploadDataMap[binaryTransferUUID].Data.Length - 1})");
                    }

                    // check for double uploaded chunk
                    if (_binaryDataRepository.UploadDataMap[binaryTransferUUID].Data[requestStream.Current.ChunkIndex] != null)
                    {
                        ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                            $"Double upload of chunk with index {requestStream.Current.ChunkIndex}");
                    }

                    // check for data size limit
                    if (_binaryDataRepository.GetUploadedDataSize(binaryTransferUUID).Result + (ulong)requestStream.Current.Payload.Length > _binaryDataRepository.UploadDataMap[binaryTransferUUID].ExpectedLength)
                    {
                        ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                            $"Data to be uploaded exceeds the specified data size of {requestStream.Current.ChunkIndex}");
                    }

                    // set received data chunk
                    _binaryDataRepository.UploadDataMap[binaryTransferUUID].Data[requestStream.Current.ChunkIndex] = requestStream.Current.Payload.ToByteArray();

                    await responseStream.WriteAsync(new UploadChunkResponse
                    {
                        BinaryTransferUUID = binaryTransferUUID.ToString(),
                        ChunkIndex = requestStream.Current.ChunkIndex,
                        LifetimeOfBinary = new Duration()    ////TODO: add lifetime of binary data
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                throw ex;
            }
        }

        public override async Task<DeleteBinaryResponse> DeleteBinary(DeleteBinaryRequest request, ServerCallContext context)
        {
            try
            {
                var binaryTransferUUID = await _binaryDataRepository.CheckBinaryUploadTransferUUID(request.BinaryTransferUUID);

                _binaryDataRepository.UploadDataMap.TryRemove(binaryTransferUUID, out IBinaryUploadRepository.UploadData _);
                ////TODO: check whether this crashes with a parallel running binary download
            }
            catch (Exception ex)
            {
                ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.InvalidBinaryTransferUuid, ex.Message);
            }

            return new DeleteBinaryResponse();
        }

        #endregion
    }
}
