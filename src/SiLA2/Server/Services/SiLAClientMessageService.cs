﻿using Sila2.Org.Silastandard;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{
    public class SiLAClientMessageService : ISiLAClientMessageService
    {
        private ConcurrentDictionary<Guid, SiLAServerMessage> _serverMessageResponseMap { get; } = new ConcurrentDictionary<Guid, SiLAServerMessage>();
        public ConcurrentQueue<SiLAClientMessage> ClientMessageRequests { get; } = new ConcurrentQueue<SiLAClientMessage>();

        public async Task AddClientMessageRequest(SiLAClientMessage silaClientMessage)
        {
            await Task.Run(() =>
            {
                if (string.IsNullOrEmpty(silaClientMessage.RequestUUID))
                {
                    silaClientMessage.RequestUUID = Guid.NewGuid().ToString();
                }

                _serverMessageResponseMap.GetOrAdd(Guid.Parse(silaClientMessage.RequestUUID), x => null);

                ClientMessageRequests.Enqueue(silaClientMessage); 
            });
        }

        public async Task<SiLAServerMessage> GetServerResponse(Guid id)
        {
            return await Task.FromResult(_serverMessageResponseMap[id]);
        }

        public async Task AddServerMessageResponse(SiLAServerMessage silaServerMessage)
        {
            await Task.FromResult(_serverMessageResponseMap[Guid.Parse(silaServerMessage.RequestUUID)] = silaServerMessage);
        }
    }
}
