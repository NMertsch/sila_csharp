﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using SiLA2.Server.Utils;
using SiLA2.Utils;
using SiLA2.Utils.Config;
using SiLA2.Utils.Network;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.Server.Services
{

    public class SiLAService : SiLAServiceBase
    {
        private readonly ISiLA2Server _siLA2Server;
        private readonly IWritableOptions<ServerConfig> _writableServerConfigSection;
        private readonly IServerConfig _serverConfig;
        private readonly ILogger<SiLAService> _logger;

        public Feature SiLAFeature { get; set; }

        public SiLAService(ISiLA2Server siLA2Server, IWritableOptions<ServerConfig> writableServerConfigSection, IServerConfig serverConfig, ILogger<SiLAService> logger)
        {
            _siLA2Server = siLA2Server;
            _writableServerConfigSection = writableServerConfigSection;
            _serverConfig = serverConfig;
            _logger = logger;
        }

        public override Task<GetFeatureDefinition_Responses> GetFeatureDefinition(GetFeatureDefinition_Parameters request, ServerCallContext context)
        {
            if(request.FeatureIdentifier == null || string.IsNullOrEmpty(request.FeatureIdentifier.Value))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError("org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/Parameter/FeatureIdentifier", "The FeatureIdentifier must not be NULL or empty !"));
                return Task.FromResult(new GetFeatureDefinition_Responses());
            }

            // lookup feature by given qualified identifier
            if (!_siLA2Server.ImplementedFeatures.Contains(request.FeatureIdentifier.Value))
            {
                if(Regex.IsMatch(request.FeatureIdentifier.Value, Constants.FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX, RegexOptions.IgnoreCase))
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError("org.silastandard/core/SiLAService/v1/DefinedExecutionError/UnimplementedFeature", 
                                                                                          $"Feature definition '{request.FeatureIdentifier.Value}' is probably missing. Check that the requested feature definition is valid and available"));
                }
                else
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError("org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/Parameter/FeatureIdentifier", $"The Fully Qualified FeatureIdentifier does not match its Regular Expression '{Constants.FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX}' !"));
                }
                return Task.FromResult(new GetFeatureDefinition_Responses());
            }

            CheckMetadata(context);

            // get feature from server
            var feature = _siLA2Server.GetFeature(request.FeatureIdentifier.Value);

            // serialize Feature definition
            var serializer = new XmlSerializer(typeof(Feature));
            var stringBuilder = new StringBuilder();
            using (var stringWriter = new ExtentedStringWriter(stringBuilder, new UTF8Encoding(false)))
            {
                serializer.Serialize(stringWriter, feature);
            }

            return Task.FromResult(new GetFeatureDefinition_Responses { FeatureDefinition = new SiLAFramework.String { Value = stringBuilder.ToString().Replace(Environment.NewLine, string.Empty) } });
        }

        //TODO: Write ServerName to appSettings.json and update it in SiLAServer
        public override Task<SetServerName_Responses> SetServerName(SetServerName_Parameters request, ServerCallContext context)
        {
            if (request.ServerName == null || string.IsNullOrEmpty(request.ServerName.Value))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError("org.silastandard/core/SiLAService/v1/Command/SetServerName/Parameter/ServerName", "The ServerName must not be NULL or empty !"));
                return Task.FromResult(new SetServerName_Responses());
            }

            if(request.ServerName.Value.Length > 255)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError("org.silastandard/core/SiLAService/v1/Command/SetServerName/Parameter/ServerName", "The ServerName must not be longer than 255 characters !"));
                return Task.FromResult(new SetServerName_Responses());
            }

            CheckMetadata(context);

            _serverConfig.Name = request.ServerName.Value;
            _writableServerConfigSection.Update(opt => opt = (ServerConfig)_serverConfig);

            return Task.FromResult(new SetServerName_Responses());
        }

        public override Task<Get_ServerName_Responses> Get_ServerName(Get_ServerName_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);
            return Task.FromResult(new Get_ServerName_Responses { ServerName = new SiLAFramework.String { Value = _serverConfig.Name } });
        }

        public override Task<Get_ServerUUID_Responses> Get_ServerUUID(Get_ServerUUID_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);
            return Task.FromResult(new Get_ServerUUID_Responses { ServerUUID = new SiLAFramework.String { Value = _serverConfig.Uuid.ToString() } });
        }

        public override Task<Get_ServerType_Responses> Get_ServerType(Get_ServerType_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);
            return Task.FromResult(new Get_ServerType_Responses { ServerType = new SiLAFramework.String { Value = _siLA2Server.ServerInformation.Type } });
        }

        public override Task<Get_ServerDescription_Responses> Get_ServerDescription(Get_ServerDescription_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);
            return Task.FromResult(new Get_ServerDescription_Responses { ServerDescription = new SiLAFramework.String { Value = _siLA2Server.ServerInformation.Description } });
        }

        public override Task<Get_ServerVendorURL_Responses> Get_ServerVendorURL(Get_ServerVendorURL_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);
            return Task.FromResult(new Get_ServerVendorURL_Responses { ServerVendorURL = new SiLAFramework.String { Value = _siLA2Server.ServerInformation.VendorURI } });
        }

        public override Task<Get_ServerVersion_Responses> Get_ServerVersion(Get_ServerVersion_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);
            return Task.FromResult(new Get_ServerVersion_Responses { ServerVersion = new SiLAFramework.String { Value = _siLA2Server.ServerInformation.Version } });
        }

        public override Task<Get_ImplementedFeatures_Responses> Get_ImplementedFeatures(Get_ImplementedFeatures_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);
            var response = new Get_ImplementedFeatures_Responses();
            foreach (var feature in _siLA2Server.ImplementedFeatures)
            {
                response.ImplementedFeatures.Add(new SiLAFramework.String { Value = feature });
            }

            return Task.FromResult(response);
        }

        private void CheckMetadata(ServerCallContext context)
        {
            if (SilaClientMetadata.GetAllSilaClientMetadataIdentifiers(context.RequestHeaders).Count > 0)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.NoMetadataAllowed, "Requests of the SiLAService feature MUST NOT contain SiLA Client Metadata"));
            }
        }

        private sealed class ExtentedStringWriter : StringWriter
        {
            public ExtentedStringWriter(StringBuilder builder, Encoding desiredEncoding) : base(builder)
            {
                Encoding = desiredEncoding;
            }

            public override Encoding Encoding { get; }
        }
    }
}
