﻿using Grpc.Net.Client;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using System;
using SiLA2.Utils.Config;
using SiLA2.Utils.gRPC;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Logging;

namespace SiLA2.Server
{
    public class ServerDataProvider : IServerDataProvider
    {
        private readonly IGrpcChannelProvider _grpcChannelProvider;
        private readonly ILogger<ServerDataProvider> _logger;

        public ServerDataProvider(IGrpcChannelProvider grpcChannelProvider, ILogger<ServerDataProvider>logger)
        {
            _grpcChannelProvider = grpcChannelProvider;
            _logger = logger;
        }

        public async Task<ServerData> GetServerData(string host, int port)
        {
            return await GetServerData(host, port, true, null);
        }

        public async Task<ServerData> GetServerData(string host, int port, bool acceptAnyServerCertificate, string silaCA)
        {
            GrpcChannel channel = null;
            if(string.IsNullOrEmpty(silaCA))
            {
                //Get Server Channel
                channel = await _grpcChannelProvider.GetChannel(host, port, true);
            }
            else
            {
                var ca = GetCaFromFormattedCa(silaCA);
                channel = await _grpcChannelProvider.GetChannel(host, port, acceptAnyServerCertificate, ca);
            }

            var silaService = new SiLAService.SiLAServiceClient(channel);
            var config = GetServerConfig(silaService, host, port);
            var info = GetServerInfo(silaService);
            var server = new ServerData(config, info);
            return server;
        }

        private ServerConfig GetServerConfig(SiLAService.SiLAServiceClient silaService, string host, int port)
        {
            var nameResponse = silaService.Get_ServerName(new Get_ServerName_Parameters());
            var uuidRsponsse = silaService.Get_ServerUUID(new Get_ServerUUID_Parameters());
            return new ServerConfig(nameResponse.ServerName.Value, Guid.Parse(uuidRsponsse.ServerUUID.Value), host, port);
        }

        private ServerInformation GetServerInfo(SiLAService.SiLAServiceClient silaService)
        {
            var typeResponse = silaService.Get_ServerType(new Get_ServerType_Parameters());
            var descriptionResponse = silaService.Get_ServerDescription(new Get_ServerDescription_Parameters());
            var vendorUriResponse = silaService.Get_ServerVendorURL(new Get_ServerVendorURL_Parameters());
            var versionResponse = silaService.Get_ServerVersion(new Get_ServerVersion_Parameters());
            return new ServerInformation(typeResponse.ServerType.Value, descriptionResponse.ServerDescription.Value, vendorUriResponse.ServerVendorURL.Value, versionResponse.ServerVersion.Value);
        }

        private X509Certificate2 GetCaFromFormattedCa(string ca)
        {
            X509Certificate2 cacert = null;
            try
            {
                var sb = new StringBuilder();
                var caMdnsSplitted = ca.Split("\r\n");
                foreach (var line in caMdnsSplitted)
                {
                    if (line.StartsWith("ca"))
                    {
                        var firstEquals = line.IndexOf('=');
                        if (firstEquals < 0)
                            continue;
                        sb.Append(line.Substring(firstEquals + 1) + "\n");
                    }
                }
                var caSb = sb.ToString();

                var pem = PemEncoding.Find(caSb);
                var certData = Convert.FromBase64String(caSb[pem.Base64Data]);
                cacert = new X509Certificate2(certData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, null);
            }
            return cacert;
        }
    }
}
