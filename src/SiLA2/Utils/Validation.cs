﻿using System;
using System.Globalization;
using System.Linq;
using SiLA2.Utils;

namespace SiLA2.Server.Utils
{
    /// <summary>
    /// This class provides functions to validate command parameters against their according constraints as defined for the given parameter in the given Feature Definition.
    /// Raises a SiLA validation error in case a constraint is violated.
    /// TODO: implement functions for all SiLA Basic types and all potential constraints
    /// </summary>
    public static class Validation
    {
        /// <summary>
        /// Validates the given value of a String type parameter according to the constrains defined for the parameter of the given command in the given feature
        /// </summary>
        /// <param name="parameterValue">The value to be validated.</param>
        /// <param name="feature">The feature that defines the given parameter.</param>
        /// <param name="commandIdentifier">The identifier of the command the parameter is defined for.</param>
        /// <param name="parameterIdentifier">The identifier of the parameter thats constraints shall be used for validation.</param>
        public static void ValidateParameter(string parameterValue, Feature feature, string commandIdentifier, string parameterIdentifier)
        {
            // lookup command
            if (feature.Items.FirstOrDefault(i => i is FeatureCommand c && c.Identifier == commandIdentifier) is not FeatureCommand command) { return; }

            // lookup parameter
            if (command.Parameter.FirstOrDefault(p => p.Identifier == parameterIdentifier) is not { } parameter) { return; }

            // get parameter data type
            DataTypeType dataType;
            if (parameter.DataType.Item is string s)
            {
                // get according data type definition
                if (feature.Items.FirstOrDefault(i => i is SiLAElement e && e.Identifier == s) is not SiLAElement silaElement) { return; }

                dataType = silaElement.DataType;
            }
            else
            {
                dataType = parameter.DataType;
            }

            // check if parameter has constraints
            if (dataType.Item is not ConstrainedType cType) { return; }

            // check if parameter type matches the type of the given parameter value
            if (cType.DataType.Item is not BasicType.String) { return; }

            // check for constraints
            if (cType.Constraints.Length != null && parameterValue.Length != int.Parse(cType.Constraints.Length, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "String length out of bound: the length must be " + cType.Constraints.Length));
            }

            if (cType.Constraints.MinimalLength != null && parameterValue.Length < int.Parse(cType.Constraints.MinimalLength, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "String length out of bound: the length must not be less than " + cType.Constraints.MinimalLength));
            }

            if (cType.Constraints.MaximalLength != null && parameterValue.Length > int.Parse(cType.Constraints.MaximalLength, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "String length of bound: the length must not be greater than " + cType.Constraints.MaximalLength));
            }

            if (cType.Constraints.Set != null && !cType.Constraints.Set.Contains(parameterValue))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"String value `{parameterValue}` is not part of the defined set: the value must be one of {string.Join(',', cType.Constraints.Set.ToArray())}"));
            }

            if (cType.Constraints.Pattern != null && !System.Text.RegularExpressions.Regex.Match(parameterValue, cType.Constraints.Pattern).Success)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"String value `{parameterValue}` does not match the defined pattern '{cType.Constraints.Pattern}'"));
            }

            if (cType.Constraints.ContentType != null)
            {
                //TODO
                throw new Grpc.Core.RpcException(new Grpc.Core.Status(Grpc.Core.StatusCode.Unimplemented, ""));
            }

            if (cType.Constraints.FullyQualifiedIdentifierSpecified)
            {
                switch (cType.Constraints.FullyQualifiedIdentifier)
                {
                    case ConstraintsFullyQualifiedIdentifier.FeatureIdentifier:
                        if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedFeatureIdentifier(parameterValue))
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value `{parameterValue}` is not a fully qualified feature identifier"));
                        }
                        break;

                    case ConstraintsFullyQualifiedIdentifier.CommandIdentifier:
                        if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedCommandIdentifier(parameterValue))
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value `{parameterValue}` is not a fully qualified command identifier"));
                        }
                        break;

                    case ConstraintsFullyQualifiedIdentifier.CommandParameterIdentifier:
                        if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedCommandParameterIdentifier(parameterValue))
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value `{parameterValue}` is not a fully qualified command parameter identifier"));
                        }
                        break;

                    case ConstraintsFullyQualifiedIdentifier.CommandResponseIdentifier:
                        if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedCommandResponseIdentifier(parameterValue))
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value `{parameterValue}` is not a fully qualified command response identifier"));
                        }
                        break;

                    case ConstraintsFullyQualifiedIdentifier.IntermediateCommandResponseIdentifier:
                        if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedIntermediateCommandResponseIdentifier(parameterValue))
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value `{parameterValue}` is not a fully qualified intermediate command response identifier"));
                        }
                        break;

                    case ConstraintsFullyQualifiedIdentifier.DefinedExecutionErrorIdentifier:
                        if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedDefinedExecutionErrorIdentifier(parameterValue))
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value `{parameterValue}` is not a fully qualified defined execution error identifier"));
                        }
                        break;

                    case ConstraintsFullyQualifiedIdentifier.PropertyIdentifier:
                        if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedPropertyIdentifier(parameterValue))
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value `{parameterValue}` is not a fully qualified property identifier"));
                        }
                        break;

                    case ConstraintsFullyQualifiedIdentifier.TypeIdentifier:
                        if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedCustomDataTypeIdentifier(parameterValue))
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value `{parameterValue}` is not a fully qualified custom data type identifier"));
                        }
                        break;

                    case ConstraintsFullyQualifiedIdentifier.MetadataIdentifier:
                        if (!FullyQualifiedIdentifierUtils.IsFullyQualifiedMetadataIdentifier(parameterValue))
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value `{parameterValue}` is not a fully qualified metadata identifier"));
                        }
                        break;
                }
            }

            if (cType.Constraints.Schema != null)
            {
                switch (cType.Constraints.Schema.Type)
                {
                    case ConstraintsSchemaType.Xml:
                        if (cType.Constraints.Schema.ItemElementName == ItemChoiceType.Inline &&
                            !SchemaValidator.ValidateAgainstXmlSchema(parameterValue, cType.Constraints.Schema.Item, out var errorMessage)
                            ||
                            cType.Constraints.Schema.ItemElementName == ItemChoiceType.Url &&
                            !SchemaValidator.ValidateAgainstXmlSchemaUrl(parameterValue, cType.Constraints.Schema.Item, out errorMessage)
                           )
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value is not compliant to the specified XML schema{(cType.Constraints.Schema.ItemElementName == ItemChoiceType.Url ? $"{cType.Constraints.Schema.Item}" : string.Empty)} : {errorMessage}"));
                        }
                        break;

                    case ConstraintsSchemaType.Json:
                        if (cType.Constraints.Schema.ItemElementName == ItemChoiceType.Inline &&
                            !SchemaValidator.ValidateAgainstJsonSchema(parameterValue, cType.Constraints.Schema.Item, out errorMessage)
                            ||
                            cType.Constraints.Schema.ItemElementName == ItemChoiceType.Url &&
                            !SchemaValidator.ValidateAgainstJsonSchemaUrl(parameterValue, cType.Constraints.Schema.Item, out errorMessage)
                           )
                        {
                            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                                feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                                $"String value is not compliant to the specified Json schema{(cType.Constraints.Schema.ItemElementName == ItemChoiceType.Url ? $"{cType.Constraints.Schema.Item}" : string.Empty)} : {errorMessage}"));
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Validates the given value of a Real type parameter according to the constrains defined for the parameter of the given command in the given feature
        /// </summary>
        /// <param name="parameterValue">The value to be validated.</param>
        /// <param name="feature">The feature that defines the given parameter.</param>
        /// <param name="commandIdentifier">The identifier of the command the parameter is defined for.</param>
        /// <param name="parameterIdentifier">The identifier of the parameter thats constraints shall be used for validation.</param>
        public static void ValidateParameter(double parameterValue, Feature feature, string commandIdentifier, string parameterIdentifier)
        {
            // lookup command
            if (feature.Items.FirstOrDefault(i => i is FeatureCommand c && c.Identifier == commandIdentifier) is not FeatureCommand command) { return; }

            // lookup parameter
            if (command.Parameter.FirstOrDefault(p => p.Identifier == parameterIdentifier) is not SiLAElement parameter) { return; }

            // get parameter data type
            DataTypeType dataType;
            if (parameter.DataType.Item is string s)
            {
                // get according data type definition
                if (feature.Items.FirstOrDefault(i => i is SiLAElement e && e.Identifier == s) is not SiLAElement silaElement) { return; }

                dataType = silaElement.DataType;
            }
            else
            {
                dataType = parameter.DataType;
            }

            // check if parameter has constraints
            if (dataType.Item is not ConstrainedType cType) { return; }

            // check if parameter type matches the type of the given parameter value
            if (cType.DataType.Item is not BasicType.Real) { return; }

            // check for constraints
            if (cType.Constraints.Set != null && !cType.Constraints.Set.ToList().ConvertAll(String2Double).Contains(parameterValue))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"Value {parameterValue} is not part of the defined set: the value must be one of {string.Join(',', cType.Constraints.Set.ToArray())}"));
            }

            if (cType.Constraints.MaximalExclusive != null && parameterValue >= String2Double(cType.Constraints.MaximalExclusive))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"Value {parameterValue} out of bound: the value must be less than {cType.Constraints.MaximalExclusive}"));
            }

            if (cType.Constraints.MaximalInclusive != null && parameterValue > String2Double(cType.Constraints.MaximalInclusive))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"Value {parameterValue} out of bound: the value must not be greater than {cType.Constraints.MaximalInclusive}"));
            }

            if (cType.Constraints.MinimalExclusive != null && parameterValue <= String2Double(cType.Constraints.MinimalExclusive))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"Value {parameterValue} out of bound: the value must be greater than {cType.Constraints.MinimalExclusive}"));
            }

            if (cType.Constraints.MinimalInclusive != null && parameterValue < String2Double(cType.Constraints.MinimalInclusive))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"Value {parameterValue} out of bound: the value must not be less than {cType.Constraints.MinimalInclusive}"));
            }
        }

        /// <summary>
        /// Validates the given value of an Integer type parameter according to the constrains defined for the parameter of the given command in the given feature
        /// </summary>
        /// <param name="parameterValue">The value to be validated.</param>
        /// <param name="feature">The feature that defines the given parameter.</param>
        /// <param name="commandIdentifier">The identifier of the command the parameter is defined for.</param>
        /// <param name="parameterIdentifier">The identifier of the parameter thats constraints shall be used for validation.</param>
        public static void ValidateParameter(long parameterValue, Feature feature, string commandIdentifier, string parameterIdentifier)
        {
            // lookup command
            if (feature.Items.FirstOrDefault(i => i is FeatureCommand c && c.Identifier == commandIdentifier) is not FeatureCommand command) { return; }

            // lookup parameter
            if (command.Parameter.FirstOrDefault(p => p.Identifier == parameterIdentifier) is not SiLAElement parameter) { return; }

            // get parameter data type
            DataTypeType dataType;
            if (parameter.DataType.Item is string s)
            {
                // get according datatype definition
                if (feature.Items.FirstOrDefault(i => i is SiLAElement e && e.Identifier == s) is not SiLAElement silaElement) { return; }

                dataType = silaElement.DataType;
            }
            else
            {
                dataType = parameter.DataType;
            }

            // check if parameter has constraints
            if (!(dataType.Item is ConstrainedType cType)) { return; }

            // check if parameter type matches the type of the given parameter value
            if (!(cType.DataType.Item is BasicType.Integer)) { return; }

            // check for constraints
            if (cType.Constraints.Set != null && !cType.Constraints.Set.ToList().ConvertAll(String2Integer).Contains(parameterValue))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"Value {parameterValue} is not part of the defined set: the value must be one of {string.Join(',', cType.Constraints.Set.ToArray())}"));
            }

            if (cType.Constraints.MaximalExclusive != null && parameterValue >= String2Integer(cType.Constraints.MaximalExclusive))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"Value {parameterValue} out of bound: the value must be less than {cType.Constraints.MaximalExclusive}"));
            }

            if (cType.Constraints.MaximalInclusive != null && parameterValue > String2Integer(cType.Constraints.MaximalInclusive))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"Value {parameterValue} out of bound: the value must not be greater than {cType.Constraints.MaximalInclusive}"));
            }

            if (cType.Constraints.MinimalExclusive != null && parameterValue <= String2Integer(cType.Constraints.MinimalExclusive))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"Value {parameterValue} out of bound: the value must be greater than {cType.Constraints.MinimalExclusive}"));
            }

            if (cType.Constraints.MinimalInclusive != null && parameterValue < String2Integer(cType.Constraints.MinimalInclusive))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    $"Value {parameterValue} out of bound: the value must not be less than {cType.Constraints.MinimalInclusive}"));
            }
        }

        private static double String2Double(string value)
        {
            return double.Parse(value, NumberStyles.Float, CultureInfo.InvariantCulture);
        }

        private static long String2Integer(string value)
        {
            return long.Parse(value, NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture);
        }
    }
}