using SiLA2.Utils.Network;
using System;
using System.Runtime.Serialization;

namespace SiLA2.Utils.Config
{
    /// <summary>
    /// Server configuration data containing server properties which may change 
    /// at runtime or are configurable.
    /// </summary>
    [DataContract]
    public class ServerConfig : IServerConfig
    {
        /// <summary>
        /// Configurable name of the server
        /// </summary>
        [DataMember(IsRequired = true)]
        public string Name { get; set; }

        /// <summary>
        /// Guid of the server, loaded from config or generated
        /// </summary>
        [DataMember(IsRequired = true)]
        public Guid Uuid { get; set; }

        [DataMember(IsRequired = true)]
        public string FQHN { get; set; }

        [DataMember(IsRequired = true)]
        public int Port { get; set; }

        [DataMember(IsRequired = true)]
        public string NetworkInterface { get; set; }

        [DataMember(IsRequired = true)]
        public string DiscoveryServiceName { get; set; }

        public ServerConfig() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="uuid"></param>
        public ServerConfig(string name, Guid uuid)
        {
            Name = name;
            Uuid = uuid;
        }

        public ServerConfig(string name, Guid uuid, string ipOrFullyQualifiedHostName, int port) : this(name, uuid)
        {
            FQHN= ipOrFullyQualifiedHostName;
            Port = port;
        }

        public ServerConfig(string name, Guid uuid, string ipOrFullyQualifiedHostName, int port, string networkInterface, string discoveryServiceName) : this(name, uuid, ipOrFullyQualifiedHostName, port)
        {
            NetworkInterface = networkInterface;
            DiscoveryServiceName = discoveryServiceName;
        }

        public override string ToString()
        {
            return $"Server '{Name}' with Id '{Uuid}'";
        }
    }
}