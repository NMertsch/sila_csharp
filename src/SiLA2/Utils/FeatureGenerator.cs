﻿namespace SiLA2.Server.Utils
{
    using SiLA2.Utils;
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    /// <summary>
    /// Class to deserialize SiLA 2 feature definition (XML) into a Feature object.
    /// </summary>
    public class FeatureGenerator
    {
        private static readonly string[] FeatureDefinitionSchemas = { "FeatureDefinition.xsd", "DataTypes.xsd", "Constraints.xsd" };
        //private static readonly ILog _Logger = LogManager.GetLogger<FeatureGenerator>();
        private static string _schemaValidationError;

        /// <summary>
        /// Verifies the data of the given XML file against the FeatureDefinition schema and deserializes the content into a Feature class.
        /// </summary>
        /// <param name="stream">The file to read the XML feature definition from</param>
        /// <returns>The resulting feature object</returns>
        public static Feature ReadFeatureFromStream(Stream stream)
        {
            string content;

            using (var reader = new StreamReader(stream))
            {
                content = reader.ReadToEnd();
            }
            return ReadFeatureFromXml(content);
        }

        /// <summary>
        /// Verifies the data of the XML file at the given url against the FeatureDefinition schema and deserializes the content into a Feature class.
        /// </summary>
        /// <param name="url">The URL of the file to read the XML feature definition from</param>
        /// <returns>The resulting feature object</returns>
        public static Feature ReadFeatureFromOnlineResource(string url)
        {
            try
            {
                // create and execute the request
                var request = (HttpWebRequest)WebRequest.Create(url);
                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();

                if (responseStream == null)
                {
                    throw new Exception("Unable to get stream of response");
                }

                // read data via response stream
                string content;
                using (var reader = new StreamReader(responseStream))
                {
                    content = reader.ReadToEnd();
                }
                return ReadFeatureFromXml(content);
            }
            catch (Exception ex)
            {
                //_Logger.Error($"Error reading the file at \"{url}\"", ex);
                throw;
            }
        }

        /// <summary>
        /// Verifies the data of the given XML file against the FeatureDefinition schema and deserializes the content into a Feature class.
        /// </summary>
        /// <param name="path">The file to read the XML feature definition from</param>
        /// <returns>The resulting feature object</returns>
        public static Feature ReadFeatureFromFile(string path)
        {
            var content = File.ReadAllText(FileUtils.ResolvePath(path));
            return ReadFeatureFromXml(content);
        }

        /// <summary>
        /// Verifies the given XML data against the FeatureDefinition schema and deserializes the content into a Feature class.
        /// </summary>
        /// <param name="featureXML">The XML data of the feature </param>
        /// <returns>The resulting feature object</returns>
        public static Feature ReadFeatureFromXml(string featureXML)
        {
            _schemaValidationError = string.Empty;


            // validate XML string against the schema
            using (var stringReader = new StringReader(featureXML))
            {
                // create an XML reader using the FeatureDefinition schema for validating
                var settings = new XmlReaderSettings
                {
                    DtdProcessing = DtdProcessing.Parse,
                    MaxCharactersFromEntities = 1024,
                    ValidationType = ValidationType.Schema
                };
                settings.ValidationEventHandler += ValidationCallBack;

                foreach (var schema in FeatureDefinitionSchemas)
                {
                    AddSchemaFromResource(settings, schema);
                }

                using (var xmlReader = XmlReader.Create(stringReader, settings))
                {
                    // parse the file
                    while (xmlReader.Read()) { }
                }
            }

            if (!string.IsNullOrEmpty(_schemaValidationError))
            {
                throw new XmlSchemaValidationException("Schema validation error: " + _schemaValidationError);
            }

            // convert the XML content into a feature object
            try
            {
                var serializer = new XmlSerializer(typeof(Feature));
                using (var reader = new StringReader(featureXML))
                {
                    var feature = (Feature)serializer.Deserialize(reader);
                    reader.Close();
                    return feature;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = $"Invalid feature definition (error while deserializing XML: {ex.Message} {(ex.InnerException != null ? ex.InnerException.Message : string.Empty)}";
                throw new XmlSchemaValidationException(errorMessage);
            }
        }

        internal static void AddSchemaFromResource(XmlReaderSettings settings, string resourceName)
        {
            var resources = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            var fullyQualifiedResourceName = resources.Single(name => name.EndsWith(resourceName));
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(fullyQualifiedResourceName))
            {
                using (var xmlReader = XmlReader.Create(stream))
                {
                    settings.Schemas.Add(null, xmlReader);
                }
            }
        }

        private static void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            _schemaValidationError = args.Message;
        }
    }
}