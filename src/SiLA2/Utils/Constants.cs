namespace SiLA2.Server.Utils
{
    public static class Constants
    {
        public const int OBSERVABLECOMMANDQUEUE_PROCESSING_CLOCK_IN_MILLISECONDS = 100;
        public const int EXECUTIONINFO_DELAY_IN_MILLISECONDS = 50;
        public const int INTERMEDIATERESPONSE_DELAY_IN_MILLISECONDS = 100;
        public const int SERVER_INITIATED_MSG_QUEUE_DELAY = 200;
        public const string FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX = "\\b[a-z][a-z0-9\\.]{0,254}/[a-z][a-z0-9\\.]{0,254}/[A-Z][a-zA-Z0-9]*/v\\d+\\b";
        public const string SILA_ORG_SILASTANDARD = "Sila2.Org.Silastandard";
    }
}