using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;

namespace SiLA2.Commands
{
    public class ObservableCommandManager<TParameter, TResponse> : IDisposable, IObservableCommandManager<TParameter, TResponse>
    {
        private readonly ConcurrentDictionary<Guid, ObservableCommandWrapper<TParameter, TResponse>> _commands = new ConcurrentDictionary<Guid, ObservableCommandWrapper<TParameter, TResponse>>();
        private readonly ConcurrentQueue<ObservableCommandWrapper<TParameter, TResponse>> _commandQueue = new ConcurrentQueue<ObservableCommandWrapper<TParameter, TResponse>>();
        private CancellationTokenSource _commandQueueCts = new CancellationTokenSource();
        private readonly ILogger<ObservableCommandManager<TParameter, TResponse>> _logger;
        private readonly ILoggerFactory _loggerFactory;
        private bool _disposedValue;

        /// <summary>
        /// The the duration during which a Command Execution UUID is valid.
        /// </summary>
        public TimeSpan LifetimeOfExecution { get; } = TimeSpan.FromSeconds(60);

        public ObservableCommandManager(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
            _logger = _loggerFactory.CreateLogger<ObservableCommandManager<TParameter, TResponse>>();

            Task.Run(() => RunCommandQueue(), _commandQueueCts.Token);

            LifetimeOfExecution = TimeSpan.FromSeconds(configuration.GetValue<int>("CommandLifetimeInSeconds"));

            if (LifetimeOfExecution <= TimeSpan.Zero)
            {
                throw new InvalidOperationException(
                    $"Lifetime of execution must be greater than 0 but {LifetimeOfExecution} was specified");
            }
        }

        /// <summary>
        /// Add an observable command execution wrapped as a task to the manager command list
        /// 
        /// Note: only use this method if progress and remaining execution time shall be updated
        /// within the command execution and propagated back out to the ExecutionInfo stream
        /// 
        /// </summary>
        /// <param name="parameter">The parameter of the observable command</param>
        /// <param name="func">Function reference which will be invoked internally with parameters</param>
        /// <param name="executionDelay"></param>
        /// <returns>Observable command wrapper as a task</returns>
        public Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(TParameter parameter, Func<IProgress<ExecutionInfo>, TParameter, CancellationToken, TResponse> func, TimeSpan executionDelay)
        {
            return AddCommand(new ObservableCommandWrapper<TParameter, TResponse>(parameter, LifetimeOfExecution, func, _loggerFactory, executionDelay));
        }

        /// <summary>
        /// Add an observable command execution wrapped as a task to the manager command list
        /// 
        /// Note: only use this method if progress and remaining execution time shall be updated
        /// within the command execution and propagated back out to the ExecutionInfo stream
        /// 
        /// </summary>
        /// <param name="parameter">The parameter of the observable command</param>
        /// <param name="func">Function reference which will be invoked internally with parameters</param>
        /// <param name="executionDelay"></param>
        /// <returns>Observable command wrapper as a task</returns>
        public Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(TParameter parameter, Func<IProgress<ExecutionInfo>, TParameter, Guid, CancellationToken, TResponse> func, TimeSpan executionDelay)
        {
            return AddCommand(new ObservableCommandWrapper<TParameter, TResponse>(parameter, LifetimeOfExecution, func, _loggerFactory, executionDelay));
        }

        /// <summary>
        /// Gets the observable command wrapper of a given command execution UUID from an Info stream
        /// </summary>
        /// <param name="commandExecutionUuid"></param>
        /// <returns></returns>
        public ObservableCommandWrapper<TParameter, TResponse> GetCommand(CommandExecutionUUID commandExecutionUuid)
        {
            if (Guid.TryParse(commandExecutionUuid.Value, out Guid commandId))
            {
                return GetCommand(commandId);
            }
            var message = $"CommandExecutionUUID '{commandExecutionUuid}' is not a valid GUID !";
            _logger.LogWarning(message);
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid, message));
            return null;
        }

        /// <summary>
        /// Registers response stream for command execution info updates for a given command UUID
        /// </summary>
        /// <param name="cmdExecId">Request containing command ID to register</param>
        /// <param name="responseStream">client response stream to register command execution info updates to</param>
        /// <param name="cancellationToken">cancellation token from open client connection</param>
        /// <returns></returns>
        public async Task RegisterForInfo(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, CancellationToken cancellationToken)
        {
            // create own CancellationTokenSource to be used by the server
            var serverSideCancellationTokenSource = new CancellationTokenSource();

            var command = GetCommand(cmdExecId);
            await command.AddInfoObserver(responseStream, cancellationToken, serverSideCancellationTokenSource);
            _logger.LogDebug($"Added Info subscription of command execution {cmdExecId}");

            // if command has already been finished return immediately
            if (GetCommand(cmdExecId).IsCompleted)
            {
                return;
            }
            
            try
            {
                while (!cancellationToken.IsCancellationRequested && !serverSideCancellationTokenSource.Token.IsCancellationRequested)
                {
                    GetCommand(cmdExecId);
                    await Task.Delay(Constants.EXECUTIONINFO_DELAY_IN_MILLISECONDS, cancellationToken);
                }
            }
            catch (TaskCanceledException)
            {
                _logger.LogDebug($"Cancelled command: {cmdExecId.Value}");
            }
        }

        public async Task ProcessIntermediateResponses<TIntermediateResponse>(CommandExecutionUUID cmdExecId, IServerStreamWriter<TIntermediateResponse> responseStream, Action<TParameter, CancellationToken> intermediateResponseHandler, CancellationToken token)
        {
            var command = GetCommand(cmdExecId);
            await command.ProcessIntermediateResponses(responseStream, intermediateResponseHandler, token);
        }

        /// <summary>
        /// Cleans up all observable command wrappers held by the manager. Tasks will be cancelled.
        /// </summary>
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _commandQueueCts.Cancel();
                }

                // TODO: Nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalizer �berschreiben
                // TODO: Gro�e Felder auf NULL setzen
                _disposedValue = true;
            }
        }

        private async Task RunCommandQueue()
        {
            while (!_commandQueueCts.IsCancellationRequested)
            {
                try
                {
                    if (_commandQueue.TryDequeue(out ObservableCommandWrapper<TParameter, TResponse> command))
                    {
                        if (command.EarliestExecutionStart - DateTime.Now > TimeSpan.Zero)
                        {
                            await Task.Delay((int)(command.EarliestExecutionStart - DateTime.Now).TotalMilliseconds);
                        }
                        await command.Execute();
                    }
                    await Task.Delay(Constants.OBSERVABLECOMMANDQUEUE_PROCESSING_CLOCK_IN_MILLISECONDS);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Error processing ObservableCommandQueue. {ex}");
                }
            }
        }

        private ObservableCommandWrapper<TParameter, TResponse> GetCommand(Guid commandId)
        {
            if (!_commands.TryGetValue(commandId, out var command))
            {
                var message = $"No such command with ID {commandId} was found";
                _logger.LogWarning(message);
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(
                    FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid,
                    message));
            }

            return command;
        }

        private Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(ObservableCommandWrapper<TParameter, TResponse> command)
        {
            if (_commands.TryAdd(command.CommandExecutionUuid, command))
            {
                _logger.LogDebug($"Added command: {command.CommandExecutionUuid}");
                _commandQueue.Enqueue(command);
                command.CommandExpired += (s, e) => RemoveCommand(e.CommandId, true); ;
                return Task.FromResult(command);
            }
            else
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(
                    FrameworkError.Types.ErrorType.CommandExecutionNotAccepted,
                    $"Command with Id {command.CommandExecutionUuid} could not be added!"));
                throw new InvalidOperationException();
            }
        }

        private void RemoveCommand(Guid commandId, bool isExpired)
        {
            _logger.LogInformation($"Removing {(isExpired ? "expired " : string.Empty)}command: {commandId}");

            if (_commands.TryRemove(commandId, out var command))
            {
                command.Dispose();
            }
            else
            {
                _logger.LogWarning($"Could not remove command with ID {commandId}");
            }
        }
    }
}
