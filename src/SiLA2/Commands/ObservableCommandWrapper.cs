using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;

namespace SiLA2.Commands
{
    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="TParameter"></typeparam>
    /// <typeparam name="TResponse"></typeparam>
    ///
    // TODO: Handle response stream cleanup once closed, think about this!
    //       When client closes info stream, when lifetimeExecutionUUID expires, etc.. 
    public sealed class ObservableCommandWrapper<TParameter, TResponse> : IDisposable
    {
        private readonly Task<TResponse> _task;
        private readonly HashSet<Tuple<IServerStreamWriter<ExecutionInfo>, CancellationToken, CancellationTokenSource>> _infoResponseTuples = new HashSet<Tuple<IServerStreamWriter<ExecutionInfo>, CancellationToken, CancellationTokenSource>>();
        private readonly ExecutionInfo _executionInfo;
        private readonly Progress<ExecutionInfo> _progress = new Progress<ExecutionInfo>();
        private readonly CancellationTokenSource _taskTokenSource = new CancellationTokenSource();
        private readonly object _expirationTimeLock = new object(); //TODO: is this lock even necessary?
        private readonly object _executionInfoLock = new object();
        private readonly ILogger<ObservableCommandWrapper<TParameter, TResponse>> _logger;
        private readonly TParameter _parameter;
        private SiLAError _commandError;
        private DateTime _expirationTime;

        private ExecutionInfo SiLAExecutionInfo
        {
            get
            {
                lock (_executionInfoLock)
                {
                    var executionInfo = new ExecutionInfo
                    {
                        CommandStatus = _executionInfo.CommandStatus,
                        ProgressInfo = _executionInfo.ProgressInfo ?? new Real(),
                        EstimatedRemainingTime = _executionInfo.EstimatedRemainingTime ?? new Duration(),
                        UpdatedLifetimeOfExecution = LifetimeOfExecution
                    };
                    return executionInfo;
                }
            }
        }

        private Duration LifetimeOfExecution
        {
            get
            {
                lock (_expirationTimeLock)
                {
                    var lifetimeOfExecution = _expirationTime - DateTime.Now;
                    return new Duration
                    {
                        Seconds = lifetimeOfExecution.Seconds,
                        Nanos = lifetimeOfExecution.Milliseconds * 10000
                    };
                }
            }
        }

        public bool IsCompleted => _executionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully || _executionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError;

        /// <summary>
        /// Command Execution Identifier
        /// </summary>
        public Guid CommandExecutionUuid { get; }

        /// <summary>
        /// Query the result of the executed command, throws a SiLA error in the form of a <see cref="RpcException"/>
        /// if the result is requested when the command has not completed.
        /// </summary>
        /// <returns>The result of the executed Func</returns>
        public TResponse Result
        {
            get
            {
                if (!IsCompleted)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(
                        FrameworkError.Types.ErrorType.CommandExecutionNotFinished,
                        "The command execution has not been finished yet"));
                }

                if (_commandError != null)
                {
                    ErrorHandling.RaiseSiLAError(_commandError);
                }

                return _task.Result;
            }
        }

        /// <summary>
        /// Command confirmation including command execution UUID and lifetime of execution 
        /// </summary>
        public CommandConfirmation Confirmation
        {
            get
            {
                return new CommandConfirmation
                {
                    CommandExecutionUUID = new CommandExecutionUUID { Value = $"{CommandExecutionUuid}" },
                    LifetimeOfExecution = LifetimeOfExecution
                };
            }
        }

        /// <summary>
        /// If execution delay is specified, it contains a time in the future until which the command execution can not be started
        /// </summary>
        public DateTime EarliestExecutionStart { get; }

        /// <summary>
        /// Event used to listen when a command has expired
        /// </summary>
        public event EventHandler<CommandExpiredArgs> CommandExpired;

        /// <summary>
        /// Constructs an observable command wrapper which manages the execution state of a specified task/func.
        /// Additionally, handles state changes to registered client streams and additional logic
        /// </summary>
        /// <param name="parameter">Input parameter of Observable Command request</param>
        /// <param name="lifetimeOfExecution">The lifetime of execution that the command is guaranteed to exist for</param>
        /// <param name="func">A wrapped Func to be passed as input which encapsulates the execution of the observable command</param>
        /// <param name="loggerFactory"></param>
        /// <param name="executionDelay"></param>
        public ObservableCommandWrapper(TParameter parameter, TimeSpan lifetimeOfExecution, Func<IProgress<ExecutionInfo>, TParameter, CancellationToken, TResponse> func, ILoggerFactory loggerFactory, TimeSpan executionDelay)
        {
            _parameter = parameter;
            CommandExecutionUuid = Guid.NewGuid();
            _logger = loggerFactory.CreateLogger<ObservableCommandWrapper<TParameter, TResponse>>();
            RegisterExecutionLifetime(lifetimeOfExecution);
            _task = new Task<TResponse>(() => func.Invoke(_progress, parameter, _taskTokenSource.Token));
            EarliestExecutionStart = DateTime.Now + executionDelay;
            lock (_executionInfoLock)
            {
                _executionInfo = new ExecutionInfo { CommandStatus = ExecutionInfo.Types.CommandStatus.Waiting };
            }
        }

        /// <summary>
        /// Constructs an observable command wrapper which manages the execution state of a specified task/func.
        /// Additionally, handles state changes to registered client streams and additional logic
        /// </summary>
        /// <param name="parameter">Input parameter of Observable Command request</param>
        /// <param name="lifetimeOfExecution">The lifetime of execution that the command is guaranteed to exist fort</param>
        /// <param name="func">A wrapped Func to be passed as input which encapsulates the execution of the observable command</param>
        /// <param name="loggerFactory"></param>
        /// <param name="executionDelay"></param>
        public ObservableCommandWrapper(TParameter parameter, TimeSpan lifetimeOfExecution, Func<IProgress<ExecutionInfo>, TParameter, Guid, CancellationToken, TResponse> func, ILoggerFactory loggerFactory, TimeSpan executionDelay)
        {
            _parameter = parameter;
            CommandExecutionUuid = Guid.NewGuid();
            _logger = loggerFactory.CreateLogger<ObservableCommandWrapper<TParameter, TResponse>>();
            RegisterExecutionLifetime(lifetimeOfExecution);
            _task = new Task<TResponse>(() => func.Invoke(_progress, parameter, CommandExecutionUuid, _taskTokenSource.Token));
            EarliestExecutionStart = DateTime.Now + executionDelay;
            lock (_executionInfoLock)
            {
                _executionInfo = new ExecutionInfo { CommandStatus = ExecutionInfo.Types.CommandStatus.Waiting };
            }
        }

        /// <summary>
        /// Wraps the command with appropriate state notifications
        /// </summary>
        /// TODO: Handle progress and remaining time
        /// <returns></returns>
        public Task Execute()
        {
            _logger.LogDebug($"Starting command: {CommandExecutionUuid}");
            var notificationTask = SetExecutionState(ExecutionInfo.Types.CommandStatus.Running);
            _task.ContinueWith(HandleCommandError, TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.ExecuteSynchronously);
            _task.ContinueWith(HandleCommandSuccess, TaskContinuationOptions.OnlyOnRanToCompletion | TaskContinuationOptions.ExecuteSynchronously);
            _task.Start();
            return notificationTask;
        }

        /// <summary>
        /// Adds a registered client response stream and cancellation token to receive updates of the ExecutionInfo of
        /// the executing command/task
        /// </summary>
        /// <param name="responseStream">gRPC response stream connected to a client</param>
        /// <param name="clientCancellationToken">client cancellation token of the connection</param>
        /// <param name="serverSideCancellationTokenSource">the cancellation token source to be used by the server</param>
        /// <returns>Async task of writing execution info to the response stream</returns>
        public Task AddInfoObserver(IServerStreamWriter<ExecutionInfo> responseStream, CancellationToken clientCancellationToken, CancellationTokenSource serverSideCancellationTokenSource)
        {
            var responseTuple = new Tuple<IServerStreamWriter<ExecutionInfo>, CancellationToken, CancellationTokenSource>(responseStream, clientCancellationToken, serverSideCancellationTokenSource);
            _infoResponseTuples.Add(responseTuple);
            clientCancellationToken.Register(RemoveResponse(responseTuple));
            serverSideCancellationTokenSource.Token.Register(RemoveResponse(responseTuple));
            _logger.LogDebug($"Added {responseTuple.Item1} stream: {responseTuple.Item1.GetHashCode()}");
            _progress.ProgressChanged += ProgressOnProgressChanged;
            return responseStream.WriteAsync(SiLAExecutionInfo);
        }

        public async Task ProcessIntermediateResponses<TIntermediateResponse>(IServerStreamWriter<TIntermediateResponse> responseStream, Action<TParameter, CancellationToken> intermediateResponseHandler, CancellationToken token)
        {
            await Task.Run(() => { intermediateResponseHandler.Invoke(_parameter, token); });
        }

        /// <summary>
        /// Removes the response tuple when the response stream is not needed anymore
        /// </summary>
        /// <param name="responseTuple"></param>
        /// <returns></returns>
        private Action RemoveResponse(Tuple<IServerStreamWriter<ExecutionInfo>, CancellationToken, CancellationTokenSource> responseTuple)
        {
            return () =>
            {
                _logger.LogDebug($"Removing {responseTuple.Item1} stream: {responseTuple.Item1.GetHashCode()}");
                if (_infoResponseTuples.TryGetValue(responseTuple, out Tuple<IServerStreamWriter<ExecutionInfo>, CancellationToken, CancellationTokenSource> foundTuple))
                {
                    var removed = _infoResponseTuples.Remove(responseTuple);
                    if (!removed)
                        throw new InvalidOperationException($"Failed to remove {responseTuple.Item2.GetHashCode()}");
                }
            };
        }

        /// <summary>
        /// Cleans up the resources of the observable command wrapper. Verifies that the underlying
        /// command execution has terminated and terminates it if not already.
        /// </summary>
        public void Dispose()
        {
            _logger.LogInformation($"Cleaning up command: {CommandExecutionUuid}...");
            if (!_taskTokenSource.IsCancellationRequested)
            {
                _taskTokenSource.Cancel();
            }

            try
            {
                _task.Wait();
            }
            finally
            {
                _infoResponseTuples.Clear();
                _logger.LogInformation("Cleaned");
            }
        }

        private async void ProgressOnProgressChanged(object sender, ExecutionInfo e)
        {
            lock (_executionInfoLock)
            {
                //TODO: This is very delicate. Race condition can happen if this check is not done.
                if (_executionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully ||
                    _executionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError ||
                    _executionInfo?.ProgressInfo?.Value == 1)
                {
                    return;
                }
                _executionInfo.ProgressInfo = e.ProgressInfo;
                _executionInfo.EstimatedRemainingTime = e.EstimatedRemainingTime;
            }
            await NotifySubscribers();
        }

        /// <summary>
        /// Registers a timer for lifetime of execution, extends it if the Wrapper task has not been completed yet
        /// </summary>
        /// <param name="lifetimeOfExecution">The lifetime of execution of the observable command</param>
        private void RegisterExecutionLifetime(TimeSpan lifetimeOfExecution)
        {
            _expirationTime = DateTime.Now + lifetimeOfExecution;
            _logger.LogDebug($"Extending lifetime of execution: {lifetimeOfExecution} Current Time: {DateTime.Now.Second}, Expiration time: {_expirationTime.Second}");

            Task.Delay(lifetimeOfExecution).ContinueWith(async task =>
            {
                if (IsCompleted)
                {
                    _logger.LogDebug($"Command {CommandExecutionUuid} expired!");
                    CommandExpired?.Invoke(this, new CommandExpiredArgs(CommandExecutionUuid));
                }
                else
                {
                    // Extend the lifetime of execution
                    RegisterExecutionLifetime(lifetimeOfExecution);
                    await NotifySubscribers();
                }
            }, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        private Task HandleCommandSuccess(Task<TResponse> task)
        {
            return SetExecutionState(ExecutionInfo.Types.CommandStatus.FinishedSuccessfully);
        }

        private Task HandleCommandError(Task<TResponse> task)
        {
            _logger.LogWarning($"Error occurred during observable command {CommandExecutionUuid} task execution {task.Exception}");
            if (task.Exception?.InnerException is RpcException exception)
            {
                _commandError = ErrorHandling.RetrieveSiLAError(exception);
            }
            if (_commandError == null)
            {
                string errorMessage;
                if (task.Exception?.InnerException != null)
                {
                    if (task.Exception?.InnerException is OperationCanceledException)
                    {
                        errorMessage = "Observable command has been cancelled";
                    }
                    else
                    {
                        errorMessage = "An unknown error occurred: " + ErrorHandling.FlattenExceptionsMessageToString(task.Exception);
                    }
                }
                else
                {
                    errorMessage = task.Exception?.Message.Length > 0 ? task.Exception.Message : $"An unknown error occurred ({task.Exception?.GetType().Name})";
                }
                _commandError = ErrorHandling.CreateUndefinedExecutionError(errorMessage);
            }

            return SetExecutionState(ExecutionInfo.Types.CommandStatus.FinishedWithError);
        }

        private Task SetExecutionState(ExecutionInfo.Types.CommandStatus status)
        {
            lock (_executionInfoLock)
            {
                if (status != _executionInfo.CommandStatus)
                {
                    _executionInfo.CommandStatus = status;
                    return NotifySubscribers();
                }
            }
            return Task.CompletedTask;
        }

        private Task NotifySubscribers()
        {
            if (_taskTokenSource.IsCancellationRequested)
                return Task.CompletedTask;

            _logger.LogDebug($"Command execution info: {SiLAExecutionInfo}");
            var tasks = new List<Task>();
            lock (_executionInfoLock)
            {
                foreach (var (serverStreamWriter, infoStreamCancellationToken, serverSideCancellationTokenSource) in _infoResponseTuples)
                {
                    if (infoStreamCancellationToken.IsCancellationRequested)
                    {
                        continue;
                    }

                    if (serverSideCancellationTokenSource.Token.IsCancellationRequested)
                    {
                        continue;
                    }

                    tasks.Add(Task.Run(() =>
                    {
                        // send execution info message
                        serverStreamWriter.WriteAsync(SiLAExecutionInfo).Wait();

                        // cancel subscription if status is 'Finished...'
                        if (IsCompleted)
                        {
                            serverSideCancellationTokenSource.Cancel();
                            _logger.LogDebug($"Canceled Info subscription of command execution {CommandExecutionUuid}");
                        }
                    }));

                }
            }
            return Task.WhenAll(tasks);
        }
    }
}