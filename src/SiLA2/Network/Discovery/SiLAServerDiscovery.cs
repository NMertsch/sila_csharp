using System;
using System.Collections.Generic;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using Microsoft.Extensions.Logging;
using SiLA2.Network.Discovery.mDNS;
using Grpc.Net.Client;
using System.Threading.Tasks;
using SiLA2.Server;
using SiLA2.Utils.gRPC;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Security.Cryptography;
using SiLA2.Utils.Network;
using SiLA2.Utils.Config;

namespace SiLA2.Network.Discovery
{
    /// <summary>
    /// Exception to be used if Server is not found during discovery
    /// </summary>
    public class ServerNotFoundException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">reason why server was not found</param>
        /// <returns></returns>
        public ServerNotFoundException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Utility class to be used to discover and find SiLA Servers using the 
    /// ServiceFinder class.
    /// </summary>
    public class SiLAServerDiscovery : ISiLAServerDiscovery
    {
        private readonly IServiceFinder _serviceFinder;
        private readonly IGrpcChannelProvider _grpcChannelProvider;
        private readonly IServerDataProvider _serverDataProvider;
        private readonly IClientConfig _clientConfig;
        private readonly ILogger<SiLAServerDiscovery> _logger;

        public SiLAServerDiscovery(IServiceFinder serviceFinder, IGrpcChannelProvider grpcChannelProvider, IServerDataProvider serverDataProvider, IClientConfig clientConfig, ILogger<SiLAServerDiscovery> logger)
        {
            _serviceFinder = serviceFinder;
            _grpcChannelProvider = grpcChannelProvider;
            _serverDataProvider = serverDataProvider;
            _clientConfig = clientConfig;
            _logger = logger;
        }

        /// <summary>
        /// Query a list of SiLA2 servers with valid SiLAService
        /// </summary>
        /// <param name="timeout">duration of Query period</param>
        /// <returns>"List of valid SiLA2 servers</returns>
        public async Task<IEnumerable<ServerData>> GetServers(int timeout = 3000)
        {
            _logger.LogInformation($"Searching SiLA2 Servers for {timeout} ms...");
            var foundServers = await _serviceFinder.GetConnections(_clientConfig.DiscoveryServiceName, _clientConfig.NetworkInterface, timeout);

            var serverDatas = new List<ServerData>();
            foreach (var server in foundServers)
            {
                try
                {
                    var serverdata = await _serverDataProvider.GetServerData(server.Address, server.Port, false, server.SilaCA);
                    serverDatas.Add(serverdata);
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"Invalid server found at channel: {server}{Environment.NewLine}{ex}");
                }
            }

            return serverDatas;
        }

        

        /// <summary>
        /// Load all server properties 
        /// </summary>
        public ServerData GetServerData(GrpcChannel channel)
        {
            var silaService = new SiLAService.SiLAServiceClient(channel);
            var config = GetServerConfig(silaService);
            var info = GetServerInfo(silaService);
            var server = new ServerData(config, info);
            return server;
        }

        private ServerConfig GetServerConfig(SiLAService.SiLAServiceClient silaService)
        {
            var name = silaService.Get_ServerName(new Get_ServerName_Parameters()).ServerName.Value;
            var uuid = Guid.Parse(silaService.Get_ServerUUID(new Get_ServerUUID_Parameters()).ServerUUID.Value);
            var config = new ServerConfig(name, uuid);
            return config;
        }

        private ServerInformation GetServerInfo(SiLAService.SiLAServiceClient silaService)
        {
            var type = silaService.Get_ServerType(new Get_ServerType_Parameters()).ServerType.Value;
            var description = silaService.Get_ServerDescription(new Get_ServerDescription_Parameters()).ServerDescription.Value;
            var vendorUri = silaService.Get_ServerVendorURL(new Get_ServerVendorURL_Parameters()).ServerVendorURL.Value;
            var version = silaService.Get_ServerVersion(new Get_ServerVersion_Parameters()).ServerVersion.Value;
            var info = new ServerInformation(type, description, vendorUri, version);
            return info;
        }

    }
}