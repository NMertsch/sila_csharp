﻿using Grpc.Net.Client;
using SiLA2.Server;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Network.Discovery
{
    public interface ISiLAServerDiscovery
    {
        ServerData GetServerData(GrpcChannel channel);
        Task<IEnumerable<ServerData>> GetServers(int timeout = 3000);
    }
}