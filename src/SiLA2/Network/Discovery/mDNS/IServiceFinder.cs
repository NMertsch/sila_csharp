﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SiLA2.Network.Discovery.mDNS
{
    public interface IServiceFinder
    {
        event Action<ConnectionInfo> ConnectionReceivedHandler;
        Task<IEnumerable<ConnectionInfo>> GetConnections(string serviceName, string networkInterface, int searchDuration = 10000);
        Task GetConnectionsContinuously(string serviceName, CancellationToken ct, string networkInterface = null, int searchIntervalInMilliseconds = 2000);
    }
}