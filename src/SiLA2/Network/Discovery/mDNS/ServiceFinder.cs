﻿using Makaretu.Dns;
using Microsoft.Extensions.Logging;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SiLA2.Network.Discovery.mDNS
{
    public class ServiceFinder : IServiceFinder
    {
        private readonly INetworkService _networking;
        private readonly ILogger<ServiceFinder> _logger;

        public event Action<ConnectionInfo> ConnectionReceivedHandler;

        public ServiceFinder(INetworkService networking, ILogger<ServiceFinder> logger)
        {
            _networking = networking;
            _logger = logger;
        }

        public async Task<IEnumerable<ConnectionInfo>> GetConnections(string serviceName, string networkInterface, int searchDuration = 3000)
        {
            var connections = new List<ConnectionInfo>();

            Func<IEnumerable<NetworkInterface>, IEnumerable<NetworkInterface>> filterFunc = null;
            if(!string.IsNullOrEmpty(networkInterface))
            {
                var nic = _networking.GetNetworkInterface(networkInterface);
                filterFunc = _networking.CreateFilterFunc(nic);
            }

            using (var mdns = new MulticastService(filterFunc))
            {
                // attach the event handler for processing responses.
                mdns.AnswerReceived += (s, e) =>
                {
                    foreach (var item in e.Message.Answers)
                    {
                        _logger.LogDebug($"{item}");
                    }
                    
                    // The first Answer will be a PTRRecord
                    var pointers = e.Message.Answers.OfType<PTRRecord>().Where(p => p.DomainName.ToString().Contains(serviceName));
                    foreach (var pointer in pointers)
                    {
                        // Ask for the service instance details
                        mdns.SendQuery(pointer.DomainName, type: DnsType.SRV);
                    }

                    // The second Answer will be a SRVRecord (also contain A and AAAA records)
                    var servers = e.Message.Answers.OfType<SRVRecord>().Where(p => p.ToString().Contains(serviceName));
                    foreach (var server in servers)
                    {
                        var A_record = e.Message.AdditionalRecords.OfType<ARecord>().FirstOrDefault();
                        if (A_record != null)
                        {
                            AddConnectionInfo(connections, server, A_record, e.Message.AdditionalRecords);
                        }

                        var AAAA_record = e.Message.AdditionalRecords.OfType<AAAARecord>().FirstOrDefault();
                        if (AAAA_record != null)
                        {
                            AddConnectionInfo(connections, server, AAAA_record, e.Message.AdditionalRecords);
                        }
                    }
                };

                mdns.Start();

                // Ask for PTRRecords (the devices will only respond to SRV that are directed exactly to them)
                var sn = string.Join(".", serviceName, "local");
                mdns.SendQuery(sn, type: DnsType.PTR);
                await Task.Delay(searchDuration);
                mdns.Stop();

                return connections;
            }
        }

        public async Task GetConnectionsContinuously(string serviceName, CancellationToken ct, string networkInterface = null, int searchIntervalInMilliseconds = 2000)
        {
            Func<IEnumerable<NetworkInterface>, IEnumerable<NetworkInterface>> filterFunc = x => null;
            if (!string.IsNullOrEmpty(networkInterface))
            {
                var nic = _networking.GetNetworkInterface(networkInterface);
                filterFunc = _networking.CreateFilterFunc(nic);
            }           

            using (var mdns = new MulticastService(filterFunc))
            {
                // attach the event handler for processing responses.
                mdns.AnswerReceived += (s, e) =>
                {
                    foreach (var item in e.Message.Answers)
                    {
                        _logger.LogDebug($"{item}");
                    }

                    // The first Answer will be a PTRRecord
                    var pointers = e.Message.Answers.OfType<PTRRecord>().Where(p => p.DomainName.ToString().Contains(serviceName));
                    foreach (var pointer in pointers)
                    {
                        // Ask for the service instance details
                        mdns.SendQuery(pointer.DomainName, type: DnsType.SRV);
                    }

                    // The second Answer will be a SRVRecord (also contain A and AAAA records)
                    var servers = e.Message.Answers.OfType<SRVRecord>().Where(p => p.ToString().Contains(serviceName));
                    foreach (var server in servers)
                    {
                        var A_record = e.Message.AdditionalRecords.OfType<ARecord>().FirstOrDefault();
                        if (A_record != null)
                        {
                            var connectionInfo = GetConnectionInfo(server, A_record, e.Message.AdditionalRecords);
                            ConnectionReceivedHandler?.Invoke(connectionInfo);
                        }

                        var AAAA_record = e.Message.AdditionalRecords.OfType<AAAARecord>().FirstOrDefault();
                        if (AAAA_record != null)
                        {
                            var connectionInfo = GetConnectionInfo(server, AAAA_record, e.Message.AdditionalRecords);
                            ConnectionReceivedHandler?.Invoke(connectionInfo);
                        }
                    }
                };

                mdns.Start();

                // Ask for PTRRecords (the devices will only respond to SRV that are directed exactly to them)
                var sn = string.Join(".", serviceName, "local");
                
                while (!ct.IsCancellationRequested)
                {
                    mdns.SendQuery(sn, type: DnsType.PTR);
                    await Task.Delay(searchIntervalInMilliseconds);
                }

                mdns.Stop();
            }
        }

        private void AddConnectionInfo<T>(List<ConnectionInfo> connections, SRVRecord server, T addressRecord, List<ResourceRecord> resourceRecords) where T : AddressRecord
        {
            var connectionInfo = GetConnectionInfo(server, addressRecord, resourceRecords);
            connections.Add(connectionInfo);
        }

        private ConnectionInfo GetConnectionInfo<T>(SRVRecord server, T addressRecord, List<ResourceRecord> resourceRecords) where T : AddressRecord
        {
            _logger.LogTrace($"Discovered Service at {addressRecord.Address}:{server.Port}");
            var sb = new StringBuilder();
            var certRecord = resourceRecords.SingleOrDefault(x => x.ToString().IndexOf(CertificateRepository.BEGIN_CERTIFICATE) > -1);
            if (certRecord != null)
            {
                var txtRecords = ((TXTRecord)certRecord).Strings;
                var first = txtRecords.FindIndex(x => x.IndexOf(CertificateRepository.BEGIN_CERTIFICATE) > -1);
                var last = txtRecords.FindIndex(x => x.IndexOf(CertificateRepository.END_CERTIFICATE) > -1);
                for (int i = first; i <= last; i++)
                {
                    sb.AppendLine(txtRecords[i]);
                }
            }
            return new ConnectionInfo(addressRecord.Address.ToString(), server.Port, sb.ToString(), addressRecord.Type);
        }
    }
}
