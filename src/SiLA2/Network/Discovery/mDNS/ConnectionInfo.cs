﻿
using Makaretu.Dns;
using System;
using System.Collections.Generic;

namespace SiLA2.Network.Discovery.mDNS
{
    public class ConnectionInfo : IEquatable<ConnectionInfo>
    {
        public string Address { get; }
        public int Port { get; }
        public string SilaCA { get; }
        public DnsType Type { get; }

        public ConnectionInfo(string address, int port, string silaCa, DnsType type)
        {
            Address = address;
            Port = port;
            SilaCA = silaCa;
            Type = type;
        }

        public override string ToString()
        {
            return $"{Address}:{Port}";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ConnectionInfo);
        }

        public bool Equals(ConnectionInfo other)
        {
            return other is not null &&
                   Address == other.Address &&
                   Port == other.Port &&
                   SilaCA == other.SilaCA &&
                   Type == other.Type;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Address, Port, SilaCA, Type);
        }

        public static bool operator ==(ConnectionInfo left, ConnectionInfo right)
        {
            return EqualityComparer<ConnectionInfo>.Default.Equals(left, right);
        }

        public static bool operator !=(ConnectionInfo left, ConnectionInfo right)
        {
            return !(left == right);
        }
    }
}
