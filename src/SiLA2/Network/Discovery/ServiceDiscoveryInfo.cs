﻿using SiLA2.Utils.Network;
using System;

namespace SiLA2.Network.Discovery
{
    public class ServiceDiscoveryInfo
    {
        public string ServerInstanceName { get; }
        public ushort Port { get; }
        public string NetworkInterface { get; }
        public string DiscoveryServiceName { get; }

        public ServiceDiscoveryInfo(IServerConfig serverConfig)
        {
            ServerInstanceName = serverConfig.Uuid.ToString();
            Port = Convert.ToUInt16(serverConfig.Port);
            NetworkInterface = serverConfig.NetworkInterface;
            DiscoveryServiceName = serverConfig.DiscoveryServiceName;
        }
    }
}


