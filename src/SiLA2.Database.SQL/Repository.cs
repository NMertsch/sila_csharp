﻿using Microsoft.EntityFrameworkCore;
using SiLA2.Database.SQL.Domain;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.Database.SQL
{

    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly IDbContext _context;
        private DbSet<T> _entities;

        public Repository(IDbContext context)
        {
            _context = context;
        }

        public virtual async Task<T> GetById(object id)
        {
            return await Entities.FindAsync(id);
        }

        public async Task<TransactionResultMessage> Insert(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException(nameof(entity));
                }

                using (var transAction = _context.Database.BeginTransaction())
                {
                    Entities.Add(entity);
                    _context.SaveChanges();
                    await transAction.CommitAsync();
                    return new TransactionResultMessage(TransactionResult.Success);
                }
            }
            catch (Exception ex)
            {
                return new TransactionResultMessage(TransactionResult.Error, ex.ToString());
            }
        }

        public async Task<TransactionResultMessage> Update(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException(nameof(entity));
                }
                using (var transAction = _context.Database.BeginTransaction())
                {
                    Entities.Update(entity);
                    _context.SaveChanges();
                    await transAction.CommitAsync();
                    return new TransactionResultMessage(TransactionResult.Success);
                }
            }
            catch (Exception ex)
            {
                return new TransactionResultMessage(TransactionResult.Error, ex.ToString());
            }
        }

        public async Task<TransactionResultMessage> Delete(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException(nameof(entity));
                }
                using (var transAction = _context.Database.BeginTransaction())
                {
                    Entities.Remove(entity);
                    _context.SaveChanges(); await transAction.CommitAsync();
                    return new TransactionResultMessage(TransactionResult.Success);
                }
            }
            catch (Exception ex)
            {
                return new TransactionResultMessage(TransactionResult.Error, ex.ToString());
            }
        }

        public virtual IQueryable<T> Table
        {
            get
            {
                return Entities;
            }
        }

        protected DbSet<T> Entities
        {
            get
            {
                if (_entities == null)
                {
                    _entities = _context.Set<T>();
                }
                return _entities;
            }
        }
    }
}