## Quick Start
If you just want to get something working, follow these pages:

* [SiLA Browser Quickstart](docs/introduction/sila-browser-quickstart) to see how a generic SiLA client looks like, and interact with the server
* [SiLA C# Quick Start](https://gitlab.com/SiLA2/sila_csharp/wikis/Quick-Start)
* [SiLA C# Tecan Quick Start](https://gitlab.com/SiLA2/vendors/sila_tecan/-/wikis/quickstart)
* [SiLA Java Quick Start](https://gitlab.com/SiLA2/sila_java/wikis/Quick-Start)
* [SiLA Python Quick Start](https://sila2.gitlab.io/sila_python/tutorials/1_quickstart.html)
