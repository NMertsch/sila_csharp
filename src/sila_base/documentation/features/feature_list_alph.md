# Alphabetic Feature list


[AbsorbanceReaderService v1.0](feature_definitions/com/madisoft/reader/AbsorbanceReaderService-v1_0.sila.xml)

[AuthenticationService v1.0](feature_definitions/org/silastandard/core/AuthenticationService-v1_0.sila.xml)

[AuthorizationConfigurationService v1.0](feature_definitions/org/silastandard/core/AuthorizationConfigurationService-v1_0.sila.xml)

[AuthorizationProviderService v1.0](feature_definitions/org/silastandard/core/AuthorizationProviderService-v1_0.sila.xml)

[AuthorizationService v1.0](feature_definitions/org/silastandard/core/AuthorizationService-v1_0.sila.xml)

[BatteryController v1.0](feature_definitions/ch/unitelabs/robot/BatteryController-v1_0.sila.xml)

[CancelController v1.0](feature_definitions/org/silastandard/core/commands/CancelController-v1_0.sila.xml)

[ConnectionConfigurationService v1.0](feature_definitions/org/silastandard/core/ConnectionConfigurationService-v1_0.sila.xml)

[CoverController v1.0](feature_definitions/org/silastandard/instruments/CoverController-v1_0.sila.xml)

[DeviceCalibrationService v1.0](feature_definitions/ch/unitelabs/robot/DeviceCalibrationService-v1_0.sila.xml)

[ErrorRecoveryService v1.0](feature_definitions/org/silastandard/core/ErrorRecoveryService-v1_0.sila.xml)

[FluorescenceReaderService v1.0](feature_definitions/com/madisoft/reader/FluorescenceReaderService-v1_0.sila.xml)

[GloMaxStatusProvider v1.0](feature_definitions/com/madisoft/reader/ReaderStatusProvider-v1_0.sila.xml)

[GreetingProvider v1.0](feature_definitions/org/silastandard/examples/GreetingProvider-v1_0.sila.xml)

[GripController v1.0](feature_definitions/ch/unitelabs/robot/GripController-v1_0.sila.xml)

[InitializationController v1.0](feature_definitions/ch/unitelabs/core/InitializationController-v1_0.sila.xml)

[LockController v1.0](feature_definitions/org/silastandard/core/LockController-v1_0.sila.xml)

[LuminescenceReaderService v1.0](feature_definitions/com/madisoft/reader/LuminescenceReaderService-v1_0.sila.xml)

[MessagingController v1.0](feature_definitions/de/tuberlin/bioprocess/communication/MessagingController-v1_0.sila.xml)

[ParameterConstraintsProvider v1.0](feature_definitions/org/silastandard/core/commands/ParameterConstraintsProvider-v1_0.sila.xml)

[PauseController v1.0](feature_definitions/org/silastandard/core/commands/PauseController-v1_0.sila.xml)

[PlateCalibrationService v1.0](feature_definitions/ch/unitelabs/robot/PlateCalibrationService-v1_0.sila.xml)

[ProgramController v1.0](feature_definitions/ch/unitelabs/core/ProgramController.sila-v1_0.xml)

[RobotController v1.0](feature_definitions/ch/unitelabs/robot/RobotController.sila-v1_0.xml)

[RobotTeachingService v1.0](feature_definitions/ch/unitelabs/robot/RobotTeachingService-v1_0.sila.xml)

[DeviceInformationProvider v1.0](feature_definitions/de/tuberlin/bioprocess/briefing/DeviceInformationProvider-v1_0.sila.xml)

[SiLAService v1.0](feature_definitions/org/silastandard/core/SiLAService-v1_0.sila.xml)

[SimulationController v1.0](feature_definitions/org/silastandard/core/SimulationController-v1_0.sila.xml)

[SingleConnectionController v1.0](feature_definitions/ch/unitelabs/core/SingleConnectionController-v1_0.sila.xml)

[StateService v1.0](feature_definitions/ch/unitelabs/core/StateService-v1_0.sila.xml)

[StorageService v1.0](feature_definitions/de/tuberlin/bioprocess/storing/StorageService-v1_0.sila.xml)

[TemperatureController v1.0](feature_definitions/org/silastandard/examples/TemperatureController-v1_0.sila.xml)

[WarpdriveService v1.0](feature_definitions/de/diginbio/examples/WarpdriveService-v1_0.sila.xml)
