## Guides

If you want to learn more, about how SiLA 2 works, follow these pages:
* [What is SiLA?](/docs/introduction/what-is-sila)
* [How to implement SiLA](/docs/implementation/how-to-implement-sila)
* [SiLA Browser quickstart](/docs/introduction/sila-browser-quickstart)
* [Troubleshooting](/docs/implementation/troubleshooting)
