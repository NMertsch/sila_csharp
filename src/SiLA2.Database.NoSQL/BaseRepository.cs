﻿using LiteDB;

namespace SiLA2.Database.NoSQL
{
    public abstract class BaseRepository<T> : IBaseRepository<T>
    {
        public ILiteDatabase DB { get; }
        public ILiteCollection<T> Collection { get; }

        protected BaseRepository(ILiteDatabase db)
        {
            DB = db;
            Collection = db.GetCollection<T>();
        }

        public virtual T Create(T entity, out object newId)
        {
            newId = Collection.Insert(entity);
            return Collection.FindById((BsonValue)newId);
        }

        public virtual T Create(T entity)
        {
            return Create(entity, out object _);
        }

        public virtual IEnumerable<T> All()
        {
            return Collection.FindAll();
        }

        public virtual T FindById(object id)
        {
            return Collection.FindById((BsonValue)id);
        }

        public virtual void Update(T entity)
        {
            Collection.Upsert(entity);
        }

        public virtual bool Delete(object id)
        {
            return Collection.Delete((BsonValue)id);
        }
    }
}