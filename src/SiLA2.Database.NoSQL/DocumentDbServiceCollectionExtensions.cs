﻿using LiteDB;
using Microsoft.Extensions.DependencyInjection;

namespace SiLA2.Database.NoSQL
{
    public static class DocumentDbServiceCollectionExtensions
    {
        public static void RegisterDocumentDatabase(this IServiceCollection iocContainer, string path, out ILiteDatabase db)
        {
            db = new LiteDatabase(path);
            iocContainer.AddSingleton(db);
        }

        public static void RegisterDocumentDatabaseTypes<T>(this IServiceCollection iocContainer, ILiteDatabase db)
        {
            iocContainer.AddSingleton(db.GetCollection<T>());
        }
    }
}
