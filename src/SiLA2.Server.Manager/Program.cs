using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server.Manager.Services;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;

namespace SiLA2.Server.Manager
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();
            builder.Services.AddServerSideBlazor();
            builder.Services.AddTransient<INetworkService, NetworkService>(); 
            builder.Services.AddSingleton<IServiceFinder, ServiceFinder>();
            builder.Services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            builder.Services.AddTransient<IServerDataService, ServerDataService>();
            builder.Services.AddSingleton<IConnectionViewModelProvider, ConnectionViewModelProvider>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.MapBlazorHub();
            app.MapFallbackToPage("/_Host");

            app.Run();
        }
    }
}