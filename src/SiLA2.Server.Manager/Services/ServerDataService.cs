﻿using Grpc.Net.Client;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using SiLA2.Server.Manager.Services;
using SiLA2.Utils.Config;
using SiLA2.Utils.gRPC;
using System.Text;
using System.Xml.Serialization;

namespace SiLA2.Server.Manager
{
    public class ServerDataService : IServerDataService
    {
        private readonly IGrpcChannelProvider _grpcChannelProvider;

        public ServerDataService(IGrpcChannelProvider grpcChannelProvider)
        {
            _grpcChannelProvider = grpcChannelProvider;
        }

        public async Task<ServerData> GetServerData(string host, int port, bool acceptAnyServerCertificated = true)
        {
            var channel = await _grpcChannelProvider.GetChannel(host, port, acceptAnyServerCertificated);
            var silaServiceClient = new SiLAService.SiLAServiceClient(channel);
            var config = await GetServerConfig(silaServiceClient, host, port);
            var info = await GetServerInfo(silaServiceClient);
            return new ServerData(config, info);
        }

        public async Task<IDictionary<string, Feature>> GetServerFeatures(string host, int port, bool acceptAnyServerCertificated = true)
        {
            var channel = await _grpcChannelProvider.GetChannel(host, port, acceptAnyServerCertificated);
            var silaServiceClient = new SiLAService.SiLAServiceClient(channel);
            var features = await silaServiceClient.Get_ImplementedFeaturesAsync(new Get_ImplementedFeatures_Parameters());
            IDictionary<string, Feature> featureMap = new Dictionary<string, Feature>();
            foreach(var fd in features.ImplementedFeatures)
            {
                var result = await silaServiceClient.GetFeatureDefinitionAsync(new GetFeatureDefinition_Parameters { FeatureIdentifier = new Sila2.Org.Silastandard.String { Value = fd.Value } });
                using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(result.FeatureDefinition.Value)))
                {
                    var serializer = new XmlSerializer(typeof(Feature));
                    var featureDefinition = (Feature)serializer.Deserialize(ms);
                    featureMap.Add(fd.Value, featureDefinition);
                }
            }
            return featureMap;
        }

        private async Task<ServerConfig> GetServerConfig(SiLAService.SiLAServiceClient silaService, string host, int port)
        {
            var nameResponse = await silaService.Get_ServerNameAsync(new Get_ServerName_Parameters());
            var uuidRsponsse = await silaService.Get_ServerUUIDAsync (new Get_ServerUUID_Parameters());
            return new ServerConfig(nameResponse.ServerName.Value, Guid.Parse(uuidRsponsse.ServerUUID.Value), host, port);
        }

        private async Task<ServerInformation> GetServerInfo(SiLAService.SiLAServiceClient silaService)
        {
            var typeResponse = await silaService.Get_ServerTypeAsync(new Get_ServerType_Parameters());
            var descriptionResponse = await silaService.Get_ServerDescriptionAsync(new Get_ServerDescription_Parameters());
            var vendorUriResponse = await silaService.Get_ServerVendorURLAsync(new Get_ServerVendorURL_Parameters());
            var versionResponse = await silaService.Get_ServerVersionAsync(new Get_ServerVersion_Parameters());
            return new ServerInformation(typeResponse.ServerType.Value, descriptionResponse.ServerDescription.Value, vendorUriResponse.ServerVendorURL.Value, versionResponse.ServerVersion.Value);
        }
    }
}
