﻿namespace SiLA2.Server.Manager.Services
{
    public interface IServerDataService
    {
        Task<ServerData> GetServerData(string host, int port, bool acceptAnyServerCertificated = true);
        Task<IDictionary<string, Feature>> GetServerFeatures(string host, int port, bool acceptAnyServerCertificated = true);
    }
}