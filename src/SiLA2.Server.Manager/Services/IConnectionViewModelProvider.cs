﻿using SiLA2.Server.Manager.Data;
using System.Collections.Concurrent;

namespace SiLA2.Server.Manager.Services
{
    public interface IConnectionViewModelProvider
    {
        ConcurrentDictionary<string, ConnectionViewModel> Servers { get; }
    }
}