﻿using SiLA2.Database.SQL.Domain;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiLA2.Frontend.Razor.Services.UserManagement.Domain
{
    public class User : BaseEntity
    {
        /// <summary>
        /// E-Mail
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Login { get; private set; }
        /// <summary>
        /// Password Hash
        /// </summary>
        public string Password { get; private set; }

        public Role Role { get; private set; }

        public User(string login, string password, Role role)
        {
            Login = login;
            Password = password;
            Role = role;
        }

        public User(Guid id, string login, string password, Role role) : this(login, password, role)
        {
            Id = id;
        }

        public void SetRole(Role role)
        {
            Role = role;
        }

        public void SetPassword(string pwd)
        {
            Password = pwd;
        }
    }
}
