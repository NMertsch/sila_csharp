﻿using Microsoft.Extensions.Logging;
using SiLA2.Database.SQL;
using SiLA2.Frontend.Razor.Services.UserManagement.Domain;
using SiLA2.Utils;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.Frontend.Razor.Services.UserManagement
{
    public class UserService : IUserService
    {
        private IRepository<User> _userRepository;
        private readonly ILogger<UserService> _logger;

        public UserService(IRepository<User> userRepository, ILogger<UserService> logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        public IQueryable<User> GetUsers()
        {
            return _userRepository.Table;
        }

        public async Task<User> GetUser(int id)
        {
            return await _userRepository.GetById(id);
        }

        public async Task InsertUser(User user)
        {
            await _userRepository.Insert(user);
        }

        public async Task UpdateUser(User user, bool hashPwd = false)
        {
            try
            {
                if (_userRepository.Table.Any(u => u.Login == user.Login))
                {
                    if (hashPwd)
                    {
                        user.SetPassword(Convert.ToBase64String(new PasswordHash(user.Password).ToArray()));
                    }
                    await _userRepository.Update(user);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, null, null);
            }
        }

        public async Task DeleteUser(User user)
        {
            try
            {
                await _userRepository.Delete(user);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, null, null);
            }
        }

        public async Task CreateUser(string login, string pwd, Role role = Role.Standard)
        {
            try
            {
                if (_userRepository.Table.Any(user => user.Login == login))
                {
                    _logger.LogWarning($"There´s already a login '{login}' stored in the database.");
                    return;
                }
                var pwHash = Convert.ToBase64String(new PasswordHash(pwd).ToArray());
                var user = new User(login, pwHash, role);
                await _userRepository.Insert(user);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, null, null);
            }
        }

        public User GetUser(string login)
        {
            return _userRepository.Table.SingleOrDefault(user => user.Login == login);
        }
    }
}