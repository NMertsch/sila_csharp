﻿using System;
using System.Text;

namespace SiLA2.Frontend.Razor.Services
{
    public static class TypeConverterExtensions
    {
        public static object GetValue(this Type propertyType, string input)
        {
            if (propertyType == typeof(Sila2.Org.Silastandard.String))
            {
                return new Sila2.Org.Silastandard.String { Value = input };
            }
            if (propertyType == typeof(Sila2.Org.Silastandard.Integer))
            {
                return new Sila2.Org.Silastandard.Integer { Value = long.Parse(input) };
            }
            if (propertyType == typeof(Sila2.Org.Silastandard.Real))
            {
                return new Sila2.Org.Silastandard.Real { Value = double.Parse(input) };
            }
            if (propertyType == typeof(Sila2.Org.Silastandard.Boolean))
            {
                return new Sila2.Org.Silastandard.Boolean { Value = bool.Parse(input) };
            }
            if (propertyType == typeof(Sila2.Org.Silastandard.Binary))
            {
                //TODO : Implementation
                throw new NotImplementedException("TODO");
            }
            if (propertyType == typeof(Sila2.Org.Silastandard.Date))
            {
                //TODO : How to handle message Timezone ?
                var date = DateTime.Parse(input);
                return new Sila2.Org.Silastandard.Date { Day = Convert.ToUInt32(date.Day), Month = Convert.ToUInt32(date.Month), Year = Convert.ToUInt32(date.Year) };
            }
            if (propertyType == typeof(Sila2.Org.Silastandard.Time))
            {
                //TODO : How to handle message Timezone ?
                var time = DateTime.Parse(input);
                return new Sila2.Org.Silastandard.Time { Hour = Convert.ToUInt32(time.Hour), Minute = Convert.ToUInt32(time.Minute), Second = Convert.ToUInt32(time.Second) };
            }
            if (propertyType == typeof(Sila2.Org.Silastandard.Timestamp))
            {
                //TODO : How to handle message Timezone ?
                var timestamp = DateTime.Parse(input);
                return new Sila2.Org.Silastandard.Timestamp { Day = Convert.ToUInt32(timestamp.Day), Month = Convert.ToUInt32(timestamp.Month), Year = Convert.ToUInt32(timestamp.Year), Hour = Convert.ToUInt32(timestamp.Hour), Minute = Convert.ToUInt32(timestamp.Minute), Second = Convert.ToUInt32(timestamp.Second) };
            }
            if (propertyType == typeof(Sila2.Org.Silastandard.Any))
            {
                return new Sila2.Org.Silastandard.Any { Type = propertyType.ToString(), Payload = Google.Protobuf.ByteString.CopyFrom(Encoding.Unicode.GetBytes(input)) };
            }
            throw new InvalidOperationException($"Unknown Property Type : {propertyType.FullName}");
        }
    }

}
