﻿using Grpc.Core;
using Grpc.Reflection.V1Alpha;
using SiLA2.Frontend.Razor.ViewModels;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using static Grpc.Reflection.V1Alpha.ServerReflection;

namespace SiLA2.Frontend.Razor.Services
{
    public class GrpcClientProvider : IGrpcClientProvider
    {
        private string[] excludeMethods = { "WithHost", "GetType", "ToString", "Equals", "GetHashCode" };
        private readonly IGrpcChannelProvider _grpcChannelProvider;
        private readonly IServerConfig _serverConfig;
        private ServerReflectionClient ServerReflectionClient;

        public Dictionary<string, ClientBase> ClientMap { get; } = new Dictionary<string, ClientBase>();

        public GrpcClientProvider(IGrpcChannelProvider grpcChannelProvider, IServerConfig serverConfig)
        {
            _grpcChannelProvider = grpcChannelProvider;
            _serverConfig = serverConfig;
        }

        public async Task CreateClients()
        {
            ServerReflectionClient = new ServerReflectionClient(await _grpcChannelProvider.GetChannel(_serverConfig.FQHN,_serverConfig.Port, true));
            
            var ct = new CancellationTokenSource();
            var response = await SingleRequestAsync(new ServerReflectionRequest
            {
                ListServices = "" // Get all services
            }, ct.Token);

            foreach (var item in response.ListServicesResponse.Service)
            {
                await CreateGrpcClient(item.Name);
            }
            ct.Cancel();
        }

        public async Task CreateGrpcClient(string ns)
        {
            if (ClientMap.ContainsKey(ns))
                return;

            var serviceObj = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).FirstOrDefault(x => x.FullName.ToLower() == ns.ToLower());
            var clientType = serviceObj.GetNestedTypes().SingleOrDefault(x => x.Name == $"{serviceObj.Name}Client"); //TODO: Condition NestedType == typeof(ClientBase) ?

            if (clientType != null)
            {
                ClientMap.Add(ns, (ClientBase)Activator.CreateInstance(clientType, await _grpcChannelProvider.GetChannel(_serverConfig.FQHN, _serverConfig.Port, true)));
            }
        }

        public async Task<IEnumerable<MethodInfo>> GetMethodInfosAsync(string ns)
        {
            return await Task.FromResult(ClientMap[ns].GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance)
                                .Where(x => !excludeMethods.Contains(x.Name) && x.GetParameters().Any(y => y.ParameterType == typeof(CancellationToken)))); // Check for Parameter CancellationToken to get distinct MethodInfos
        }

        public IEnumerable<MethodInfo> GetMethodInfos(string ns)
        {
            return ClientMap[ns].GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance)
                                .Where(x => !excludeMethods.Contains(x.Name) && x.GetParameters().Any(y => y.ParameterType == typeof(CancellationToken)));
        }

        public async Task InvokeMethod(string featureTypeIdentifier, FeatureMethodInfoViewModel methodResultBundle)
        {
            try
            {
                var client = ClientMap[featureTypeIdentifier];
                var firstParameter = methodResultBundle.Parameters.FirstOrDefault();
                var p = Activator.CreateInstance(firstParameter.Item1.ParameterType);

                if(p != null)
                {
                    foreach (var item in methodResultBundle.Parameters)
                    {
                        foreach (var prop in item.Item2)
                        {
                            prop.PropertyInfo.SetValue(p, prop.PropertyInfo.PropertyType.GetValue(prop.PropertyValue));
                        }
                    }
                }

                if(methodResultBundle.MethodInfo.IsAwaitableMethod())
                {
                    methodResultBundle.Result = await (dynamic)methodResultBundle.MethodInfo.Invoke(client, new object[] { p, null, null, null });
                }
                else
                {
                    methodResultBundle.Result = methodResultBundle.MethodInfo.Invoke(client, new object[] { p, null, null, null });
                }
            }
            catch (Exception ex)
            {
                methodResultBundle.Result = ex.ToString();
            }
        }

        private async Task<ServerReflectionResponse> SingleRequestAsync(ServerReflectionRequest request, CancellationToken cancellationToken)
        {
            using (var call = ServerReflectionClient.ServerReflectionInfo())
            {
                await call.RequestStream.WriteAsync(request);
                await call.ResponseStream.MoveNext(cancellationToken);
                var response = call.ResponseStream.Current;
                await call.RequestStream.CompleteAsync();
                return response;
            }
        }
    }
}
