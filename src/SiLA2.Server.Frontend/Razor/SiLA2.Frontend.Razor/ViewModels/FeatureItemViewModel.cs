﻿using System.Collections.Generic;

namespace SiLA2.Frontend.Razor.ViewModels
{
    public class FeatureItemViewModel
    {
        private List<FeatureProperty> _featureProperties = new List<FeatureProperty>();
        private List<FeatureMetadata> _featureMetadata = new List<FeatureMetadata>();
        private List<FeatureCommand> _featureCommands = new List<FeatureCommand>();
        private List<FeatureDefinedExecutionError> _featureDefinedExecutionErrors = new List<FeatureDefinedExecutionError>();
        private List<SiLAElement> _siLAElements = new List<SiLAElement>();
        private List<FeatureMethodInfoViewModel> _methods = new List<FeatureMethodInfoViewModel>();

        public string DisplayName { get; }
        public string FullyQualifiedIdentifier { get; }
        public string Description { get; }
        public string Namespace { get; }
        public string Identifier { get; }
        public string TypeIdentifier => $"{Namespace}.{Identifier}";

        public List<FeatureProperty> FeatureProperties => _featureProperties;
        public List<FeatureCommand> FeatureCommands => _featureCommands;
        public List<FeatureDefinedExecutionError> FeatureDefinedExecutionErrors => _featureDefinedExecutionErrors;
        public List<FeatureMetadata> FeatureMetadata => _featureMetadata;
        public List<FeatureMethodInfoViewModel> Methods => _methods;
        public List<SiLAElement> SilaElements => _siLAElements;

        public FeatureItemViewModel(string displayName, string fullyQualifiedIdentifier, string description, string ns, string identifier)
        {
            DisplayName = displayName;
            FullyQualifiedIdentifier = fullyQualifiedIdentifier;
            Description = description;
            Namespace = ns;
            Identifier = identifier;
        }

        public override string ToString()
        {
            return $"{DisplayName} ({FullyQualifiedIdentifier})";
        }
    }
}
